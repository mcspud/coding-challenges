"use client"

import {
  Alert,
  Flex,
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  Box,
  FormControl,
  FormLabel,
  FormHelperText,
  Input,
  Progress,
  Step,
  StepIcon,
  StepIndicator,
  StepStatus,
  Stepper,
  useSteps,
  UseDisclosureReturn,
} from '@chakra-ui/react'

import { useFormState } from "react-dom";
import { createUser } from '@/app/actions';
import { useContext } from 'react';
import { ActiveUserContext } from './InnerLayout';
import FormLoader from './FormLoader';
import SubmitButton from './SubmitButton';
import { nonSelectedUserId } from '@/app/constants';



type InitialState = {
  success: null | boolean
  key: number
}
const initialState: InitialState = {
  success: null,
  key: nonSelectedUserId,
}


function AddForm() {
  const [state, formAction] = useFormState(createUser, initialState)
  const context = useContext(ActiveUserContext)

  // Once we create a user we magically "log in" as them.  This is a very loose appoximation 
  // of implementing an auth flow, with the idea being that we'd normally to this properly 
  // with things like passwords and email validation etc.
  if (state.success === true) {
    context.setCurrentUser(state.key)
  }

  // Display settings for the progress bar, nothing exciting here
  const { activeStep, setActiveStep } = useSteps({
    index: 1,
    count: 3,
  })
  const progressPercent = ((activeStep - 1) / 2) * 100


  return (
    <Box>
      <Box position='relative' mb={3}>
        <Stepper size='sm' index={activeStep} gap='0'>
          {[1, 2, 3].map((step, index) => (
            <Step key={index} onClick={() => setActiveStep(index + 1)}>
              <StepIndicator bg='white'>
                <StepStatus complete={<StepIcon />} />
              </StepIndicator>
            </Step>
          ))}
        </Stepper>
        <Progress
          value={progressPercent}
          position='absolute'
          height='3px'
          width='full'
          top='10px'
          zIndex={-1}
        />
      </Box>


      <form action={formAction}>
        <FormControl hidden={activeStep !== 1}>
          <FormLabel>Email address</FormLabel>
          <Input type='email' name="email" />
          <FormHelperText>We wont share your email.</FormHelperText>
        </FormControl>

        <FormControl hidden={activeStep !== 2}>
          <FormLabel>Job Position</FormLabel>
          <Input type='text' name="position" />
          <FormHelperText>Tell us about yourself.</FormHelperText>
        </FormControl>

        <FormControl hidden={activeStep !== 3}>
          <FormLabel>Submit user</FormLabel>
          <SubmitButton />
          <FormHelperText>Save user to database.  You will be logged in as this user going forward.</FormHelperText>
        </FormControl>

        {state.success === true &&
          <Alert status="success">Successfully added user</Alert>
        }

        {state.success === false &&
          <Alert status="error">Error adding user</Alert>
        }

        <FormLoader />

      </form>
      <Flex justifyContent='space-between' mt={5}>
        <Button colorScheme='blue' isDisabled={activeStep === 1} onClick={() => setActiveStep(activeStep - 1)}>Previous </Button>
        <Button colorScheme='blue' isDisabled={activeStep === 3} onClick={() => setActiveStep(activeStep + 1)}>Next</Button>
      </Flex>
    </Box >
  )
}

type AddFormModalProps = {
  disclosure: UseDisclosureReturn
}

// This component represents the modal that allows the addition of a new user.
export default function AddFormModal({ disclosure }: AddFormModalProps) {

  return (
    <Modal isOpen={disclosure.isOpen} onClose={disclosure.onClose} size='2xl'>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Add New User</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <AddForm />
        </ModalBody>
      </ModalContent>
    </Modal >
  )
}
