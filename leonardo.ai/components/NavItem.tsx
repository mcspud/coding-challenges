import {
  Flex,
  Icon,
} from '@chakra-ui/react'
import Link from 'next/link';
import { IconType } from 'react-icons'

interface NavItemProps {
  icon: IconType,
  children: React.ReactNode
  path: string,
  isDisabled: boolean;
}

function DisabledNavItem({ icon, children }: NavItemProps) {
  return (
    <Link
      href={'#'}
      style={{ textDecoration: 'none'}}>
      <Flex
        color='gray.200'
        align="center"
        p="4"
        mx="4"
        borderRadius="lg"
        role="group"
        cursor="not-allowed"
        >
        <Icon
          mr="4"
          fontSize="16"
          as={icon}
        />
        {children}
      </Flex>
    </Link>
  )
}

export default function NavItem({ icon, children, path, isDisabled }: NavItemProps) {
  return isDisabled === true
    ? <DisabledNavItem icon={icon} children={children} path={path} isDisabled={isDisabled} />
    : (
      <Link
        href={path}
        style={{ textDecoration: 'none' }}>
        <Flex
          align="center"
          p="4"
          mx="4"
          borderRadius="lg"
          role="group"
          cursor="pointer"
          _hover={{
            bg: 'cyan.400',
            color: 'white',
          }}>
          <Icon
            mr="4"
            fontSize="16"
            _groupHover={{
              color: 'white',
            }}
            as={icon}
          />
          {children}
        </Flex>
      </Link>
    )
}
