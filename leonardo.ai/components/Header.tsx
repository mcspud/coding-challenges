"use client"

import {
  Alert,
  IconButton,
  Flex,
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  Box,
  FormControl,
  FormLabel,
  Input,
  useColorModeValue,
  useDisclosure,
  Spinner,
} from '@chakra-ui/react'

import { FiMenu } from 'react-icons/fi'
import { useFormState, useFormStatus } from "react-dom";
import { getUser, updateUser } from '@/app/actions';
import { nonSelectedUserId } from '@/app/constants';
import { useContext, useEffect, useState } from 'react';
import { ActiveUserContext } from './InnerLayout';
import FormLoader from './FormLoader';
import { SelectUser } from '@/lib/schema';
import AddFormModal from './AddFormModal';


type SubmitButtonProps = {
  disabled: boolean
}
function SubmitButton({ disabled }: SubmitButtonProps) {
  // Unfortunately due to a limitation in the `useFormStatus` hook we need to 
  // essentially redeclare a button to get "disable" behavior.
  // see https://react.dev/reference/react-dom/hooks/useFormStatus#useformstatus-will-not-return-status-information-for-a-form-rendered-in-the-same-component
  const status = useFormStatus()

  return (
    <Button colorScheme="green" type='submit' isDisabled={status.pending === true || disabled === true}>Submit</Button>
  )
}



const initialUserFormState: { success: null | boolean } = {
  success: null
}

const initialUser: SelectUser = {
  id: 0,
  email: '',
  position: '',
}

function UpdateForm() {
  const [state, formAction] = useFormState(updateUser, initialUserFormState)
  const context = useContext(ActiveUserContext)

  const [userData, setUserData] = useState<SelectUser>({
    ...initialUser,
    id: context.currentUser
  })

  useEffect(() => {
    const get = async () => {
      const data = await getUser(context.currentUser)
      setUserData(data[0])
    }
    get()
  }, [context.currentUser])

  return (
    <Box>

      <form action={formAction}>
        {/*Sneakily pass in the current users ID*/}
        <Input type='hidden' name="id" value={context.currentUser} />

        <FormControl isDisabled={userData.email === ''}>
          <FormLabel>Email address</FormLabel>
          <Input type='email' name="email" defaultValue={userData.email} />
        </FormControl>

        <FormControl isDisabled={userData.position === ''}>
          <FormLabel>Job Position</FormLabel>
          <Input type='text' name="position" defaultValue={userData.position} />
        </FormControl>

        {userData.email === '' &&
          <Flex justifyContent='center'>
            <Spinner color='red' size='xl' />
          </Flex>
        }

        {state.success === true &&
          <Alert status="success">Successfully updated profile</Alert>
        }

        {state.success === false &&
          <Alert status="error">Error updating profile</Alert>
        }

        <SubmitButton disabled={userData.email === ''} />

      </form>
    </Box >
  )
}

type HeaderProps = {
  onOpen: () => void
}

export default function Header({ onOpen }: HeaderProps) {
  const context = useContext(ActiveUserContext)
  // toggles the Register user modal state
  const newUserModal = useDisclosure()
  // toggles the profile modal
  const updateProfileModal = useDisclosure()

  return (
    <Flex
      ml={{ base: 0, md: 60 }}
      px={{ base: 4, md: 4 }}
      height="20"
      alignItems="center"
      bg={useColorModeValue('white', 'gray.900')}
      borderBottomWidth="1px"
      borderBottomColor={useColorModeValue('gray.200', 'gray.700')}
      justifyContent={{ base: 'space-between', md: 'flex-end' }}
    >

      <IconButton
        display={{ base: 'flex', md: 'none' }}
        onClick={onOpen}
        variant="outline"
        aria-label="open menu"
        icon={<FiMenu />}
      />

      <Flex justifyContent='end'>
        <Button
          colorScheme='blue'
          onClick={updateProfileModal.onOpen}
          mx={2}
          isDisabled={context.currentUser === nonSelectedUserId}>
          Profile
        </Button>
        <Button colorScheme='blue' onClick={newUserModal.onOpen} mx={2}>Register</Button>
      </Flex>

      <AddFormModal disclosure={newUserModal} />

      <Modal isOpen={updateProfileModal.isOpen} onClose={updateProfileModal.onClose} size='2xl'>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Update Profile</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <UpdateForm />
          </ModalBody>
        </ModalContent>
      </Modal>
    </Flex>
  )
}

