import {
  Box,
  CloseButton,
  Flex,
  Text,
  useColorModeValue,
  UseDisclosureReturn,
} from '@chakra-ui/react'
import {
  FiHome,
  FiTrendingUp,
} from 'react-icons/fi'
import NavItem from './NavItem'
import { useContext } from 'react'
import { ActiveUserContext } from './InnerLayout'
import { nonSelectedUserId } from '@/app/constants'



interface SidebarNavProps {
  disclosure: UseDisclosureReturn
}
export default function Sidebar({ disclosure }: SidebarNavProps) {

  const {currentUser} = useContext(ActiveUserContext)

  return (
    <Box
      transition="3s ease"
      bg={useColorModeValue('white', 'gray.900')}
      borderRight="1px"
      borderRightColor={useColorModeValue('gray.200', 'gray.700')}
      w={{ base: 'full', md: 60 }}
      pos="fixed"
      h="full"
      display={{ base: 'none', md: 'block' }}
    >
      <Flex h="20" alignItems="center" mx="8" justifyContent="space-between">
        <Text fontSize="2xl" fontFamily="monospace" fontWeight="bold">
          Leonardo.ai
        </Text>
        <CloseButton display={{ base: 'flex', md: 'none' }} onClick={disclosure.onClose} />
      </Flex>
        <NavItem isDisabled={false} icon={FiHome} path={'/'}>Users</NavItem>
        {/* If there is no current user (represented by the sential value) then disable access to the info page*/}
        <NavItem isDisabled={currentUser === nonSelectedUserId} icon={FiTrendingUp} path={'/information'}>Information</NavItem>
    </Box>
  )
}

