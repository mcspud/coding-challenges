import { countUsers, listUsers } from './actions'
import {
  Box,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableCaption,
  TableContainer,
} from '@chakra-ui/react'


export default async function Users() {
  const users = await listUsers()
  const count = await countUsers()

  return (
    <Box>
      <TableContainer>
        <Table variant='simple'>
          <TableCaption>Displaying {count[0].value} users</TableCaption>
          <Thead>
            <Tr>
              <Th>User ID</Th>
              <Th>User Email</Th>
              <Th>Job Title</Th>
            </Tr>
          </Thead>
          <Tbody>
            {users.map(user => (
              <Tr key={user.id}>
                <Td>{user.id}</Td>
                <Td>{user.email}</Td>
                <Td>{user.position}</Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      </TableContainer>
    </Box>

  )
}
