// Due to using server side rendering and limitations of NextJS we need to split 
// our "client" side components out into their own modules(files).  Its annoying 
// because this should ideally be inlined with the corresponding "page.tsx" file, 
// but this seems like a worthwhile tradeoff to me.
"use client"

import { PlantT } from './actions'
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  Tr,
  Td,
  Table,
  Tbody,
  useDisclosure,
} from '@chakra-ui/react'
import Image from 'next/image'

export default function Plant(plant: PlantT) {

  const modal = useDisclosure()

  return (
    <Tr cursor='pointer' onClick={() => modal.onOpen()} _hover={{ bg: 'white' }}>

      <Td>{plant.id}</Td>
      <Td>{plant.name}</Td>
      <Td><Image
        src={plant.photo}
        alt={plant.name}
        width={50}
        height={50} />
      </Td>

      <Modal isOpen={modal.isOpen} onClose={modal.onClose} size='2xl'>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Planet Details</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Image
              src={plant.photo}
              alt={plant.name}
              width={500}
              height={200} />
            <Table variant='simple'>
              <Tbody>
                <Tr>
                  <Td>ID</Td>
                  <Td>{plant.id}</Td>
                </Tr>
                <Tr>
                  <Td>Name</Td>
                  <Td>{plant.name}</Td>
                </Tr>
                <Tr>
                  <Td>Description</Td>
                  <Td>{plant.description}</Td>
                </Tr>

              </Tbody>
            </Table>
          </ModalBody>
        </ModalContent>
      </Modal>
    </Tr>
  )
}

