import Plant from './Planet'
import { getItems, PlantT } from './actions'
import {
  Box,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  TableCaption,
  TableContainer,
} from '@chakra-ui/react'


export default async function Users() {
  const plants = await getItems()

  return (
    <Box>
      <TableContainer>
        <Table variant='simple'>
          <TableCaption>Displaying items</TableCaption>
          <Thead>
            <Tr>
              <Th>ID</Th>
              <Th>Name</Th>
              <Th>Photo</Th>
            </Tr>
          </Thead>
          <Tbody>
            {plants.data.map(plant => <Plant key={plant.id} {...plant} />)}
          </Tbody>
        </Table>
      </TableContainer>
    </Box>

  )
}
