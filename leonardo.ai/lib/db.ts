import { drizzle } from "drizzle-orm/postgres-js";
import postgres from 'postgres'


const queryClient = postgres(process.env.POSTGRES_URL_NON_POOLING as string)

// returns an active connection the database 
export default drizzle(queryClient);
