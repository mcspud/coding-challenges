import { drizzle } from "drizzle-orm/postgres-js";
import { migrate } from 'drizzle-orm/postgres-js/migrator';
import postgres from 'postgres'
import { loadEnvConfig } from '@next/env'


async function runMigrations() {
  // Sets up environment variables.  This flag allows the native .env.ENVIRONMENT loader for 
  // nextjs to work how we expect it to, allowing for (theoretically) easier deployments with
  // vercel.  See the package.json `migrate` scripts
  if (process.env.NODE_ENV === 'development') {
    loadEnvConfig(process.cwd())
  }
  const migrationClient = postgres(process.env.POSTGRES_URL_NON_POOLING as string, { max: 1 });
  const db = drizzle(migrationClient, { logger: true });
  // runs migrations
  await migrate(db, { migrationsFolder: `${process.cwd()}/drizzle` });
  // clean exit for the CI/CD server or your local machine
  process.exit(0);
}

runMigrations().then(r => console.log(r)).catch(e => console.log(e))
