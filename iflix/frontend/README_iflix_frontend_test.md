<img height="150px" src="https://happypatowens.com/public/iflix_logo.png">

##  Frontend ~~test~~ coding experiment

Hi there! we're so excited that you're applying for a role in the iflix engineering team.
So that we can get an idea about how you problem solve and write code, we have a small
coding experiment we'd love you to try.

#### The Task

We'd love you to write a web component that submits 5 star ratings for our iflix titles.
Our visitors should be presented with an opportunity to rate a title out of 5 stars after
viewing. We're imagining a modal dialog that pops over the video at the end that lets the
user rate the video they have just watched, for bonus points, display the average rating
once the user has rated, and provide a seperate component to display the average rating
when users are browsing content.

The backend is ready to accept the following information to rate and rertieve content:

For rating content:

```json
POST /rating
{
  "userId" : 4132246,
  "contentId" : 562135,
  "rating" : 3
}
```

For retrieving content:
```json

GET /rating/:contentId
{
  "contentId" : 562135,
  "average" : 3.2
}
```

#### What we're looking for
Don't stress! take your time, it's not a race.  We're not looking for perfection but a
well-considered MVP that we could further develop for production. Imagine this is a
tech-spike to explore the concept which we will test on a small percentage (1-2 million)
users.

Things to consider:

* Appropriate error handling
* Tests
* Feel free to select a component-oriented framework that you're familiar with
* Clear maintainable code with good use of commenting

Good luck!
