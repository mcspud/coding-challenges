import {Provider} from 'react-redux'
import { State } from '../data/@records'
import { Content, ContentId } from '../data/@domain';
import { Content as ContentRecord } from '../data/content/@records'

import Immutable from 'immutable'
import Faker from 'faker'
import Enzyme from 'enzyme'
import Adapter from "enzyme-adapter-react-16";
import React from 'react'
import ReactDOM from 'react-dom'
import App from './index'
import Data from '../data'
import {FilterByGenre} from './index'


Enzyme.configure({ adapter: new Adapter() });


const ContentFactory = (genre:string = 'Action'): Content => ({
    contentId: Faker.random.number({min: 10000000, max: 99999999}),
    genre: genre,
    title: Faker.lorem.sentence()
})


it('renders without crashing', () => {
    const div = document.createElement('div');

    ReactDOM.render(<Provider store={Data.store}><App /></Provider>, div);
    ReactDOM.unmountComponentAtNode(div);
});

it('Filters state by a list of available genres', () => {
    // setup
    const actionCount = Faker.random.number({max: 10})
    const horrorCount = Faker.random.number({max: 10})
    const state = State().updateIn(['content', 'list'], content => {
        const action = Array(actionCount).fill(null).map(_ => ContentFactory('Action')).map(ContentRecord)
        const horror = Array(horrorCount).fill(null).map(_ => ContentFactory('Horror')).map(ContentRecord)
        return action.concat(horror).reduce((acc, val) => acc.set(val.contentId, val), Immutable.Map<ContentId, ContentRecord>())
    })

    // test
    expect(FilterByGenre('Action')(state).size).toEqual(actionCount)
    expect(FilterByGenre('Horror')(state).size).toEqual(horrorCount)
})

it('Has the logo image source', () => {
    const element = Enzyme.mount(<Provider store={Data.store}><App /></Provider>)
    expect(element.html()).toContain('logo.png')
})
