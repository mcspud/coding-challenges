import observables from '../observables'
import actions from './actions'
import creators from './creators'


const getAllContent = observables.GET(
    actions.load_content.START,
    creators.load_content.SUCCESS,
    creators.load_content.ERROR,
);


export default {
    getAllContent
}
