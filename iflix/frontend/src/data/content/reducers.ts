import { State, Content as ContentRecord } from './@records'
import { AnyAction } from 'redux';
import { AsyncCreatorSuccess, AsyncCreatorGet, AsyncCreatorError } from '../@types';
import { Content, ContentId } from '../@domain';

import * as I from 'immutable'
import actions from './actions'

export const loadContentStart = (state: State, action: AsyncCreatorGet) =>
    state
        .update('lifecycle', lifecycle => lifecycle.merge({
            loading: true,
            success: false,
            error: false,
        }));

export const loadContentSuccess = (state: State, action: AsyncCreatorSuccess<Array<Content>>) =>
    state
        .update('lifecycle', lifecycle => lifecycle.merge({
            loading: false,
            success: true,
            error: false,
        }))
        .update('list', map => map.merge(action.data
            .map(ContentRecord)
            .reduce((acc, val) => acc.set(val.contentId, val), I.Map<ContentId, ContentRecord>())
        ))


export const loadContentError = (state: State, action: AsyncCreatorError) =>
    state
        .update('lifecycle', lifecycle => lifecycle.merge({
            loading: false,
            success: false,
            error: true,
        }))

export default (state = State(), action: AnyAction & any) => {
    switch (action.type) {
        case actions.load_content.START: return loadContentStart(state, action)
        case actions.load_content.SUCCESS: return loadContentSuccess(state, action)
        case actions.load_content.ERROR: return loadContentError(state, action)
        default: return state
    }
}
