import { Map, Record, RecordOf } from 'immutable'
import { Lifecycle } from "../../@records/lifecycle";
import { Content } from "./content";
import { ContentId } from '../../@domain';

type _State = {
    list: Map<ContentId, Content>
    lifecycle: Lifecycle
}

export type State = RecordOf<_State>
export const State: Record.Factory<_State> = Record({
    list: Map(),
    lifecycle: Lifecycle()
});
