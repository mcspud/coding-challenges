import { AsyncCreators, AsyncCreatorGet, AsyncCreatorSuccess, AsyncCreatorError, AjaxResponseWithAction, AjaxErrorWithAction } from "../@types";
import actions from './actions'
import endpoints from './endpoints'


const LoadContentStart = (): AsyncCreatorGet => ({
    type: actions.load_content.START,
    url: endpoints.LOAD_CONTENT(),
})


const LoadContentSuccess = (result: AjaxResponseWithAction): AsyncCreatorSuccess => ({
    type: actions.load_content.SUCCESS,
    response: result,
    data: result.response,
    action: result.action
})


const LoadContentError = (result: AjaxErrorWithAction): AsyncCreatorError => ({
    type: actions.load_content.ERROR,
    response: result,
    error: result.response,
    action: result.action
})


export default {
    load_content: <AsyncCreators>{
        START: LoadContentStart,
        SUCCESS: LoadContentSuccess,
        ERROR: LoadContentError
    }
}
