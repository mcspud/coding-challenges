import { Map, Record, RecordOf } from 'immutable'
import { Rating } from "./rating";
import { ContentId } from '../../@domain';

type _State = {
    list: Map<ContentId, Rating>
}

export type State = RecordOf<_State>
export const State: Record.Factory<_State> = Record({
    list: Map(),
});
