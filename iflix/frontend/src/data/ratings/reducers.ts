import { State, Rating } from './@records'
import { Action } from 'redux';
import { ContentId } from '../@domain';
import { AsyncCreatorGet, AsyncCreatorSuccess, AsyncCreatorError, AsyncCreatorPost } from '../@types';

import actions from './actions'


export const loadRatingStart = (state: State, action: AsyncCreatorGet<{contentId: number}>) => {
    const rating = state.list
        .get(action.contentId, Rating())
        .update('lifecycle', lifecycle => lifecycle.merge({
            loading: true,
            success: false,
            error: false,
        }))
    return state.setIn(['list', action.contentId], rating)
}


export const loadRatingSuccess = (state: State, action: AsyncCreatorSuccess<Rating>) => {
    const contentId: ContentId = action.action.contentId
    const rating = state.list
        .get(contentId, Rating())
        .merge(action.data)
        .update('lifecycle', lifecycle => lifecycle.merge({
            loading: false,
            success: true,
            error: false,
        }))
    return state.setIn(['list', contentId], rating)
}


export const loadRatingError = (state: State, action: AsyncCreatorError<{contentId: number}>) => {
    const rating = state.list
        .get(action.error.contentId, Rating())
        .update('lifecycle', lifecycle => lifecycle.merge({
            loading: false,
            success: false,
            error: true,
        }))
    return state.setIn(['list', action.action.contentId], rating)
}


export const submitRatingStart = (state: State, action: AsyncCreatorPost<{contentId: number}>) => {
    const rating = state.list
        .get(action.contentId, Rating())
        .update('lifecycle', lifecycle => lifecycle.merge({
            loading: true,
            success: false,
            error: false,
        }))
    return state.setIn(['list', action.contentId], rating)
}


export const submitRatingSuccess = (state: State, action: AsyncCreatorSuccess<Rating>) => {
    const contentId: ContentId = action.action.contentId
    const rating = state.list
        .get(contentId, Rating())
        .merge(action.data)
        .update('lifecycle', lifecycle => lifecycle.merge({
            loading: false,
            success: true,
            error: false,
        }))
    return state.setIn(['list', contentId], rating)
}


export const submitRatingError = (state: State, action: AsyncCreatorError<{contentId: number}>) => {
    const rating = state.list
        .get(action.action.contentId, Rating())
        .update('lifecycle', lifecycle => lifecycle.merge({
            loading: false,
            success: false,
            error: true,
        }))
    return state.setIn(['list', action.action.contentId], rating)
}

export default (state = State(), action: Action & any) => {
    switch (action.type) {
        case actions.load_rating.START: return loadRatingStart(state, action)
        case actions.load_rating.SUCCESS: return loadRatingSuccess(state, action)
        case actions.load_rating.ERROR: return loadRatingError(state, action)

        case actions.submit_rating.START: return submitRatingStart(state, action)
        case actions.submit_rating.SUCCESS: return submitRatingSuccess(state, action)
        case actions.submit_rating.ERROR: return submitRatingError(state, action)
        default: return state
    }
}
