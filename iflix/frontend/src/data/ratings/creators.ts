import { AsyncCreatorGet, AsyncCreatorSuccess, AsyncCreatorError, AsyncCreatorPost, AjaxResponseWithAction, AjaxErrorWithAction } from "../@types"
import { AjaxResponse, AjaxError } from "rxjs/ajax"
import { ContentId, PostRatingBody } from "../@domain"

import actions from './actions'
import endpoints from './endpoints'


const LoadRatingStart = (contentId: ContentId): AsyncCreatorGet<{contentId: number}> => ({
    type: actions.load_rating.START,
    url: endpoints.LOAD_RATING_FOR_CONTENT(contentId),
    contentId: contentId
})


const LoadRatingSuccess = (result: AjaxResponseWithAction): AsyncCreatorSuccess => ({
    type: actions.load_rating.SUCCESS,
    response: result,
    data: result.response,
    action: result.action
})


const LoadRatingError = (result: AjaxErrorWithAction): AsyncCreatorError => ({
    type: actions.load_rating.ERROR,
    response: result,
    error: result.response,
    action: result.action
})

const SubmitRatingStart = (contentId: ContentId, body: PostRatingBody): AsyncCreatorPost<{contentId: number}> => ({
    type: actions.submit_rating.START,
    url: endpoints.SUBMIT_RATING_FOR_CONTENT(),
    body: body,
    contentId: contentId
})


const SubmitRatingSuccess = (result: AjaxResponseWithAction<AsyncCreatorPost<{contentId: number}>>): AsyncCreatorSuccess => ({
    type: actions.submit_rating.SUCCESS,
    response: result,
    data: result.response,
    action: result.action
})


const SubmitRatingError = (result: AjaxErrorWithAction<AsyncCreatorPost<{contentId: number}>>): AsyncCreatorError => ({
    type: actions.submit_rating.ERROR,
    response: result,
    error: result.response,
    action: result.action
})


export default {
    load_rating: {
        START: LoadRatingStart,
        SUCCESS: LoadRatingSuccess,
        ERROR: LoadRatingError
    },

    submit_rating: {
        START: SubmitRatingStart,
        SUCCESS: SubmitRatingSuccess,
        ERROR: SubmitRatingError,
    }
}
