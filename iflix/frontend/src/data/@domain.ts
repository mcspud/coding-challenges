import { Lifecycle } from "./@records";

export type ContentId = number
export type Genre =
    | 'Action'
    | 'Horror'
    | 'Sci-fi'

export type Empty = {
    STRING: string
    NUMBER: number
}


export const Empty: Empty = {
    STRING: '',
    NUMBER: -1
}


export type Rating = {
    lifecycle: Lifecycle
    contentId: ContentId
    score: number
    count: number
    total: number
}


export type Content = {
    contentId: ContentId
    title: string
    genre: string
}


export type Score = {
    contentId: ContentId
    score: number
}


export type PostRatingBody = {
    contentId: ContentId
    userId: number
    rating: number
}
