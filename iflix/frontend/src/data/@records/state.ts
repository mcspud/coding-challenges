import { Record, RecordOf } from 'immutable';
import { State as ContentState } from '../content/@records'
import { State as RatingsState } from '../ratings/@records'


type _State = {
    content: ContentState
    ratings: RatingsState
}


export type State = RecordOf<_State>
export const State: Record.Factory<_State> = Record({
    content: ContentState(),
    ratings: RatingsState(),
});
