import content from './content/creators'
import ratings from './ratings/creators'

export default {
    content,
    ratings
}
