# Frontend

This package represents base work for a frontend user "ratings" component.
The UI has been approximately modelled after the production UI theme, with each "panel" representing a piece of content.

## Core Technical Decisions

The following major decisions were made:

#### Typescript

Typing is good.  Deep and proper typing of your components allows the compiler to generate out a huge swarth of tests for you automatically, greatly reducing bugs.  It also makes your code tractable - once the initial pain of learning to read types (basically a seperate language in itself), the flow and transformation of data in a codebase becomes much easier to follow.

In statically typed functional language theres a saying that goes "if it compiles it probably works" due to the rigor of compiler checked types.  Javascript is a pig, but we can put lipstick on it and if squint hard enough it looks roughly right.

#### Immutable Store

Javascript is mutable by default, and provides no unified syntax for operating over collections.
Immutable JS solves both of these problems elegantly.
Immutable `Record` types are used extensively to implement a predictable state tree.  These types are backed by typescript definitions so that the compiler makes sure we don't break those either.  All major IDE's also support nested immutable type helpers with typescript, so it becomes much easier to work through.

#### RxJS

RxJS was implented as the method to fetch data and model user interactions.  Instead of relying on multiple tools for this, the reactive programming paradigm was embraced fully, chaining user and application events into a single predictable pattern of observables.

#### React

Its all the rage, despite its serious flaws.  The world isn't ready for Elm or Purescript yet :(

## Project Structure

The project is broken into two distinct components, a `data` layer and an `application` layer.  The `data` layer does all of the work, whilst the `application` layer merely displays that data.

#### Conventions

- Types are more important than code, as they alone represent valid program states.  To this end, anything with an `@` infront of it should be considered a "super important" thing and also (probably) a type.
- Dont use `any` in your types, except when using generics.
- There are two broad categories of types - the business object things that we're interested in, and the stuff we need to get the system to actually run and work predictably.  To this end thse have been split into different files,`@domain` and `@types` respectively.  The `Customer` type would live in `@domain`, whilst the `AsynchronousDatabaseQuery` definitely lives in `@types`.

#### Data

The `data` module contains the meat of the implementation.  Each package within here has the following structure:
```
@records
actions.ts
creators.ts
reducers.ts
```

- `@records` is a module that provides a typesafe, immutable safe implementation of state.  These are our "persistent" objects.
- `actions.ts` provides a set of unique strings in `flux` style
- `creators.ts` provides a set of **typed** `action creator` functions in `flux` style
- `reducers.ts` provides a set of **typed** state reduction functions in `flux` style

Each module will optionally have:

```
endpoints.ts
epics.ts
```

- `endpoints.ts` contains a set of *string generating **functions***.  No constants, **only** functions.
- `epics.ts` contains a standard implementation for observable promises to go and do async stuff.

Epics are a very strong way to implement controls over promises via observables, and provide a much better experience to work with in any sufficiently complex code base.  Since the request-response cycle is so generic, the `data/observables.ts` file provides some common abstractions around it using epics.

Epics are controlled by actions and run asynchronously **AFTER** the action has passed through the reduction functions.  They operate similarly to a `saga`, in they take an action and emit an action back through the reduction loop, and do side-effecty things internally outside of the main thread.
