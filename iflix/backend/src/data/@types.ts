import mongoose from 'mongoose'

export type BaseModel = {
    timestamps: {
        createdAt: '_created_at',
        updatedAt: '_updated_at'
    }
}

export const BaseModel: BaseModel = {
    timestamps: {
        createdAt: '_created_at',
        updatedAt: '_updated_at'
    }
}

export type SchemaMeta<T = any> = {
    schema: mongoose.Schema
    model: mongoose.Model<T & mongoose.Document>
}

type IsRequired<T> =
    undefined extends T
    ? false
    : true;

type FieldType<T> =
    T extends number ? typeof Number :
    T extends string ? typeof String :
    Object;

type Field<T> = {
    type: FieldType<T>,
    required: IsRequired<T>,
    enum?: Array<T>
    index?: boolean
    unique?: boolean
};

export type ModelDefinition<M> = {
    [P in keyof M]-?:
    M[P] extends Array<infer U>
        ? Array<Field<U>>
        : Field<M[P]>
};
