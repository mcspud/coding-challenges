export type ContentId = number

export type Genre =
    | 'Action'
    | 'Horror'
    | 'Sci-fi'

export type Content = {
    contentId: ContentId
    title: string
    genre: Genre
}

export type Score = {
    contentId: ContentId
    count: number
    total: number
    score: number
}

export type Rating = {
    contentId: ContentId
    userId: number
    rating?: number
}

export type Collections = {
    RATINGS: 'ratings'
    SCORES: 'scores'
    CONTENT: 'content'
}

export const Collections: Collections = {
    RATINGS: 'ratings',
    SCORES: 'scores',
    CONTENT: 'content',
}

export type RangeBoundary = {
    MIN: number
    MAX: number
}

export const ContentIdBoundary: RangeBoundary = {
    MIN: 100000,
    MAX: 999999,
}

export const UserIdBoundary: RangeBoundary = {
    MIN: 1000000,
    MAX: 9999999,
}

export const RatingBoundary: RangeBoundary = {
    MIN: 1,
    MAX: 5,
}
