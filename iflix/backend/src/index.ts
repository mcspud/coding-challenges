import { MongoNetworkError } from 'mongodb';
import { Express } from 'express'

import express from 'express'
import paths from './paths'
import routes from './routes'
import connect from './connector'
import config from './config'
import cors from 'cors'

const successfulConnect = (port: string) => () =>
    console.log(`Content ready to get rated on port ${port}!`)

const unsuccessfulConnect = (error: MongoNetworkError) =>
    console.log(error)

const installMiddleware = (app: Express) => () => {
    app.use(express.json())
    app.use(cors())
}

const bindRoutes = (app: Express) => () => {
    // content
    app.get(paths.content.GET, routes.content.GET)

    // rating
    app.get(paths.rating.GET, routes.rating.GET)
    app.post(paths.rating.POST, routes.rating.POST)
}

const app: Express = express();

const initialise = () =>
    app.listen(config.APPLICATION_PORT, () =>
        connect.RATINGS_DB()
            .then(installMiddleware(app))
            .then(bindRoutes(app))
            .then(successfulConnect(config.APPLICATION_PORT))
            .catch(unsuccessfulConnect))

initialise()
