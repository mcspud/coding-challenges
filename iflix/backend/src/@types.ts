export type ApplicationConfiguration = {
    MONGO_HOST: string
    MONGO_PORT: string
    MONGO_RATING_DATABASE: string

    APPLICATION_PORT: string
}
