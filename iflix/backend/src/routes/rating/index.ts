import { RouteHandlers } from '../@types';
import GET from './get';
import POST from './post'

export default <RouteHandlers>{
    GET,
    POST,
}
