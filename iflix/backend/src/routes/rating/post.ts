import { Request, Response, NextFunction } from 'express'
import { Rating, Score, ContentId, ContentIdBoundary, UserIdBoundary, RatingBoundary } from '../../@domain';
import { validateOrReject, IsInt, Min, Max, } from "class-validator";
import Data from '../../data'

export class Validator implements Rating {

    @IsInt()
    @Min(ContentIdBoundary.MIN)
    @Max(ContentIdBoundary.MAX)
    contentId!: number;

    @IsInt()
    @Min(UserIdBoundary.MIN)
    @Max(UserIdBoundary.MAX)
    userId!: number;

    @IsInt()
    @Min(RatingBoundary.MIN)
    @Max(RatingBoundary.MAX)
    rating!: number;

}


export const DefaultScore = (contentId: ContentId): Score => ({
    contentId: contentId,
    count: 0,
    score: 0,
    total: 0
})


export const UpdateScore = async (rating: Rating) =>
    await Data.scores.model
        .findOne({ contentId: rating.contentId })
        .then(instance => instance !== null
            ? instance
            : new Data.scores.model(DefaultScore(rating.contentId)).save()
        ).then(score => score
            .set({
                count: score.count += 1,
                total: score.total += rating.rating
            })
            .set({
                score: score.total / score.count
            })
            .save()
        )

export default async (req: Request, res: Response, next: NextFunction) => {
    const body: Rating = Object.assign(new Validator(), req.body)
    const data = validateOrReject(body).catch(next)

    await new Data
        .ratings
        .model(body)
        .save()
        .then(rating => {
            UpdateScore(rating)
            res.send(rating)
        })
        .catch(next)
}
