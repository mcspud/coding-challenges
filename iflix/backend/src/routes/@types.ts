import { RequestHandler } from 'express';

// Forces a naming convention for all route modules
export type Handlers = {
    [s: string]: RouteHandlers
}

// Forces a module structure for all exported routes
export type RouteHandlers = {
    GET: RequestHandler
    POST: RequestHandler
}
