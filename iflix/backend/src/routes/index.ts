import { Request, Response } from 'express';
import { Handlers } from './@types';
import content from './content'
import rating from './rating'

export const NotImplemented = (req: Request, res: Response) =>
    res
        .status(400)
        .send({message: "Route does not exist"})


export default <Handlers>{
    content,
    rating,
}
