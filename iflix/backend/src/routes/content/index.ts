import GET from './get'
import POST from './post'
import { RouteHandlers } from '../@types';

export default <RouteHandlers>{
    GET,
    POST,
}
