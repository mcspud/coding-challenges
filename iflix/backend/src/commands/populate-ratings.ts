import { UserIdBoundary, RatingBoundary } from '../@domain';
import { Content as ContentType } from '../@domain'
import { UpdateScore } from '../routes/rating/post'

import Faker from 'faker'
import Connect from '../connector'
import Data from '../data'


const createRating = (content: ContentType) =>
    new Data
        .ratings
        .model({
            contentId: content.contentId,
            userId: Faker.random.number({ min: UserIdBoundary.MIN, max: UserIdBoundary.MAX }),
            rating: Faker.random.number({ min: RatingBoundary.MIN, max: RatingBoundary.MAX }),
        })
        .save()
        .then(UpdateScore)


const populateRecords = async () => {
    const contents = await Data.content.model.find({})
    await Promise.all(contents.map(createRating))
    await Promise.all(contents.map(createRating))
    await Promise.all(contents.map(createRating))
    await Promise.all(contents.map(createRating))
    await Promise.all(contents.map(createRating))
    process.exit()
}


Connect.RATINGS_DB().then(populateRecords)
