import { Genre, Content as ContentType } from '../@domain';


import Mongoose from 'mongoose'
import Faker from 'faker'
import Connect from '../connector'
import Data from '../data'


const createContentItem = (genre: Genre, id: number) => new Data.content.model({
    contentId: id,
    title: Faker.random.words(5),
    genre: genre,
})
    .save()
    .catch(console.log)

const Range = (range: number): Array<number> =>
    Array(range).fill(null).map((_, i) => i)

const populateRecords = async () => {
    Mongoose.connection.db.dropDatabase();
    let contentId = 100000

    const action = Range(10).map(async val => {
        contentId += 1
        return await createContentItem('Action', contentId)
    })
    const horror = Range(10).map(async val => {
        contentId += 1
        return await createContentItem('Horror', contentId)
    })
    const scifi = Range(10).map(async val => {
        contentId += 1
        return await createContentItem('Sci-fi', contentId)
    })

    await Promise
        .all(action.concat(horror).concat(scifi))
        .then(console.log)
        .then(_ => process.exit())
}


Connect.RATINGS_DB().then(populateRecords)
