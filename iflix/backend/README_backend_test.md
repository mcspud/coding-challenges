<img height="150px" src="https://happypatowens.com/public/iflix_logo.png">

##  Backend ~~test~~ coding experiment

Hi there! we're so excited that you're applying for a role in the iflix engineering team. 
So that we can get an idea about how you problem solve and write code, we have a small 
coding experiment we'd love you to try. 


#### The Task

We'd love you to write a RESTful API that accepts 5 star ratings for our iflix titles.

Our visitors will be presented with an opportunity to rate a title out of 5 stars after 
viewing. We'd also like to display the average star rating on our titles, without outliers 
or small sample-sizes giving an unfair rating for newly added content.

The client has the following information available to send to your endpoints, in any format you choose. 

For rating content: 

```json
{ 
  "userId" : 4132246,
  "contentId" : 562135,
  "rating" : 3
}
```

For retrieving content:

```json
{ 
  "contentId" : 562135
}
```

#### What we're looking for

Don't stress! take your time, it's not a race.  We're not looking for perfection but a
well-considered MVP that we could further develop for production. Imagine this is a 
tech-spike to explore the concept which we will test on a small percentage (1-2 million)
users.

Things to consider:

* Appropriate error handling
* Tests
* Select a robust data storage option that performs well for high-read
* Clear maintainable code with good use of commenting
* A well documented approach (in your README) that an implementing team can follow after the tech-spike.
* Select a language and tool-chain appropriate for the role you are applying for, if unsure speak with your interviewer. 

Good luck!
