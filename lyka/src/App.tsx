import { Component, Show, JSXElement, createEffect } from "solid-js";
import { createStore } from "solid-js/store";
import { useKeyDownList } from "@solid-primitives/keyboard";
import { ArrowDown, ArrowLeft, ArrowRight, ArrowUp } from "./icons";

// Type tags for your friendly neighbourhood compiler

const _dimension: unique symbol = Symbol("Dimension")
const _robot: unique symbol = Symbol("Robot");
const _position: unique symbol = Symbol("Position");
const _positionX: unique symbol = Symbol("PositionX");
const _positionY: unique symbol = Symbol("PositionY");
const _action: unique symbol = Symbol("Action")
const _crate: unique symbol = Symbol("Crate")


// Represents a dimension of the grid.  We construct this 
// with a brand to make sure that we can safely curry the 
// bounds checks later on. 
//
// Exported for testing

type Dimension = {
  readonly _tag: typeof _dimension
  readonly value: number
}

export const Dimension = (a: number): Dimension => ({
  _tag: _dimension,
  value: a,
})

// Our implemented Grid.  There is only one 0 value because 
// each axis projects from the same point.

const gridStart = Dimension(0)
const gridSizeX = Dimension(10);
const gridSizeY = Dimension(10)

// These are the 6 signals we can send

type Signal =
  | "N"
  | "S"
  | "E"
  | "W"
  | "G"
  | "D"

type SignalMove = Exclude<Signal, 'G' | 'D'> 
type SignalLift = Exclude<Signal, SignalMove> 

// We can only move in 2 directions, so we use this to 
// refine the N S and E W movements to a plane

type Axis =
  | "Horizontal"
  | "Vertical";



// INFO:  I'm leaving the original comments below, but I went 
//        and implemented Pointed, Equals and Semigroup into these 
//        types and made the code much, much, much safer.
//        -------------------------------------------------------
//        The EntityPosX and EntityPosY really should be 
//        promoted to a Pointed SemiGroup, however
//        without implementing a full spec including Functors,
//        Applicatives and Eithers its just a nice-to-have 
//        at this point.  The the only association I'm doing is 
//        in the update position command so assuming all my 
//        types are tight enough we should be able to look in 
//        that one spot for any weird errors - ie the compiler 
//        does 99% of our work for us :) 
//
//        In any serious/real application you'd use the
//        ADTs from a library like fp-ts to construct
//        this with typesafe combinators.
//
//        I've gone and added in some Ring combinators (equality)
//
//        🤷
//
// We brand this type to make sure 
// that we don't accidentally mix up assigning boxed
// values.  At any one computation we need to determine 
// that the step size, direction and position, so 
// we need to keep our coordinate system and their 
// combinators tight

type Position = {
  readonly _position: typeof _position
  readonly value: number;
}

// Horizontal axis.  

export type EntityPosX = Position & {
  readonly _tag: typeof _positionX;

  eq: (x: EntityPosX) => boolean   
  neq: (x: EntityPosX) => boolean   
  concat: (x: EntityPosX) => EntityPosX
};

export const EntityPosX = (value: number): EntityPosX => ({
  _position: _position,
  _tag: _positionX,
  value,

  eq: x => x.value === value,
  neq: x => x.value !== value, 
  concat: x => EntityPosX(x.value + value),
})

EntityPosX.of = (x: number) => EntityPosX(x)


// Vertical axis. 

export type EntityPosY = Position & {
  readonly _tag: typeof _positionY;
  
  eq: (x: EntityPosY) => boolean   
  neq: (x: EntityPosY) => boolean   
  concat: (x: EntityPosY) => EntityPosY
};

export const EntityPosY = (value: number): EntityPosY => ({
  _position: _position,
  _tag: _positionY,
  value,

  eq: x => x.value === value,   
  neq: x => x.value !== value,
  concat: x => EntityPosY(x.value + value),
})

EntityPosY.of = (x: number) => EntityPosY(x)


// An action represents a movement action.  Paired with an 
// axis to work on it lets us traverse the plane.  
//
// We limit our step size to something reasonable.  There 
// is a type seam here that we should really use to infer 
// that the value is always between the grid bounds, but 
// I don't think that infered type generation is possible 
// in typescript.

type Action = {
  readonly _tag: typeof _action;
  readonly value: -1 | 1
}

export const Action = (value: Action["value"]): Action => ({
  _tag: _action,
  value: value
})

export const movePlus1 = Action(1)
export const moveMinus1 = Action(-1)

// These represent our boundary checks.  The upperbound is < rather than <= 
// because our grid size of 10 starts at index 0, meaning our upper bounded
// limit is 9 and not 10.  This intuition does not hold for the lower
// bounds.
// These are curried to pure functions because this lets us specify a grid of 
// arbitrary size, like 20 wide and 10 high in a scenario where that might 
// matter.
// These are exported for testing reasons.

export const lowerBounds = (d: Dimension) => (a: Action) => (p: Position) => (p.value + a.value) >= d.value
export const upperBounds = (d: Dimension) => (a: Action) => (p: Position) => (p.value + a.value) < d.value

// A MoveSignal constructor, returning a signal to be processed.
// This data has the direction it is going in (the axis), as well as
// limit of what that can be.  This limit data lets us construct grids
// that arent square, with the tradeoff that we have to build our
// types a little more verbosely.

type MoveSignal = {
  action: Action,
  axis: Axis;
  canMove: (p: Position) => boolean
};

// A MoveSignal constructor, returning a signal to be processed.
// 
// action:      How big the move amount will be, letting us put a fully sick 
//              supercharger on our entity and making it move 9 spaces at once
// axis:        Plane in which the entity moves
// canMove:     Checks if our movement will take us out of bounds.  There are 
//              prebuilt movement checks included.

const MoveSignal =
  (
    action: MoveSignal["action"],
    axis: MoveSignal["axis"],
    canMove: (a: Action) => MoveSignal["canMove"]
  ) => ({ action, axis, canMove: canMove(action) });


// Our robot itself.  
// We need to make sure that it has a unique identity in the type system, otherwise 
// if we add more entities to the system we can lose track of them very fast.

export type Robot = {
  readonly _tag: typeof _robot;
  readonly positionX: EntityPosX;
  readonly positionY: EntityPosY;
  readonly isLifting: boolean;
};

export const Robot = (positionX: EntityPosX, positionY: EntityPosY, isLifting: boolean = false): Robot => ({
  _tag: _robot,
  positionX,
  positionY,
  isLifting
});


// Updates your robot! (actually creates a new one...)
// Typesafe using inferred generics 

export const updateRobot = <K extends keyof Robot>(key: K, value: Robot[K], robot: Robot): Robot => ({
  ...robot,
  [key]: value,
})


// The function that moves the robot.  If we modify the robot we always return a new robot 
// This allows for a nice immutable data stream, meaning  we can do cool stuff later like 
// backtrack changes to state via a rewind due to no having to track mutated references 

function moveRobot(signal: MoveSignal, robot: Robot): Robot {
  switch (signal.axis) {
    case "Horizontal":
      const canMoveX = signal.canMove(robot.positionX)
      const posX = robot.positionX.concat(EntityPosX.of(signal.action.value))
      return canMoveX ? updateRobot("positionX", posX, robot) : robot
    case "Vertical":
      const canMoveY = signal.canMove(robot.positionY)
      const posY = robot.positionY.concat(EntityPosY.of(signal.action.value)) 
      return canMoveY ? updateRobot("positionY", posY, robot) : robot
    default:
      // Exhaustive type checking
      return signal.axis satisfies never
  }
}

// Warehouse crate
export type Crate = {
  readonly _tag: typeof _crate;
  readonly positionX: EntityPosX;
  readonly positionY: EntityPosY;
};

export const Crate = (positionX: EntityPosX, positionY: EntityPosY): Crate => ({
  _tag: _crate,
  positionX,
  positionY,
});


// Checks to see if a robot is over a crate.  Super rad O(n) performance - ideally we'd convert this 
// to a map and render, BUT then we have to maintain the map as a cache-like structure and update it 
// which adds an extra processing step, especially for the render cycle.
//
// It is something I'd do in a real system but not for a tech test.  This is a classic time-vs-space 
// tradeoff problem
const robotOverCrate = (r: Robot, c: Array<Crate>):boolean => {
  return c.map(crate => crate.positionX.eq(r.positionX) && 
                        crate.positionY.eq(r.positionY))
          .includes(true)
}

// Notice the use of predicate combination, ie:
// true && true   === true
// true && false  === false 
// false && true  === false 
// false && false === false 
// The intuition is a robot can only lift a crate if 
//  - its over a crate 
//  - its not currently lifting 
const canRobotLiftCrate = (r: Robot, c: Array<Crate>):boolean => {
  return robotOverCrate(r, c) === true && r.isLifting === false
}

// Actually lift the crate.  The intuition is that if a crate is being held by the robot then its 
// position is the same as the robot itself.  This means we don't need to keep 2 objects updated 
// in the database, we just remove it from the list and add a new crate in when we drop it later
const lift = (r: Robot, c: Array<Crate>):Array<Crate> => {
  return c.filter(crate => crate.positionX.neq(r.positionX) || crate.positionY.neq(r.positionY)) 
}


// Same use of predicate combination above, ie:
// true && true   === true
// true && false  === false 
// false && true  === false 
// false && false === false 
// Intuition:  We can't drop a crate on another crate, and we can't drop a 
//             crate if we aren't holding one already.  Both must hold true 
//             for this operation to succeed 
const canRobotDropCrate = (r: Robot, c: Array<Crate>):boolean => {
  return robotOverCrate(r, c) === false && r.isLifting === true
}

// The inverse operation of lift.  Arrays are Pointed Monoids, which means:
//   - they have a concat method   (semigroup)
//   - they have an of constructor (pointed)
//   - they have an "empty" value  (monoid, in this case [].  [] + [A] === [A], in the same way 2 + 0 === 2) 
//
// We can utilise this feature of an Array to make new a crate based on the position of the crane and lift it into 
// an array, which we can then use concat on.  This operation returns a new list so we maintain immutability 
const drop = (r: Robot, c: Array<Crate>): Array<Crate> => {
    return c.concat(Array.of(Crate(EntityPosX(r.positionX.value), EntityPosY(r.positionY.value))))
}

// Lifts or drops a crate.  We need to modify multiple parts of the store here, with the intuition being:
// - We lift a crate and 
//    - toggle the robot status to isLifting === true 
//    - We remove the "lifted crate" from crates on the ground 
// - We drop a crate and
//    - toggle the robot status to isLifting === false
//    - We add the "lifted crate" back to crates on the ground 
function craneOperation(signal: SignalLift, store: Store): Store {
  switch(signal) {
    // "Grip the crate"
    case 'G':
      return canRobotLiftCrate(store.robot, store.crates) === true 
        ? ({robot: updateRobot("isLifting", true, store.robot), crates: lift(store.robot, store.crates)}) 
        : store 

    // "Drop the crate"
    case 'D':
      return canRobotDropCrate(store.robot, store.crates) === true
        ? ({robot: updateRobot("isLifting", false, store.robot) , crates: drop(store.robot, store.crates)})
        : store

    default:
      // Exhaustive type check
      return signal satisfies never;
  }

}
// Our warehouse.  
// Tracks the location of the robot

type Store = {
  robot: Robot
  crates: Array<Crate>
}


// Here we convert a command (like "N") into a movement instruction.

const moveSignalMap: { [K in SignalMove]: MoveSignal } = {
  N: MoveSignal(moveMinus1, "Vertical", lowerBounds(gridStart)),
  S: MoveSignal(movePlus1, "Vertical", upperBounds(gridSizeY)),
  E: MoveSignal(movePlus1, "Horizontal", upperBounds(gridSizeX)),
  W: MoveSignal(moveMinus1, "Horizontal", lowerBounds(gridStart)),
};


// We convert our MoveSignal map into a LazyCommand variant.
// At this point all execution steps are ready, and are just 
// waiting for the Store to be passed to trigger update.

type LazyCommand = (s: Store) => Store 

const commandMap: { [K in Signal]: LazyCommand } = {
  N: (s: Store) => ({...s, robot: moveRobot(moveSignalMap.N, s.robot)}), 
  S: (s: Store) => ({...s, robot: moveRobot(moveSignalMap.S, s.robot)}), 
  E: (s: Store) => ({...s, robot: moveRobot(moveSignalMap.E, s.robot)}), 
  W: (s: Store) => ({...s, robot: moveRobot(moveSignalMap.W, s.robot)}), 
  G: (s: Store) => craneOperation('G', s),
  D: (s: Store) => craneOperation('D', s),
}

// Our command parser.  It takes in a string, splits it apart, 
// performs some validations and generates a list of movement 
// commands.
//
// Exported for testing

export const commandParser = (sequence: string): Array<LazyCommand> =>
  sequence
    .split(' ')
    .map(s => s.toUpperCase())
    .filter((C: string): C is Signal => (['N', 'S', 'E', 'W', 'G', 'D'] as Signal[]).includes(C as Signal))
    .map(C => commandMap[C])


// Our "reactive store", provided by SolidJS.
// Exported for testing
export const [data, setData] = createStore<Store>({
  robot: Robot(EntityPosX(0), EntityPosY(0)),
  crates: [
    Crate(EntityPosX(2), EntityPosY(1)),
    Crate(EntityPosX(2), EntityPosY(2)),
    Crate(EntityPosX(4), EntityPosY(2)),
    Crate(EntityPosX(5), EntityPosY(9)),
    Crate(EntityPosX(5), EntityPosY(5)),
    Crate(EntityPosX(5), EntityPosY(4)),
    Crate(EntityPosX(8), EntityPosY(3)),
  ],
})

// Takes in a string and runs them as commands
//
// Exported for testing
export const runCommands = (signals: string) =>
  commandParser(signals)
  .forEach(command => setData(command(data)))


// Interface starts here

const pressedKeys = useKeyDownList();

// This function takes our keys (arrowup, h, k etc) and maps a signal (N, S, E or W) to them.
// This signal is pulled from a command map and sent to the robot for truth and justice.

const mapKeyToCommand = (actionKey: string, command: Signal) => {
  window.addEventListener("keydown", (event: KeyboardEvent) => {
    if (event.key.toUpperCase() === actionKey.toUpperCase()) {
      runCommands(command)
    }
  })
}

// These are our robot control keys.  Notice now we issue the same N S E W signals from different 
// keys to get the results we need

createEffect(() => {
  // VIM  
  mapKeyToCommand("H", 'W')
  mapKeyToCommand("J", 'S')
  mapKeyToCommand("K", 'N')
  mapKeyToCommand("L", 'E')

  // Arrow Keys
  mapKeyToCommand("ARROWLEFT", 'W')
  mapKeyToCommand("ARROWDOWN", 'S')
  mapKeyToCommand("ARROWUP", 'N')
  mapKeyToCommand("ARROWRIGHT", 'E')

  // Crane Actions
  mapKeyToCommand("G", 'G')
  mapKeyToCommand("D", 'D')
})

// Our warehouse

const grid = new Array(gridSizeX.value)
  .fill(null)
  .map(() => new Array(gridSizeY.value).fill(null));


// Determine if the robot is on the current grid location 

const robotOnGrid = (x: EntityPosX, y: EntityPosY): boolean => {
  const robot = () => data.robot
  return robot().positionX.eq(x) && robot().positionY.eq(y)
}

// Determine if this grid location has a Crate

const crateOnGrid = (x: EntityPosX, y: EntityPosY): boolean => {
  const crates = () => data.crates
  const results = crates().map(crate => crate.positionX.eq(x) && crate.positionY.eq(y))
  return results.includes(true)
}


// Only show the crate if: 
//  - this grid has one 
//  - the robot isn't on this location

const showCrate = (x: EntityPosX, y: EntityPosY): boolean => {
  return crateOnGrid(x, y) === true && robotOnGrid(x, y) === false 
}


// Only return true if: 
//  - this grid has a crate
//  - the robot is on this location
//  - the robot isnt currently lifting 

const displayCanRobotLiftCrateOnGrid = (x: EntityPosX, y: EntityPosY): boolean => {
  const robot = () => data.robot
  return canRobotLiftCrate(data.robot, data.crates) === true && robotOnGrid(x, y) && robot().isLifting === false 
}

// Only return true if: 
//  - this grid has no crate
//  - the robot is on this location
//  - the robot is currently lifting a crate 

const displayRobotCannotDropCrateOnGrid = (x: EntityPosX, y: EntityPosY): boolean => {
  const robot = () => data.robot
  return robotOnGrid(x, y) === true && crateOnGrid(x, y) === true && robot().isLifting === true  
}

// Default display for a grid background colour.  This is my first time using solid JS and its 
// not clear to me how to represent default values 😅

const defaultDisplay = (x: EntityPosX, y: EntityPosY): boolean => {
 return displayCanRobotLiftCrateOnGrid(x, y) === false && displayRobotCannotDropCrateOnGrid(x, y) === false
}


// A position on the grid. Will dynamically update to show our robot or crate

export function Grid(props: { x: EntityPosX; y: EntityPosY }) {
  return (
    <div classList={{
      "w-5 h-5 flex justify-center items-center ": true,
      "bg-lime-200": displayCanRobotLiftCrateOnGrid(props.x, props.y) === true,
      "bg-pink-300": displayRobotCannotDropCrateOnGrid(props.x, props.y) === true,
      "bg-teal-700": defaultDisplay(props.x, props.y) === true,
    }}>
      <Show when={robotOnGrid(props.x, props.y) === true}>🤖</Show >
      <Show when={showCrate(props.x, props.y) === true}>📦</Show >
    </div>
  );
}

// Reactive UI buttons for moving the robot around

export function MoveButton(props: { key: string; children: JSXElement }) {
  return (
    <div
      classList={{
        "w-10 h-10 flex justify-center items-center text-slate-800": true,
        "bg-yellow-200": pressedKeys().includes(props.key) === false,
        "bg-gray-200": pressedKeys().includes(props.key) === true,
      }}
    >
      {props.children}
    </div>
  );
}

// Main interface

const App: Component = () => {

  return (
    <div class="bg-slate-900 w-screen h-screen flex justify-center items-center  text-slate-950 min-w-3xl ">
      <div class="flex-col justify-center space-y-10">
        <div class={`grid grid-rows-10 grid-cols-10`}>
          {grid.map((_, y) => _.map((_, x) => <Grid x={EntityPosX(x)} y={EntityPosY(y)} />))}
        </div>

        <div class="space-y-2">
          <div class="text-yellow-200 text-xs">VIM</div>
          <div class="flex justify-center space-x-3">
            <MoveButton key={"H"}>H</MoveButton>
            <MoveButton key={"J"}>J</MoveButton>
            <MoveButton key={"K"}>K</MoveButton>
            <MoveButton key={"L"}>L</MoveButton>
          </div>
        </div>

        <div class="space-y-2">
          <div class="text-yellow-200 text-xs">Arrows</div>
          <div class="flex justify-center space-x-3">
            <MoveButton key={"ARROWLEFT"}><ArrowLeft /></MoveButton>
            <MoveButton key={"ARROWDOWN"}><ArrowDown /></MoveButton>
            <MoveButton key={"ARROWUP"}><ArrowUp /></MoveButton>
            <MoveButton key={"ARROWRIGHT"}><ArrowRight /></MoveButton>
          </div>
        </div>

        <div class="space-y-2">
          <div class="text-yellow-200 text-xs">Crane</div>
          <div class="flex justify-start space-x-3">
            <MoveButton key={"G"}>G</MoveButton>
            <MoveButton key={"D"}>D</MoveButton>
          </div>
        </div>

      </div>
    </div>
  );
};

export default App;
