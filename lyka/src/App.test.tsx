import { describe, expect, test } from "vitest";
import { render } from "@solidjs/testing-library";
import App, {
  Grid,
  Action,
  Dimension,
  EntityPosX,
  EntityPosY,
  lowerBounds,
  upperBounds,
  moveMinus1,
  movePlus1,
  data,
  setData,
  commandParser,
  runCommands,
  Robot,
  updateRobot,
} from "./App";


describe("<App />", () => {
  test("actually renders", async () => {
    render(() => <App />);
  });

  test("Robot exists on screen", async () => {
    const { getByText } = render(() => <App />);
    // Fails if old mate doesn't exist
    getByText('🤖')
  });
});


describe("<Grid />", () => {
  test("actually renders", async () => {
    render(() => <Grid x={EntityPosX(0)} y={EntityPosY(0)} />);
  });
});


describe("Invariants", async () => {
  // This testing block looks at the what we've configured as our invariants and makes
  // sure they meet our expectation.  Testing says that you shouldn't duplicate anything,
  // however I have a strong, strong disagreement with this statement when testing invariants.
  //
  // Invariants are your systems spine, and if you need a few tests to make sure that some
  // static values are consistently static then that is a tradeoff worth making
  describe("movement actions", async () => {
    test("moveMinus1 has correct value", () => {
      expect(moveMinus1.value).to.equal(-1);
    });

    test("movePlus1 has correct value", () => {
      expect(movePlus1.value).to.equal(1);
    });
  });

  describe("lowerBounds Function", () => {
    test("should return false for trying to move below grid boundary (Horizontal)", () => {
      const gridSizeX = Dimension(0);
      const moveAction = Action(-1);
      const robotX = EntityPosX(0);
      const canMove = lowerBounds(gridSizeX)(moveAction)(robotX);
      expect(canMove).toBe(false);
    });

    test("should return true for moving within grid boundaries (Horizontal)", () => {
      const gridSizeX = Dimension(0);
      const moveAction = Action(1);
      const robotX = EntityPosX(0);
      const canMove = lowerBounds(gridSizeX)(moveAction)(robotX);
      expect(canMove).toBe(true);
    });
  });

  describe("upperBounds Function", () => {
    test("should return false for trying to move above grid boundary (Vertical)", () => {
      const gridSizeY = Dimension(10);
      const moveAction = Action(1);
      const robotY = EntityPosY(9);
      const canMove = upperBounds(gridSizeY)(moveAction)(robotY);
      expect(canMove).toBe(false);
    });

    test("should return true for trying to move below grid boundary (Vertical)", () => {
      const gridSizeY = Dimension(10);
      const moveAction = Action(-1);
      const robotY = EntityPosY(9);
      const canMove = upperBounds(gridSizeY)(moveAction)(robotY);
      expect(canMove).toBe(true);
    });
  });
});

describe("Commands ", () => {
  test("CommandParser strips out useless signals", () => {
    const signals = 'A B C 1 2 3 4 { } [ ] + - ;'
    const commands = commandParser(signals)
    expect(commands.length).toBe(0)
  });

  test("CommandParser is case friendly", () => {
    const signals = 'n s e w g d'
    const commands = commandParser(signals)
    expect(commands.length).toBe(6)
  });

  test("RunCommands actually... runs... the commands", () => {
    const robot = Robot(EntityPosX(0), EntityPosY(9))
    setData("robot", robot)
    // check initial state
    expect(data.robot.positionX.value).toBe(0)
    expect(data.robot.positionY.value).toBe(9)
    // update state
    const signals = 'n e n e n e n e'
    runCommands(signals)
    expect(data.robot.positionX.value).toBe(4)
    expect(data.robot.positionY.value).toBe(5)
  })
})

describe("Robot", () => {
  test("updateRobot updates positionX", () => {
    const robot = Robot(EntityPosX(0), EntityPosY(9))
    const newRobot = updateRobot("positionX", EntityPosX(5), robot)

    expect(newRobot.positionX.value).toEqual(5)
  })

  test("updateRobot updates positionY", () => {
    const robot = Robot(EntityPosX(0), EntityPosY(5))
    const newRobot = updateRobot("positionY", EntityPosY(5), robot)

    expect(newRobot.positionY.value).toEqual(5)
  })
})
