## Usage

Built against `node v20`.

To setup

```bash
$ npm install 
$ npm run dev 
```

Navigate to `http:localhost:3000` to see your new robot.

###  Tests

There are `vitest` unit tests and `playwright` e2e tests.

```bash
$ npm run test
$ npx playwright test --ui
```

These are configured to be in watch mode to so can be run in parallel whilst working.  You'll need to have `npm dev run` executing in a separate terminal when running `playwright` tests.

![](./tests.png)
