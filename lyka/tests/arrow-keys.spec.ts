import { test, expect } from "@playwright/test";

test("test robot cannot go off screen", async ({ page }) => {

  // presses the same key 20 times - helps us hit boundary checks
  const presser = async (key: string) => {
    for (let i = 0; i < 20; i++) {
      await page.keyboard.press(key);
    }
  };

  await page.goto("http://localhost:3000/");

  await expect(page.getByText("🤖")).toBeVisible();

  // Move to bottom of grid:
  await presser("ArrowDown");
  await expect(page.getByText("🤖")).toBeVisible();
  await expect(page.locator('div:nth-child(91)')).toHaveText("🤖")

  // Move to bottom of grid:
  await presser("ArrowRight");
  await expect(page.getByText("🤖")).toBeVisible();
  await expect(page.locator('div:nth-child(100)')).toHaveText("🤖")

  // Move to top of grid:
  await presser("ArrowUp");
  await expect(page.getByText("🤖")).toBeVisible();
  await expect(page.locator('div:nth-child(10)')).toHaveText("🤖")

  // Move to left of grid:
  await presser("ArrowLeft");
  await expect(page.getByText("🤖")).toBeVisible();
  await expect(page.locator('.w-5').first()).toHaveText("🤖")
});
