import { test, expect } from "@playwright/test";


test ("vim keys are visible", async ({ page }) => { 

  await page.goto("http://localhost:3000/");

  await expect(page.getByText('H')).toBeVisible();
  await expect(page.getByText('J')).toBeVisible();
  await expect(page.getByText('K')).toBeVisible();
  await expect(page.getByText('L')).toBeVisible();
})

const keyPresser = (key: string):string => 
  Array(20).fill(key).join('')

test("test robot cannot go off screen", async ({ page }) => {


  await page.goto("http://localhost:3000/");

  await expect(page.getByText("🤖")).toBeVisible();

  // Move to bottom of grid:
  await page.keyboard.type(keyPresser('J'));
  await expect(page.getByText("🤖")).toBeVisible();
  await expect(page.locator('div:nth-child(91)')).toHaveText("🤖")

  // Move to right of grid:
  await page.keyboard.type(keyPresser('L'));
  await expect(page.getByText("🤖")).toBeVisible();
  await expect(page.locator('div:nth-child(100)')).toHaveText("🤖")

  // Move to top of grid:
  await page.keyboard.type(keyPresser('K'));
  await expect(page.getByText("🤖")).toBeVisible();
  await expect(page.locator('div:nth-child(10)')).toHaveText("🤖")

  // Move to left of grid:
  await page.keyboard.type(keyPresser('H'));
  await expect(page.getByText("🤖")).toBeVisible();
  await expect(page.locator('.w-5').first()).toHaveText("🤖")
});
