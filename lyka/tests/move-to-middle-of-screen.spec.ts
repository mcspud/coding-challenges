import { test, expect } from "@playwright/test";

test("test robot can move to middle of screen", async ({ page }) => {
  await page.goto("http://localhost:3000/");

  await expect(page.getByText("🤖")).toBeVisible();

  // Move to bottom of grid:
  await page.keyboard.type("j");
  await page.keyboard.type("j");
  await page.keyboard.type("j");
  await page.keyboard.type("j");
  await page.keyboard.type("j");
  await page.keyboard.type("j");
  await page.keyboard.type("j");
  await page.keyboard.type("j");
  await page.keyboard.type("j");
  await expect(page.getByText("🤖")).toBeVisible();
  await expect(page.locator("div:nth-child(91)")).toHaveText("🤖");

  // Tech test said:
  // If the robot starts in the south-west corner of the warehouse then the following commands will move it to the middle of the warehouse.
  //  N E N E N E N E
  // Using the arrow keys we'll map that command sequence

  await page.keyboard.press('ArrowUp')
  await page.keyboard.press('ArrowRight')
  await page.keyboard.press('ArrowUp')
  await page.keyboard.press('ArrowRight')
  await page.keyboard.press('ArrowUp')
  await page.keyboard.press('ArrowRight')
  await page.keyboard.press('ArrowUp')
  await page.keyboard.press('ArrowRight')

  await expect(page.getByText("🤖")).toBeVisible();
  // one of the middle panels
  await expect(page.locator("div:nth-child(55)")).toHaveText("🤖");
});
