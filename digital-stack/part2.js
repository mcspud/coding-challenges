// All of these examples were done live on syncfiddle, with the other party on 
// the end of the phone.  They could see what I typed.

// After part 1 they said not to bother with this but I did it anyway because it
// was so quick

// Find all people who are younger than 50

data = [
    { 
        name: 'Peter',
        age: 61,
    },{ 
        name: 'Paul',
        age: 22,
    },{ 
        name: 'Jack',
        age: 50,
    },{ 
        name: 'Simon',
        age: 41,
    }
]

const solution = people =>
    people.filter(person => person.age < 50)
    
result = solution(data)