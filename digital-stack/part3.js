// All of these examples were done live on syncfiddle, with the other party on 
// the end of the phone.  They could see what I typed.

// How would you make this code cleaner?
var movable = document.getElementById('movable');
movable.style.position = "absolute";
movable.style.left = "0px";
movable.style.top = "0px";

  
function performAnimation() {
	setTimeout(function() {
    movable.style.left = '5px'
    movable.style.top = '5px'
	}, 200)
  
  setTimeout(function() {
    movable.style.left = '10px'
    movable.style.top = '10px'
	}, 400)
  
  setTimeout(function() {
    movable.style.left = '15px'
    movable.style.top = '15px'
	}, 600)
  
  setTimeout(function() {
    movable.style.left = '20px'
    movable.style.top = '20px'
	}, 800)
  
  setTimeout(function() {
    movable.style.left = '25px'
    movable.style.top = '25px'
	}, 1000)
}
  
performAnimation();

// Answer
const performAnimation2 = () => {
    let start = 0;
  
    let myExicitingInterval = setInterval(() => {
    movable.style.left = `${start * 5}px`;
    movable.style.top = `${start * 5}px`;
    
    if ( start === 5) clearInterval(myExicitingInterval);
    start += 1;
	}, 200)
}
  
performAnimation2();