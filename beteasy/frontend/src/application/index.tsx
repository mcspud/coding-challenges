import { useSelector } from 'react-redux'
import { State } from '../data';

import React from 'react'
import Data from '../data'
import NavBar from './NavBar'
import LoadingIcon from './LoadingIcon'
import Events from './Events'

const LoadData = () =>
    Data.store.dispatch(
        Data.creators.events.load_events.START()
    )

export default () => {
    const lifecycle = useSelector((state: State) => state.events.lifecycle)

    return (
        <main>
            <NavBar onClick={LoadData} />
            <div className="container">
                <div className="columns">
                    <h2 className='text-beteasy-primary p-centered'>Next To Jump</h2>
                </div>
                <div className="columns">
                    {lifecycle.loading && <LoadingIcon />}
                    {lifecycle.error && <div className='text-error p-centered'> Error loading content</div>}
                </div>
                <div className="columns">
                    <div className="col-xs-1 col-sm-1 col-md-2 col-lg-3 col-xl-3 col-3"></div>
                    <div className="col-xs-10 col-sm-10 col-md-8 col-lg-6 col-xl-6 col-6">
                        <Events />
                    </div>
                    <div className="col-xs-1 col-sm-1 col-md-2 col-lg-3 col-xl-3 col-3"></div>
                </div>
            </div>
        </main>
    )
}
