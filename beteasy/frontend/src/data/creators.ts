import events from './events/creators'
import system from './system/creators'

export default {
    events,
    system,
}
