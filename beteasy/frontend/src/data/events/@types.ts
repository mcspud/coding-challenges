import { Event } from '../../@domain'


export type NextToJumpResponse = {
    result: Array<Event>
    success: boolean
}
