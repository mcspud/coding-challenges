import * as R from 'ramda'
import { AnyAction } from 'redux';

import observables from '../observables'
import actions from './actions'
import creators from './creators'

import systemActions from '../system/actions'
import systemCreators from '../system/creators'

const getAllContent = observables.GET(
    actions.load_events.START,
    creators.load_events.SUCCESS,
    creators.load_events.ERROR,
);


const loadContentOnInit = observables.DEPENDENT(
    systemActions.loaded.INITIALISED,
    creators.load_events.START
)


const refreshContent = observables.DEPENDENT(
    systemActions.clock.UPDATED,
     R.ifElse(
        (action: AnyAction & {tick: number}) => action.tick !== 0 && action.tick % 10 === 0,
        creators.load_events.START,
        systemCreators.NOOP,
     )
)


export default {
    getAllContent,
    loadContentOnInit,
    refreshContent,
}
