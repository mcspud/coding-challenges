import { State, Event as EventRecord } from './@records'
import { AnyAction } from 'redux'
import { AsyncCreatorSuccess, AsyncCreatorGet, AsyncCreatorError } from '../@types'
import { EventId } from '../../@domain'
import { NextToJumpResponse } from './@types'

import * as I from 'immutable'
import actions from './actions'


export const loadEventStart = (state: State, action: AsyncCreatorGet) =>
    state
        .update('lifecycle', lifecycle => lifecycle.merge({
            loading: true,
            success: false,
            error: false,
        }))

export const loadEventSuccess = (state: State, action: AsyncCreatorSuccess<NextToJumpResponse>) =>
    state
        .update('lifecycle', lifecycle => lifecycle.merge({
            loading: false,
            success: true,
            error: false,
        }))
        .update('data', map => map.merge(
            action
                .data
                .result
                .map(EventRecord)
                .reduce((acc, val) => acc.set(val.EventID, val), I.Map<EventId, EventRecord>())
        ));


export const loadEventError = (state: State, action: AsyncCreatorError) =>
    state
        .update('lifecycle', lifecycle => lifecycle.merge({
            loading: false,
            success: false,
            error: true,
        }));


export default (state = State(), action: AnyAction & any) => {
    switch (action.type) {
        case actions.load_events.START: return loadEventStart(state, action)
        case actions.load_events.SUCCESS: return loadEventSuccess(state, action)
        case actions.load_events.ERROR: return loadEventError(state, action)
        default: return state
    }
}
