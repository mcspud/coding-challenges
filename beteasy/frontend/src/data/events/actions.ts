import { AsyncActions } from "../@types";

export default {
    load_events: <AsyncActions>{
        START: '@events/LOAD_EVENTS_START',
        SUCCESS: '@events/LOAD_EVENTS_SUCCESS',
        ERROR: '@events/LOAD_EVENTS_ERROR',
    }
}
