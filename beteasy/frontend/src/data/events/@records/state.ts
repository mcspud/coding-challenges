import { Map, Record, RecordOf } from 'immutable'
import { Lifecycle } from "../../@records/lifecycle";
import { EventId } from '../../../@domain';
import { Event } from './event';


type _State = {
    data: Map<EventId, Event>
    lifecycle: Lifecycle
}


export type State = RecordOf<_State>
export const State: Record.Factory<_State> = Record({
    data: Map<EventId, Event>(),
    lifecycle: Lifecycle()
});
