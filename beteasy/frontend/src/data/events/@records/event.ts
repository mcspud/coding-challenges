import { Record, RecordOf } from 'immutable';
import { Event as _Event, Empty } from "../../../@domain";

import { EventType } from './eventtype'
import { Venue } from './venue'

const defaultValues: _Event = {
    EventID: Empty.NUMBER,
    MasterEventID: Empty.NUMBER,
    EventName: Empty.STRING,
    EventTypeDesc: Empty.STRING,
    MasterEventName: Empty.STRING,
    AdvertisedStartTime: Empty.STRING,
    RaceNumber: Empty.NUMBER,
    EventType: EventType(),
    Venue: Venue(),
    IsMultiAllowed: false,
    Slug: Empty.STRING,
    DateSlug: Empty.STRING,
    RacingStreamAllowed: false,
    RacingReplayStreamAllowed: false,
};


export type Event = RecordOf<_Event>
export const Event: Record.Factory<_Event> = Record(defaultValues);
