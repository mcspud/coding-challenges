import { Record, RecordOf } from 'immutable';
import { EventType as _EventType, Empty } from "../../../@domain";


const defaultValues: _EventType = {
    EventTypeID: Empty.NUMBER,
    MasterEventTypeID: Empty.NUMBER,
    Slug: Empty.STRING,
    EventTypeDesc: Empty.STRING
};


export type EventType = RecordOf<_EventType>
export const EventType: Record.Factory<_EventType> = Record(defaultValues);
