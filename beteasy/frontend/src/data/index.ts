import creators from './creators'
import epics from './epics'
import reducers from './reducers'
import store from './store'
import timer from './system/timer'

export { State } from './@records'


export default {
    creators,
    epics,
    reducers,
    store,

    timer
}
