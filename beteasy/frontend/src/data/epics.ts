import { combineEpics } from 'redux-observable';

import events from './events/epics'

export default combineEpics(
    events.getAllContent,
    events.loadContentOnInit,
    events.refreshContent,
)
