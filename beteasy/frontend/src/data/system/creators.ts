import actions from './actions'
import { Action, AnyAction } from 'redux';

const applicationInitialised = (): Action => ({
    type: actions.loaded.INITIALISED
})

const clockUpdated = (tick: number): AnyAction & {tick: number} => ({
    type: actions.clock.UPDATED,
    tick: tick,
})

const NOOP = (): Action => ({
    type: '@system/NOOP'
})

export default {
    applicationInitialised,
    clockUpdated,
    NOOP,
}
