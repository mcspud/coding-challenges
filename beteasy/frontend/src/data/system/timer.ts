import { interval } from 'rxjs';
import Store from '../store'
import creators from './creators'

const source = interval(1000);

export default source.subscribe(tick => Store.dispatch(creators.clockUpdated(tick)))
