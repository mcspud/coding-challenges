import { combineReducers } from 'redux-immutable';
import events from './events/reducers'

export default combineReducers({
    events,
})
