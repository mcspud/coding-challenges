import 'spectre.css'
import 'icons.css'
import './index.scss'

import React from 'react'
import ReactDOM from 'react-dom'
import App from './application'
import Data from './data'
import { Provider } from 'react-redux'

const Application = () =>
    <Provider store={Data.store}>
        <App />
    </Provider>

Data.store.dispatch(Data.creators.system.applicationInitialised())

ReactDOM.render(<Application />, document.getElementById('root'));
