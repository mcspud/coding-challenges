### Rejected

I got denied for this role.  Their feedback (via the phone) was as follows:

- My backend solution (~45 significant lines of code) didn't implement "best practice"
- I didn't have "separation of concerns" - ie. it was all in 1 file
- I didnt have tests to show it "worked"

I'd like to point out that this company is a .NET shop.  I am an experienced functional programmer but I have limited exposure to .NET; so I used F#, which is part of the .NET (Core) runtimes but is really a dialect of Ocaml.  Their challenge README said I can use any language I want to do this challenge.

The solution uses **Type Providers** (thats those *FSharp.Data.XMLProvider* and *FSharp.Data.JsonProvider* things) to generate out a list of static types at compile time, and ALSO implements a runtime validator using "sample" data.  You literally point the *Provider* at a data source and the compiler generates you out compile time checks and runtime validators in a single line of code.  Yes, you read that right.  The **compiler**.  

You can see that the actual solution logic is fairly straight forward - 3 functions in total.  The first two functions load the data from the `.xml` and `.json` files respectively, and convert them into the data type `List Horse`.  The final function takes these two lists of `Horse`, concats them together producing a homogenous list of the same type, arranges them by `Price` ascending order, and then converts them to a `Log` type (which shows `HorseName` and `Price`) and which is the final result that is printed to the screen.  Since I am using clearly defined `Record` types here, the compiler is automatically testing every. single. piece. of. code. for me, except the bit where I actually print it to the screen.  This is a large part of what compilers are meant to do - keep you honest.

I'm guessing what they meant as "best practice" was to manually hand hack the domain into a set of C# classes, have modules/namespaces/packages for each of the responses, and to manually define the validators to mix it all together, having a *Program* class and a *main* method that opens the files and deserialises them into C# objects.  Instead of doing that, I wrote 2 lines of code (one for each file) and let the compiler do it all for me automatically.  

🤷‍♂️

<!-- 
Looking at the code in *src/App/Program.fs*, you can see that every entry point to the code runs through `<ProviderType>.Parse`,  which is the validator method, and then any optional nulls have to be accounted for or the whole fucking thing wont compile.

It is obvious that these guys are **not** a .NET shop - they are your standard mediocre OO programmers whom have a vanishingly small understanding of the very tools and runtime that they use, and god forbid if you let the tools automate away your informal, buggy, error prone code.
-->
