# .NET Code Challenge

## Backend (.NET/F#)

- You will need .net core 2.2 installed.
- Navigate to `/backend/FsRaces/src/App`
- Open a terminal in your OS of choice and run the following commands, assuming the `dotnet` alias is already on your `$PATH`

```
dotnet restore
dotnet build
dotnet run
```

A table will print to the terminal, showing the horses name and their betting price in ascending order.