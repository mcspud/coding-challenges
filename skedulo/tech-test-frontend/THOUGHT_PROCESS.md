# Specify node environment.

This test doesn't work on node 17+.  I've added a `.nvmrc` file so that the correct version of node can be used easily 

### Question 1

All UI interactions can be modelled cleanly as state machines.   I've gone and used a state machine library to implement this solution.  State machines let us extract almost logic from the view, and by leaning heavily on types we get a much better developer experience.

I have made a few accomodations here:

- Nothing happens until 3 or more characters are typed
- There is a half second debounce before the search triggers
- Searching while the search triggers will cause the promise result to be discarded.

There is no pressing a return key to search - it is driven purely by input length and frequency.


### Question 2


I used a state machine (again!) to orchestrate getting the required values.  Normally you'd use parallel states insead of the big `Promise.all()`, however I purposely did this to show how essentially all logic can be moved into a defined schematic (state machine), even if you are combining the results of 5 promise requests.

Normally I would have it in there so you can handle each request individually including failure states, but there was no point here since it was all local.  I've put in a demo implementation of this below for reference, as it is a formalised implementation that I believe represents true best practice.

```ts 
const resourceMachine = createMachine<Context>({
  id: 'resource-machine',
  initial: 'loading',
  predictableActionArguments: true,
  context: {
    jobAllocations: [],
    activityAllocations: [],
    activities: [],
    resources: [],
    jobs: [],
    schedule: [],
  },
  states: {
    loading: {
      type: 'parallel',
      states: {
        getJobAllocations: {
          invoke: {
            src: getJobAllocations,
            onDone: {
              target: 'done',
              actions: [assign((c, e: DoneInvokeEvent<Context['jobAllocations']>) => ({ ...c, jobAllocations: e.data }))]
            }
          }
        },
        getActivityAllocations: {
          invoke: {
            src: getActivityAllocations,
            onDone: {
              target: 'done',
              actions: [assign((c, e: DoneInvokeEvent<Context['activityAllocations']>) => ({ ...c, activityAllocations: e.data }))]
            }
          }
        },
        getActivities: {
          invoke: {
            src: getActivities,
            onDone: {
              target: 'done',
              actions: [assign((c, e: DoneInvokeEvent<Context['activities']>) => ({ ...c, activities: e.data }))]
            }
          }
        },
        getResources: {
          invoke: {
            src: getResources,
            onDone: {
              target: 'done',
              actions: [assign((c, e: DoneInvokeEvent<Context['resources']>) => ({ ...c, resources: e.data }))]
            }
          }
        },
        getJobs: {
          invoke: {
            src: getJobs,
            onDone: {
              target: 'done',
              actions: [assign((c, e: DoneInvokeEvent<Context['jobs']>) => ({ ...c, jobs: e.data }))]
            }
          }
        },
      },
      allDone: {
        ...
      }
    }
  }
})

```

The great thing about parallel state machines is that you can easily lean on the event loop runtime of the browser/node, 
which unfortunately many people don't really take advantage of.  I have grave concerns when developers talk only about the language - they are ignoring their runtime which is the other half the story.  

# Question 3 

I used `tailwind` to style this.  It is very hard to go back to anything else after using it :)  Besides that this was a relatively standard component/data implementation.
