import React from "react"
import { IAppTabContainer, Job as JobT, JobAllocations } from "../common/types"


import { DataService } from "../service/DataService"

import "./QuestionThree.output.css"


const Header = (props: { children: React.ReactNode }) => (

  <div className="bg-[#3d9bff] w-full p-5 text-white">
    <div className="space-y-3 flex text-3xl">
      {props.children}
    </div>
  </div>
)

const Job = (props: { job: JobT, allocations: Array<JobAllocations> }) => {

  // turn this into a set incase we get duplicates.
  const resourceCount = props.allocations
    .filter(allocation => allocation.jobId === props.job.id)
    .reduce((acc, v) => acc.add(v), new Set())
    .size

  const jobDate = new Date(props.job.start).toDateString()
  const jobStart = new Date(props.job.start).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })
  const jobEnd = new Date(props.job.end).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })

  return (
    <div className="bg-white p-4">
      <div>
        <span className="text-[#223048] font-bold">{props.job.name}</span>
        <span className="text-[#8d8383] pl-3">(Job #{props.job.id})</span>
      </div>
      <div className="text-[#8d8383] text-sm">{props.job.location}</div>
      <div className='flex justify-between'>
        <div>
          <div className="mt-3 text-[#8d8383] text-sm">
            {jobDate}
          </div>
          <div className="text-[#8d8383] text-sm">
            {`${jobStart} - ${jobEnd}`}
          </div>
        </div>
        {resourceCount > 0 &&
          <div className='rounded-full border border-blue-300 w-10 h-10 flex justify-center items-center text-blue-300'>{resourceCount}</div>
        }
      </div>

    </div>
  )
}

export const QuestionThree: React.FC<IAppTabContainer> = () => {
  const dynamicHeaderContent = () => 'Dynamic Header Content'
  const [jobs, setJobs] = React.useState<Awaited<ReturnType<typeof DataService.getJobs>>>([])
  const [allocations, setAllocations] = React.useState<Awaited<ReturnType<typeof DataService.getJobAllocations>>>([])

  React.useEffect(() => {
    const getJobs = async () => {
      const data = await DataService.getJobs()
      setJobs(data)
    }

    getJobs()

    const getAllocations = async () => {
      const data = await DataService.getJobAllocations()
      setAllocations(data)
    }

    getAllocations()
  }, [])

  return (
    <div className="fixed w-screen">
      <Header>{dynamicHeaderContent()}</Header>
      <div className="flex h-screen z-0">

        <div className="p-2 bg-[#223048]">
          <div className="space-y-3 flex flex-col justify-between">
            <div className="flex justify-center">
              <div className="w-12 h-12 rounded-full bg-[#c4c4c4] w-full" />
            </div>

            <div className="flex justify-center">
              <div className="w-12 h-12 rounded-full bg-[#c4c4c4] w-full" />
            </div>

            <div className="flex justify-center">
              <div className="w-12 h-12 rounded-full bg-[#c4c4c4] w-full" />
            </div>

            <div className="flex justify-center">
              <div className="w-12 h-12 rounded-full bg-[#c4c4c4] w-full" />
            </div>
          </div>
        </div>
        <div className="flex flex-col overflow-y-auto bg-[#e4eef1] p-5 border-r border-gray-300 space-y-4 w-4/12">
          {jobs.map(job => <Job job={job} key={job.id} allocations={allocations} />)}
        </div>
        <div className="flex-1 overflow-y-auto bg-white p-4 space-y-4">
          {Array(100).fill(null).map((_, i) =>
            <div className="bg-[#e4eef1] h-24 w-2/2" key={i}></div>
          )}
        </div>
      </div>

    </div>
  )
}
