import React from "react"

export const SectionGroup = (props: {children: React.ReactNode}) => <div>{props.children}</div>
