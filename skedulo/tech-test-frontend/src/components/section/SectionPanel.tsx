import React from "react"

export const SectionPanel = (props: {children: React.ReactNode}) => <div>{props.children}</div>
