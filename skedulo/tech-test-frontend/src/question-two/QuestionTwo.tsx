import React from "react"
import { IAppTabContainer } from "../common/types"

import { SectionGroup } from "../components/section/SectionGroup"
import { SectionPanel } from "../components/section/SectionPanel"

import { DataService } from '../service/DataService'

import { createMachine, assign, DoneInvokeEvent, Activity } from "xstate"
import { useMachine } from "@xstate/react"

type Allocation = {
  allocType: 'job' | 'activity',
  name: string,
  start: string,
  end: string
}

interface ResourceSchedule {
  resourceName: string
  resourceId: number
  allocations: Allocation[]
}

const getJobAllocations = async () => await DataService.getJobAllocations()
const getActivityAllocations = async () => await DataService.getActivityAllocations()
const getActivities = async () => await DataService.getActivities()
const getResources = async () => await DataService.getResources()
const getJobs = async () => await DataService.getJobs()

type Context = {
  jobAllocations: Awaited<ReturnType<typeof getJobAllocations>>
  activityAllocations: Awaited<ReturnType<typeof getActivityAllocations>>
  activities: Awaited<ReturnType<typeof getActivities>>
  resources: Awaited<ReturnType<typeof getResources>>
  jobs: Awaited<ReturnType<typeof getJobs>>
  schedule: Array<ResourceSchedule>,
}

// Okie dokie theres a fair bit going on there.  I essentially had to mimic SQL inner joins, but I don't 
// have the beauty of a Prolog parser so here we go.  I've commented the logic as we go.
export const filterResourceSchedules = (c: Context): Array<ResourceSchedule> => {

  // Here I convert lists of jobs and activities into maps.  This means we incur 1 O(n) scan time for the 
  // map, but all further lookups are O(1), ie we've traded time for space.  Also I find maps infinitely 
  // easier to work with that lists, so hopefully this makes sense :) 
  const jobsMap: Record<number, Context['jobs'][0]> = c.jobs.reduce((acc, job) => ({ ...acc, [job.id]: job }), {});
  const activitiesMap: Record<number, Context['activities'][0]> = c.activities.reduce((acc, activity) => ({ ...acc, [activity.id]: activity }), {});

  // Now we map through each user (resource) to compute their schedule.
  return c.resources.map((resource) => {

    // We are going to create 2 internal psuedo-queue structures here.  Unfortunately this test requires 
    // jobs and activities to be in a special order, so we need to split it up individually and process 
    // each part as a chunk.
    //
    const filteredActivityAllocations: Context['activityAllocations'] =
      c.activityAllocations.filter(allocation => allocation.resourceId === resource.id)

    const filteredJobAllocations: Context['jobAllocations'] =
      c.jobAllocations.filter(allocation => allocation.resourceId === resource.id)


    // We need to process the "activities" first to keep the test gods happy
    const activities: Array<Allocation> = filteredActivityAllocations.map(allocation => {
      // This is our O(1) lookup mentioned above
      const activity = activitiesMap[allocation.activityId];
      return {
        allocType: 'activity',
        name: activity.name, 
        start: activity.start,
        end: activity.end,
      }
    })

    // We do jobs next.
    const jobs: Array<Allocation> = filteredJobAllocations.map(allocation => {
        const job = jobsMap[allocation.jobId];
        return {
          allocType: 'job',
          name: job.name,
          start: job.start,
          end: job.end,
        };
    })

    // Our resource schedule!
    return {
      resourceName: resource.name,
      resourceId: resource.id,
      // This is the magic here.  Since Arrays are Monoids, and Monoids are Semigroups that 
      // have been promoted, we can safely join them together.   We need to extract out a 
      // common union type to make this happen, otherwise typescipt will complain about 
      // each Array<A> !== Array<B>
      allocations: activities.concat(jobs),
    };
  });
}


const resourceMachine = createMachine<Context>({
  id: 'resource-machine',
  initial: 'loading',
  context: {
    jobAllocations: [],
    activityAllocations: [],
    activities: [],
    resources: [],
    jobs: [],
    schedule: [],
  },
  states: {
    loading: {
      invoke: {
        src: (_c, _e) => {
          return Promise.all([
            getJobAllocations(),
            getActivityAllocations(),
            getActivities(),
            getResources(),
            getJobs()
          ]);
        },
        onDone: {
          target: 'allDone',
          actions: assign((context, event: DoneInvokeEvent<any>): Context => {
            // Destructure the resolved values from the event
            const [jobAllocations, activityAllocations, activities, resources, jobs] = event.data;

            // Update the context with the resolved values
            return {
              ...context,
              jobAllocations,
              activityAllocations,
              activities,
              resources,
              jobs
            };
          })
        }
      }
    },
    allDone: {
      entry: assign((c, _) => ({...c, schedule: filterResourceSchedules(c)}))
    }
  },
});




export const QuestionTwo: React.FC<IAppTabContainer> = (props) => {

  const [current] = useMachine(resourceMachine)

  return (
    <SectionGroup>
      <SectionPanel>Please refer to INSTRUCTIONS.md</SectionPanel>
      <SectionPanel>
        <pre>{JSON.stringify(current.context.schedule)}</pre>
      </SectionPanel>
    </SectionGroup>
  )
}
