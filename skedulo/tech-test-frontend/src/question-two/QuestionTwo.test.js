import React from 'react'
import { render, screen } from '@testing-library/react'
import * as data from '../server/db.json'

import { QuestionTwo } from './QuestionTwo'
import { filterResourceSchedules } from './QuestionTwo';

const mockDataService = {
  getJobs: () => Promise.resolve(data.jobs),
  getJobAllocations: () => Promise.resolve(data.jobAllocations),
  getActivities: () => Promise.resolve(data.activities),
  getActivityAllocations: () => Promise.resolve(data.activityAllocations),
  getResources: () => Promise.resolve(data.resources)
}

test('fetched and merged data is in the expected shape', async () => {
  render(<QuestionTwo service={mockDataService} />)

  expect(await screen.findByText('[{"resourceName":"Sam Seaborn","resourceId":0,"allocations":[{"allocType":"job","name":"Shield some wiring","start":"2018-09-01T09:00:00Z","end":"2018-09-01T13:00:00Z"}]},{"resourceName":"Donna Moss","resourceId":1,"allocations":[{"allocType":"activity","name":"Meal Break","start":"2018-09-01T12:15:00Z","end":"2018-09-01T13:10:00Z"},{"allocType":"job","name":"Build a shed","start":"2018-09-01T10:15:00Z","end":"2018-09-01T11:00:00Z"}]},{"resourceName":"Toby Ziegler","resourceId":2,"allocations":[{"allocType":"activity","name":"Meal Break","start":"2018-09-01T12:15:00Z","end":"2018-09-01T13:10:00Z"}]}]'))
})



/**
 * Expected output:

[
   {
      "resourceName":"Sam Seaborn",
      "resourceId":0,
      "allocations":[
         {
            "allocType":"job",
            "name":"Shield some wiring",
            "start":"2018-09-01T09:00:00Z",
            "end":"2018-09-01T13:00:00Z"
         }
      ]
   },
   {
      "resourceName":"Donna Moss",
      "resourceId":1,
      "allocations":[
         {
            "allocType":"activity",
            "name":"Meal Break",
            "start":"2018-09-01T12:15:00Z",
            "end":"2018-09-01T13:10:00Z"
         },
         {
            "allocType":"job",
            "name":"Build a shed",
            "start":"2018-09-01T10:15:00Z",
            "end":"2018-09-01T11:00:00Z"
         }
      ]
   },
   {
      "resourceName":"Toby Ziegler",
      "resourceId":2,
      "allocations":[
         {
            "allocType":"activity",
            "name":"Meal Break",
            "start":"2018-09-01T12:15:00Z",
            "end":"2018-09-01T13:10:00Z"
         }
      ]
   }
]

 */



describe('filterResourceSchedules', () => {
  it('should return an array of resource schedules with correct mappings and combinations', () => {
    // Mock data
    const context = {
      jobAllocations: [
        { resourceId: 1, jobId: 1 },
        { resourceId: 1, jobId: 2 },
      ],
      activityAllocations: [
        { resourceId: 1, activityId: 1 },
        { resourceId: 2, activityId: 2 },
      ],
      activities: [
        { id: 1, name: 'Activity 1', start: '2024-01-01', end: '2024-01-02' },
        { id: 2, name: 'Activity 2', start: '2024-01-03', end: '2024-01-04' },
      ],
      resources: [
        { id: 1, name: 'Resource 1' },
        { id: 2, name: 'Resource 2' },
      ],
      jobs: [
        { id: 1, name: 'Job 1', start: '2024-01-05', end: '2024-01-06' },
        { id: 2, name: 'Job 2', start: '2024-01-07', end: '2024-01-08' },
      ],
    };

    const expectedSchedule = [
      {
        resourceName: 'Resource 1',
        resourceId: 1,
        allocations: [
          { allocType: 'activity', name: 'Activity 1', start: '2024-01-01', end: '2024-01-02' },
          { allocType: 'job', name: 'Job 1', start: '2024-01-05', end: '2024-01-06' },
          { allocType: 'job', name: 'Job 2', start: '2024-01-07', end: '2024-01-08' },
        ],
      },
      {
        resourceName: 'Resource 2',
        resourceId: 2,
        allocations: [
          { allocType: 'activity', name: 'Activity 2', start: '2024-01-03', end: '2024-01-04' },
        ],
      },
    ];

    // Call
    const result = filterResourceSchedules(context);

    // Check 
    expect(result).toEqual(expectedSchedule);
  });

});
