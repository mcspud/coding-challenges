import React from "react"

import { IAppTabContainer } from "../common/types"

import { SectionGroup } from "../components/section/SectionGroup"
import { SectionPanel } from "../components/section/SectionPanel"

import "./QuestionOne.css"

import { searchMachine } from './machine'
import { useMachine } from '@xstate/react';


const Loader = () => <div className="lds-dual-ring" />

export const QuestionOne: React.FC<IAppTabContainer> = () => {
  const [current, send] = useMachine(searchMachine);

  return (
    <SectionGroup>
      <SectionPanel>Please refer to INSTRUCTIONS.md</SectionPanel>
      <input 
        className="search-bar"
        type="text"
        value={current.context.searchData}
        onChange={(e) => send('SEARCH_CHANGED', { data: e.target.value })}>
      </input>

      {current.value === 'loading' && <Loader />}

      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Start</th>
            <th>End</th>
            <th>Contact</th>

          </tr>
        </thead>
        <tbody>
          {current.context.jobRecords.map(job =>
            <tr>
              <td>{job.name}</td>
              <td>{new Date(job.start).toLocaleString('en-au')}</td>
              <td>{new Date(job.end).toLocaleString('en-au')}</td>
              <td>{job.contact.name}</td>
            </tr>
          )}
        </tbody>
      </table>
    </SectionGroup>
  )
}
