import { interpret } from 'xstate';
import { DataService } from '../service/DataService';
import { clearRecords, search, SearchChanged, SearchContext, SearchEvents, searchGreatherThanMinimumLength, searchIsEmpty, searchMachine, updateSearch } from './machine';

describe('Search Machine state transitions', () => {
  it('should start with initial state', () => {
    const searchService = interpret(searchMachine);

    expect(searchService.initialState.value).toEqual('initial');
  });

  it('should transition to lessThan3 when search data is less than 3 characters', () => {
    const searchService = interpret(searchMachine);
    searchService.start()
    searchService.send({ type: 'SEARCH_CHANGED', data: 'ab' });
    expect(searchService.getSnapshot().value).toEqual('lessThan3');
  });

  it('should transition to moreThan3 when search data is more than 3 characters', async () => {
    const searchService = interpret(searchMachine);
    searchService.start()
    searchService.send({ type: 'SEARCH_CHANGED', data: 'ab' });
    searchService.send({ type: 'SEARCH_CHANGED', data: 'abcdef' });
    searchService.send({ type: 'SEARCH_CHANGED', data: 'abcdefhijk' });

    // test
    expect(searchService.getSnapshot().value).toEqual('moreThan3');
  });


  it('should transition to loading when search data is more than 3 characters and no typing for half a second', () => {
    jest.useFakeTimers();

    const searchService = interpret(searchMachine);
    searchService.start()
    // More async faking.   Need to trick the event loop for timing here. 
    searchService.send({ type: 'SEARCH_CHANGED', data: 'abcd' });
    searchService.send({ type: 'SEARCH_CHANGED', data: 'abcd' });
    searchService.send({ type: 'SEARCH_CHANGED', data: 'abcd' });
    searchService.send({ type: 'SEARCH_CHANGED', data: 'abcd' });

    jest.advanceTimersByTime(500);

    expect(searchService.getSnapshot().value).toEqual('loading');

    jest.useRealTimers();
  });

  it('should transition to initial when search data is empty', () => {
    const searchService = interpret(searchMachine);
    searchService.start()

    // Even more async faking.  I think this is an issue with the age of the testing library, will need 
    // to investigate another time
    searchService.send({ type: 'SEARCH_CHANGED', data: 'abcd' });
    searchService.send({ type: 'SEARCH_CHANGED', data: 'abc' });
    searchService.send({ type: 'SEARCH_CHANGED', data: 'ab' });
    searchService.send({ type: 'SEARCH_CHANGED', data: 'a' });
    searchService.send({ type: 'SEARCH_CHANGED', data: '' });

    expect(searchService.state.value).toEqual('initial');
  });
});


jest.mock('../service/DataService');

describe('updateSearch', () => {
  it('should update search data in context', () => {
    const context: SearchContext = { searchData: '', jobRecords: [] };
    const event: SearchChanged = { type: 'SEARCH_CHANGED', data: 'test' };

    const updatedContext = updateSearch(context, event);

    expect(updatedContext.searchData).toBe('test');
    expect(updatedContext.jobRecords).toEqual([]);
  });
});


describe('clearRecords', () => {
  it('should clear job records in context', () => {
    const context: SearchContext = {
      searchData: 'test',
      jobRecords: [
        {
          name: 'Job 1',
          start: new Date().toISOString(),
          end: new Date().toISOString(),
          contact: {
            name: 'Jamie'
          }
        }]
    };
    const event: SearchChanged = { type: 'SEARCH_CHANGED', data: '' };

    const updatedContext = clearRecords(context, event);

    expect(updatedContext.searchData).toBe('test');
    expect(updatedContext.jobRecords).toEqual([]);
  });
});

describe('searchGreatherThanMinimumLength', () => {
  it('should return true when search data length is greater than or equal to 3', () => {
    const context: SearchContext = { searchData: 'test', jobRecords: [] };
    const event: SearchChanged = { type: 'SEARCH_CHANGED', data: 'definitely longer than 3' };
    const result = searchGreatherThanMinimumLength(context, event);

    expect(result).toBe(true);
  });

  it('should return false when search data length is less than 3', () => {
    const context: SearchContext = { searchData: 'te', jobRecords: [] };
    const event: SearchChanged = { type: 'SEARCH_CHANGED', data: '' };
    const result = searchGreatherThanMinimumLength(context, event);

    expect(result).toBe(false);
  });
});


describe('searchIsEmpty', () => {
  it('should return true when search data is empty', () => {
    const context: SearchContext = { searchData: '', jobRecords: [] };
    const event: SearchChanged = { type: 'SEARCH_CHANGED', data: '' };
    const result = searchIsEmpty(context, event);

    expect(result).toBe(true);
  });

  it('should return false when search data is not empty', () => {
    const context: SearchContext = { searchData: 'test', jobRecords: [] };
    const event: SearchChanged = { type: 'SEARCH_CHANGED', data: 'not empty' };
    const result = searchIsEmpty(context, event);

    expect(result).toBe(false);
  });
});

