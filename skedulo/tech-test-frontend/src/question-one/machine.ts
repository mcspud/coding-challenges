import { assign, createMachine, DoneInvokeEvent } from 'xstate'
import { DataService } from '../service/DataService'
import { z } from 'zod'


// Validate our incoming GraphQL data.
export const Job = z.object({
  name: z.string(),
  start: z.string().datetime(),
  end: z.string().datetime(),
  contact: z.object({
    name: z.string()
  })
})


export const Jobs = z.array(Job)

export type Job = z.infer<typeof Job>
export type Jobs = z.infer<typeof Jobs>


// State machine shape
export type SearchContext = {
  // strings are a Monoid so no need to deal with null
  searchData: string
  // Arrays are Monoids so no need to deal with null.
  jobRecords: Jobs
}

// This very simple system can derive all computable states from a single event :) 
export type SearchChanged = { type: 'SEARCH_CHANGED', data: string }
export type SearchEvents =
  | SearchChanged


// State machine actions
export const updateSearch = (context: SearchContext, event: SearchChanged): SearchContext => {
  return ({ ...context, searchData: event.data })
}

export const clearRecords = (context: SearchContext, _: SearchEvents): SearchContext => {
  return ({ ...context, jobRecords: [] })
}

export const searchGreatherThanMinimumLength = (context: SearchContext, _: SearchEvents): boolean => {
  return context.searchData.length >= 3
};

export const searchIsEmpty = (context: SearchContext, _: SearchEvents): boolean => {
  return context.searchData.length === 0
};

export const search = async (context: SearchContext, _: SearchEvents): Promise<Jobs> => {
  const data = await DataService.getJobsWithSearchTerm(context.searchData)
  return Jobs.parse(data)
}

export const assignJobs = assign((context: SearchContext, event: DoneInvokeEvent<Jobs>): SearchContext =>
  ({ ...context, jobRecords: event.data })
)

// State machine itself
export const searchMachine = createMachine<SearchContext, SearchEvents>({
  id: 'search',
  predictableActionArguments: true,
  preserveActionOrder: true,
  initial: 'initial',
  context: {
    searchData: '',
    jobRecords: [],
  },
  states: {
    initial: {
      // Whenever we enter the state we clear any job records
      entry: [assign(clearRecords)],
      on: {
        // Immediately map any change to LessThan3
        SEARCH_CHANGED: {
          actions: [assign(updateSearch)],
          target: 'lessThan3',
        }
      }
    },
    lessThan3: {
      on: {
        // Dispatch to the transition handler.  The reason is documented above that state node
        SEARCH_CHANGED: {
          actions: [assign(updateSearch)],
          target: 'transitionHandler',
        },
      }
    },
    moreThan3: {
      on: {
        // Dispatch to the transition handler.  The reason is documented above that state node
        SEARCH_CHANGED: {
          actions: [assign(updateSearch)],
          target: 'transitionHandler',
        },
      },
      // If no one types for half a second the debouncer ends and a transition to the search 
      // service is called.
      after: {
        500: {
          target: 'loading'
        }
      }
    },
    // unfortunately we need to map to a handler here given how old this test is.  In more 
    // modern versions of this statechart library we can update the context before we fire 
    // any guard clauses, but here we are forced to transition to a new state so we can 
    // update the context first. 
    transitionHandler: {
      always: [
        { target: 'initial', cond: searchIsEmpty },
        { target: 'moreThan3', cond: searchGreatherThanMinimumLength },
        { target: 'lessThan3' },
      ]
    },
    loading: {
      invoke: {
        src: search,
        onDone: {
          target: 'transitionHandler',
          actions: assignJobs,
        },
        onError: {
          target: 'transitionHandler'
        }
      }
    },
  }
})
