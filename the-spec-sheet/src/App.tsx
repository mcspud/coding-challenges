import * as React from 'react'
import * as F from 'fp-ts/lib/function'
import * as O from 'fp-ts/lib/Option'
import * as A from 'fp-ts/lib/Array'
import * as E from 'fp-ts/lib/Either'
import * as N from 'fp-ts/lib/number'

import { Popover } from '@headlessui/react'
import { useInterpret, useActor } from '@xstate/react';
import { ActorRefFrom } from 'xstate'
import { MagnifyingGlassIcon } from '@heroicons/react/20/solid'


import { lazyFileBrowserMachine, fileBrowserMachine } from './machine'

import * as constants from './constants'
import * as codecs from './codecs'
import AddModal from './AddModal'
import Resource from './Resource'
import Footer from './Footer'
import Breadcrumbs from './Breadcrumbs'
import reporter from 'io-ts-reporters'


type GlobalStateContext = {
    fileBrowserService: ActorRefFrom<typeof fileBrowserMachine>
}
export const GlobalStateContext = React.createContext({} as GlobalStateContext);

export const react_noop = F.constant(<></>)
export const react_lift = (c: React.ReactNode) => F.constant(<>{c}</>)

export const optEq = O.getEq(N.Eq)

export default function App() {
    const fileBrowserService = useInterpret(lazyFileBrowserMachine());

    // Theoretically this should encode context to localStorage but I'm not
    // having any luck when I'm getting it back out.  
    // I'm guessing that the custom/non-default option encoder isn't working, 
    // but looking at the data structure it will require a fully fledged 
    // parser and  I'm not going to patch that in for a tech test.
    fileBrowserService.subscribe(state =>
        F.pipe(
            state.context,
            codecs.contextC.encode,
            JSON.stringify,
            data => window.localStorage.setItem(constants.STORAGE_KEY, data)))

    const [current, send] = useActor(fileBrowserService)

    // Search value used in the actual text input.  Since we are using {None}
    // for a sentinel we need convert it to a value that both React and
    // the user understands, ie the magical empty string. 
    // In a more meaty frontend you'd probably externalise the Search 
    // bar into a <SeachComponent />, allowing for enhancements like
    // autocomplete etc
    const _search = F.pipe(
        current.context.search,
        O.fold(F.constant(''), F.identity))

    // Eithers are typically overloaded to deal with errors, but it
    // represents a functional branch here, ie we fold out a default
    // value (the complete list) if there is no search value, 
    // otherwise we filter the list
    const _searchResources = F.pipe(
        current.context.search,
        E.fromOption(F.constant(current.context.resources)),
        E.map(search => F.pipe(
            current.context.resources,
            A.filter(res => {
                const _search = search.toLowerCase()
                const _title = res.title.toLowerCase()
                return _title.includes(_search)
            }),
        )),
        E.fold(F.identity, F.identity)
    )

    // Recieves pre-filtered search responses and further narrows
    // the set, returning a set of resources at the same "level"
    const _heirarchyResources = F.pipe(
        _searchResources,
        A.filter(res => optEq.equals(res.parent, current.context.current)))

    // Functionally lifts a List<JSX.Components> into a React.Node
    const Resources = F.pipe(
        _heirarchyResources,
        A.map(r => <div className='m-4'><Resource {...r} /></div>),
        react_lift)

    return (
        <GlobalStateContext.Provider value={{ fileBrowserService }}>
            <AddModal />
            <div className="min-h-full">
                <header className="bg-white shadow">
                    <div className="mx-auto max-w-7xl px-2 sm:px-4 lg:px-8">
                        <Popover className="flex h-16 justify-between">
                            <div className="flex px-2 lg:px-0">
                                <div className="flex flex-shrink-0 items-center">
                                </div>
                                <nav aria-label="Global" className="hidden lg:ml-6 lg:flex lg:items-center lg:space-x-4">

                                </nav>
                            </div>
                            <div className="flex flex-1 items-center justify-center px-2 lg:ml-6 lg:justify-end">
                                <div className="w-full max-w-lg lg:max-w-xs">

                                    <div className="relative">
                                        <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                                            <MagnifyingGlassIcon className="h-5 w-5 text-gray-400" aria-hidden="true" />
                                        </div>
                                        <input
                                            name="search"
                                            className="block w-full rounded-md border border-gray-300 bg-white py-2 pl-10 pr-3 leading-5 placeholder-gray-500 shadow-sm focus:border-blue-600 focus:placeholder-gray-400 focus:outline-none focus:ring-1 focus:ring-blue-600 sm:text-sm"
                                            placeholder="Search"
                                            type="search"
                                            onChange={e => send({ type: 'ENTER_SEARCH', data: e.currentTarget.value })}
                                            value={_search}
                                        />
                                    </div>
                                </div>
                            </div>
                        </Popover>
                    </div>

                    <div className="mx-auto max-w-7xl px-4 sm:px-6">
                        <div className="border-t border-gray-200 py-3">
                            <nav className="flex" aria-label="Breadcrumb">
                                <div className="hidden sm:block">
                                    <Breadcrumbs />
                                </div>
                            </nav>
                        </div>
                    </div>
                </header>

                <main className="py-5 flex">
                    <section className='mx-5 w-2/3 grid grid-cols-2'>
                        <Resources />
                    </section>
                    <section className='mx-5 w-1/3'>
                        <div className="bg-white px-4 py-5 shadow sm:rounded-lg sm:px-6">
                            <div className="justify-stretch flex flex-col">
                                <button
                                    onClick={() => send('TOGGLE_MODAL')}
                                    type="button"
                                    className="inline-flex items-center justify-center rounded-md border border-transparent bg-blue-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2">
                                    Add Resource
                                </button>
                            </div>
                        </div>
                    </section>

                </main>
            </div>
            <Footer />
        </GlobalStateContext.Provider>
    )
}
