import { assign, createMachine } from 'xstate';
import { Lens } from 'monocle-ts'
import * as O from 'fp-ts/lib/Option'
import * as F from 'fp-ts/lib/function'
import * as A from 'fp-ts/lib/Array'
import * as E from 'fp-ts/lib/Either'

import * as constants from './constants'
import * as codecs from './codecs'

export type TFile = {
    type: 'file'
    title: string
    description: string
    parent: O.Option<number>
    id: number,
}
export type TFolder = {
    type: 'folder'
    title: string
    parent: O.Option<number>
    id: number,
}
export type TResource =
    | TFile
    | TFolder

export type TContext = {
    search: O.Option<string>
    current: O.Option<number>
    add: {
        title: O.Option<string>
        description: O.Option<string>
    }
    resources: Array<TResource>
}

export type TEnterSearch = { type: 'ENTER_SEARCH', data: string }
export type TSubmitSearch = { type: 'SUBMIT_SEARCH' }
export type TEnterTitle = { type: 'ENTER_TITLE', data: string }
export type TEnterDescription = { type: 'ENTER_DESCRIPTION', data: string }
export type TAddResource = { type: 'ADD_FOLDER' }
export type TToggleModal = { type: 'TOGGLE_MODAL' }
export type TNavigate = { type: 'NAVIGATE', data: O.Option<number> }
export type TEvent =
    | TEnterSearch
    | TSubmitSearch
    | TEnterTitle
    | TEnterDescription
    | TNavigate
    | TToggleModal
    | TAddResource


const contextLens = Lens.fromPath<TContext>()

const emptyString = (value: string): O.Option<string> => F.pipe(
    value, O.fromNullable, O.chain(O.fromPredicate(v => v.length > 0))
)
const update_search = assign((context: TContext, event: TEnterSearch) => {
    return F.pipe(event.data, emptyString, v => contextLens(['search']).set(v)(context))
})
const update_title = assign((context: TContext, event: TEnterTitle) => {
    return F.pipe(event.data, emptyString, v => contextLens(['add', 'title']).set(v)(context))
})
const update_description = assign((context: TContext, event: TEnterDescription) => {
    return F.pipe(event.data, emptyString, v => contextLens(['add', 'description']).set(v)(context))
})
const add_folder = assign((context: TContext, event: TAddResource) => {
    const resource: TResource = F.pipe(
        context.add.description,
        O.fold<string, TResource>(
            (): TFolder => ({
                type: 'folder',
                title: F.pipe(context.add.title, O.fold(F.constant(''), F.identity)),
                parent: F.pipe(context.current),
                id: context.resources.length
            }),
            (): TFile => ({
                type: 'file',
                title: F.pipe(context.add.title, O.fold(F.constant(''), F.identity)),
                description: F.pipe(context.add.description, O.fold(F.constant(''), F.identity)),
                parent: F.pipe(context.current),
                id: context.resources.length
            }),
        )
    )
    return F.pipe(
        context,
        contextLens(['resources']).modify(A.append(resource)),
        contextLens(['add', 'title']).set(O.none),
        contextLens(['add', 'description']).set(O.none),
    )
})
const navigate = assign((context: TContext, event: TNavigate) => {
    return F.pipe(event.data, contextLens(['current']).set)(context)
})

export const folder = (id: number, title: string, parent: O.Option<number>): TFolder => ({
    type: 'folder',
    title,
    parent,
    id
})
export const file = (id: number, title: string, description: string, parent: O.Option<number>): TFile => ({
    type: 'file',
    title,
    description,
    parent,
    id,
})

export const fileBrowserMachine = createMachine<TContext, TEvent>({
    id: 'fileBrowserMachine',
    context: {
        search: O.none,
        add: {
            title: O.none,
            description: O.none,
        },
        current: O.none,
        resources: [
            folder(0, 'Folder 1', O.none),
            file(1, 'File 1', 'File 1 for Folder 1', O.some(0)),
            file(2, 'File 2', 'File 2 for Folder 1', O.some(0)),
            file(3, 'File 3', 'File 3 for Folder 1', O.some(0)),
            file(4, 'File 4', 'File 4 for Folder 1', O.some(0)),
            folder(5, 'Folder 2', O.none),
            folder(9, 'Folder 9', O.some(5)),
            file(6, 'File', 'File for folder 2', O.some(0)),
            folder(7, 'Folder 3', O.none),
            file(8, 'File', 'Top Level', O.none)
        ],
    },
    initial: 'main',
    states: {
        main: {
            on: {
                TOGGLE_MODAL: {
                    target: 'add',
                },
                NAVIGATE: {
                    actions: [navigate]
                },
                ENTER_SEARCH: {
                    actions: [update_search],
                }
            }
        },
        add: {
            on: {
                TOGGLE_MODAL: {
                    target: 'main',
                },
                ENTER_TITLE: {
                    actions: [update_title],
                },
                ENTER_DESCRIPTION: {
                    actions: [update_description],
                },
                ADD_FOLDER: {
                    actions: [add_folder],
                    target: 'main',
                }
            }
        }
    }
});


export const lazyFileBrowserMachine = () => F.pipe(
    E.tryCatch(
        () => F.pipe(
            window.localStorage.getItem(constants.STORAGE_KEY),
            codecs.contextC.decode,
            E.map((context: TContext) => fileBrowserMachine.withContext(context)),
            E.fold(() => fileBrowserMachine, F.identity),
        ),
        () => fileBrowserMachine,
    ),
    E.fold(F.identity, F.identity)
)

