import * as t from 'io-ts'
import { option } from 'io-ts-types/lib/option'
import type { TContext } from './machine'

const fileC = t.type({
    type: t.literal('file'),
    title: t.string,
    description: t.string,
    parent: option(t.number),
    id: t.number,
})

const folderC = t.type({
    type: t.literal('folder'),
    title: t.string,
    parent: option(t.number),
    id: t.number,
})

const resourceC = t.union([
    fileC, 
    folderC,
])

export const contextC = t.type({
    search: option(t.string),
    current: option(t.number),
    add: t.type({
        title: option(t.string),
        description: option(t.string),
    }),
    resources: t.array(resourceC),
})
export type contextC = t.TypeOf<typeof contextC>


// The below code ensures our runtime encders and 
// compiletime types are compatible.

// This is a gnarly hack I use to establish type equality,
// `declare` is a compile time only declaration so its =
// values get stripped.
// Ive never bothered to look to see if the `if (false)` 
// stripped out as unreachable.  Leaving that as an exercise
// to the reader
// NOTE: If you know a better way to do this purely in the
// type system with inbuilts please let me know 
declare const dummy1: contextC;
declare const dummy2: TContext;
declare function sameType<T>(v1: T, v2: T): void;
if (false) {
    sameType(dummy1, dummy2);
}