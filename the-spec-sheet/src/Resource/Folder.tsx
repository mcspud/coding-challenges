import * as React from 'react'
import * as F from 'fp-ts/lib/function'
import * as O from 'fp-ts/lib/Option'
import { FolderIcon } from '@heroicons/react/24/outline'
import { TFolder } from '../machine'
import { GlobalStateContext } from '../App'
import { useActor } from '@xstate/react'

type Props = Pick<TFolder, 'title' | 'parent' | 'id'> 


export const Folder = (props: Props) => {
    const services = React.useContext(GlobalStateContext)
    const [current, send] = useActor(services.fileBrowserService)
  
    const parent = F.pipe(props.parent, O.fold(F.constant(O.none._tag), v => Number(v).toString()))
    const onClick = () => send({type: 'NAVIGATE', data: O.some(props.id)})

    return (
        <section 
            onDoubleClick={onClick}
            className="col-span-1 col-start-1 cursor-pointer bg-white hover:bg-gray-50 transition duration-150">
            <div className=" px-4 py-5 shadow sm:rounded-lg sm:px-6">
                <div className="justify-stretch mt-6 flex justify-center">
                    <FolderIcon className='w-12 h-12' />
                </div>
                <div className="justify-stretch mt-6 justify-center">
                    <div className='m-2 text-center'>{props.title}</div>
                </div>
                <div className='text-xs m-2'>
                    <div>ID: {props.id}</div>
                    <div>Parent: {parent}</div>
                </div>
            </div>
        </section>
    )
}

export default Folder