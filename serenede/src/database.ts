
// This id was prone to collisions, heres a far more useful identifier
const taskId = () => `task_${Math.random()}`
const tasks: Task[] = [
    {title: 'Fix typo', done: false, id: taskId()},
    {title: 'Upload new logo', done: true, id:  taskId()},
    {title: 'Add login button ', done: false, id:  taskId()},
    {title: 'Write tests', done: false, id:  taskId()},
    {title: 'Deploy to production', done: false, id:  taskId()},
    {title: 'Deadfasdfloy to asdf', done: false, id:  taskId()},
    {title: 'asdfasdf asdfasfd', done: false, id:  taskId()},
    {title: 'asdf asdfasdfas', done: false, id:  taskId()},
];

/*
 * This file mocks a database by storing some data in memory. 
 * Just pretend the data comes from Postgres if you wish :)
 */

export async function fetchTasks(from: number, size: number) {
    // Problem 3: Pagination
    // Lets return back a resultset showing:
    // - total number of results
    // - current offset in the list
    // - current page size 
    
    return {
      size: tasks.length,
      from: from,
      page: size,
      results: [...tasks].splice(from, size),
    }
}

export async function setTaskDone(taskId: string, done: boolean) {
    const task = tasks.find(t => t.id === taskId);

    if (!task)
        throw Error(`No task with ID ${taskId}`);

    task.done = done;
    return task;
}

export async function addTask(title: string) {
    const task = {
        title,
        done: false,
        id:  taskId(),
    };
    tasks.push(task);
    return task;
}

export async function deleteTask(taskId: string) {
    const index = tasks.findIndex(task => task.id === taskId)
    tasks.splice(index, 1)
}