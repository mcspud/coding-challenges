import styles from './TaskCard.module.css';
import {FormEventHandler, useCallback, useState} from "react";

const TaskCard = (props: { task: Task } & { deleteTask: (deletedTask: Task) => void }) => {
    const [task, setTask] = useState<Task>(props.task);
    const [asyncToggleActive, setAsyncToggleActive] = useState<boolean>(false)

    const deleteTask = () => {
      // all changes should be disabled while an effect is running
      setAsyncToggleActive(true)
      fetch('/api/done', {
            method: 'DELETE',
            body: JSON.stringify({
                taskId: props.task.id,
            }),
        })
        .then(() => props.deleteTask(props.task))
        .then(() => setAsyncToggleActive(false))
        .catch(() => setAsyncToggleActive(false))
    }
    
    const onDoneToggled = useCallback(() => {
      // all changes should be disabled while an effect is running
        setAsyncToggleActive(true)
        fetch('/api/done', {
            method: 'PUT',
            body: JSON.stringify({
                taskId: task.id,
                done: !task.done,
            }),
        })
            .then(r => r.json())
            .then(setTask)
            .then(() => setAsyncToggleActive(false))
            .catch(() => setAsyncToggleActive(false));
    }, [task.done, task.id]);
    
    return (
        <div className={styles.taskCard}>
            <div className={styles.title}>{task.title}</div>
            <div className={styles.done}>
                <input
                    type={"checkbox"}
                    checked={task.done}
                    onChange={onDoneToggled}
                    disabled={asyncToggleActive === true} 
                />
            </div>
            <div>
              <button 
                onClick={deleteTask}
                disabled={asyncToggleActive === true}>Delete</button>
            </div>
        </div>
    );
};


export default TaskCard;