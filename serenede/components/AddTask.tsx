import {FormEventHandler, useCallback, useState} from "react";
import styles from "./TaskCard.module.css";

type Props = {
    onTaskAdded: (task: Task) => void
};

export const AddTask = (props: Props) => {
    const [title, setTitle] = useState("");
    const [taskActive, setTaskActive] = useState<boolean>(false);
    
    const onSubmit: FormEventHandler = useCallback(e => {
        e.preventDefault();
        setTaskActive(true);
        fetch('/api/add', {
            method: 'POST',
            body: JSON.stringify({title}),
        })
            .then(r => r.json())
            .then(props.onTaskAdded)
            .then(() => setTitle(""))
            .then(() => setTaskActive(false))
            .catch(() => setTaskActive(false))

    }, [props.onTaskAdded, title]);
    return (
        <form onSubmit={onSubmit}>
            <div className={styles.taskCard}>
                <div className={styles.title}>
                    <input
                        type="text"
                        placeholder="Add new task"
                        value={title}
                        onChange={e => setTitle(e.target.value)}
                    />
                </div>
                <div className={styles.add}>
                    <button 
                      type="submit"
                      disabled={taskActive}>Add</button>
                </div>
            </div>
        </form>
    );
}

export default AddTask;