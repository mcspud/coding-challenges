This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Workflow:

Each state and transition can be seen here:
https://xstate.js.org/viz/?gist=ccb2009eceed0333ac722559febec356

Setup:

- Clone the repo
- At the project root, assuming `yarn` as the package manager:

```
yarn install
yarn start
```

- Create a user using the registration
function in the app
- Login using the login page
- Click around and update your profile data
