import React from 'react'

const ProfileLoading: React.FC = () =>
    <div>
        Loading profile data... :D
    </div>

export default ProfileLoading
