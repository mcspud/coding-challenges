import React from 'react'
import { AutomataService } from '../automata'

type RegisterSuccess = {service: AutomataService}
const RegisterSuccess = ({service}: RegisterSuccess) =>
    <div>
        New user successfully registered!
        <div>
            <a href="#" onClick={() => service.send('TO_LOGIN')}>Login with new user</a>
        </div>
    </div>

export default RegisterSuccess
