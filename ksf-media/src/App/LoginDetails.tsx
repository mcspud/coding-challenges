import React from 'react'
import { AutomataService } from '../automata'

const changeHandler = (service: AutomataService, field: string) => (e: React.FormEvent<HTMLInputElement>) =>
    service.send('ENTER_LOGIN_DETAILS', {field: field, value: e.currentTarget.value})

type Login = {service: AutomataService}
const Login = ({service}: Login) =>
    <div>
        <h2>Login</h2>
        <div>
            <div>Username: <input type="text" onChange={changeHandler(service, 'username')}/></div>
            <div>Password: <input type="text" onChange={changeHandler(service, 'password')}/></div>
        </div>
        <div>
            <button onClick={() => service.send('LOGIN_START')}>Submit</button>
        </div>
        <a href="#" onClick={() => service.send('TO_REGISTER_NEW_USER')}>
            Register new user
        </a>
    </div>

export default Login
