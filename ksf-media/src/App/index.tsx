import React from 'react';
import { interpret } from 'xstate'
import { useService } from '@xstate/react';

import machine from '../automata'
import Header from './Header'

import LoginDetails from './LoginDetails'
import LoginSubmitting from './LoginSubmitting'
import LoginError from './LoginError'

import RegisterDetails from './RegisterDetails'
import RegisterError from './RegisterError'
import RegisterSuccess from './RegisterSuccess'
import RegisterSubmitting from './RegisterSubmitting'

import ProfileLoading from './ProfileLoading'
import ProfileError from './ProfileError'
import ProfileDetails from './ProfileDetails'
import ProfileUpdate from './ProfileUpdate'
import ProfileUpdating from './ProfileUpdating'
import ProfileUpdateError from './ProfileUpdateError'

const service = interpret(machine, {devTools: true}).start()


const App: React.FC = () => {
    const [current, send] = useService(service);

    return (
    <div className="App">
        <Header />
        {console.log(JSON.stringify(service.machine.config))}
        <div>
            <p>{current.value}</p>
        </div>
        {current.matches('login') &&
            <LoginDetails service={service}/>}

        {current.matches('submit_login') &&
            <LoginSubmitting service={service}/>}

        {current.matches('login_error') &&
            <LoginError service={service}/>}

        {current.matches('login_success') &&
            <p>Successfully logged you in!</p>}

        {current.matches('load_profile_data') &&
            <ProfileLoading />  }

        {current.matches('load_profile_data_error') &&
            <ProfileError service={service} />  }

        {current.matches('profile_page') &&
            <ProfileDetails service={service} />}

        {current.matches('update_address') &&
            <ProfileUpdate service={service} />}

        {current.matches('submit_address_error') &&
            <ProfileUpdateError service={service} />}

        {current.matches('submit_address') &&
            <ProfileUpdating />}

        {current.matches('register_new_user') &&
            <RegisterDetails service={service} />}

        {current.matches('submit_new_user') &&
            <RegisterSubmitting service={service} />}

        {current.matches('submit_new_user_success') &&
            <RegisterSuccess service={service} />}

        {current.matches('submit_new_user_error') &&
            <RegisterError service={service} />}
    </div>
    )
}

export default App;
