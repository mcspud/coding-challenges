import React from 'react'
import { AutomataService } from '../automata'

type LoginError = {service: AutomataService}
const LoginError = ({service}: LoginError) =>
    <div>
        There was an error logging you in.
        <div>
            <button onClick={() => service.send('LOGIN_START')}>Try again</button>
        </div>
        <div>
            <a href="#" onClick={() => service.send('TO_LOGIN')}>Back to Login</a>
        </div>
    </div>

export default LoginError
