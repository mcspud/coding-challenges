import { assign, createMachine, DoneInvokeEvent } from 'xstate';
import { Lens } from 'monocle-ts'
import * as O from 'fp-ts/lib/Option'
import * as F from 'fp-ts/lib/function'
import * as E from 'fp-ts/lib/Either'
import * as T from 'fp-ts/lib/Task'
import * as TE from 'fp-ts/lib/TaskEither'

import * as constants from './constants'
import * as c from './codecs'
import * as t from "io-ts";


export type TContext = {
    search: O.Option<string>
    results: O.Option<c.ResultListC>
    lastResult: O.Option<c.Result>
}

type TEnterSearch = { type: 'ENTER_SEARCH', data: string }
type TSubmitSearch = { type: 'SUBMIT_SEARCH' }
type TSearchResults = { type: 'SEARCH_RESULTS', data: O.Option<c.ResultListC> }
type TSearchSelect = { type: 'SEARCH_SELECT', data: c.Result }
type TEvent =
    | TEnterSearch
    | TSubmitSearch
    | TSearchResults
    | TSearchSelect


// applicative optics for the machine context
const contextLens = Lens.fromPath<TContext>()

// takes a string and if empty folds it to {None}, otherwise folds it to {Some <value>}
const emptyStringToNone = (value: string): O.Option<string> => F.pipe(
    value, O.fromNullable, O.chain(O.fromPredicate(v => v.length > 0))
)

const update_search = assign((context: TContext, event: TEnterSearch) => {
    return F.pipe(event.data, emptyStringToNone, contextLens(['search']).set, f => f(context))
})
const clear_search = assign((context: TContext, event: TEnterSearch) => {
    return F.pipe(O.none, contextLens(['search']).set, f => f(context))
})
const select_search = assign((context: TContext, event: TSearchSelect) => F.pipe(
    context,
    F.pipe(event.data.name, O.some, contextLens(['search']).set),
    F.pipe(event.data, O.some, contextLens(['lastResult']).set),
))


// Search 
// ------------------------------
// The actual search function.  Any errors are safely wrapped in a TaskEither for later
// processing.  This uses the convention/standard of right-bias.
const async_search = (url: string): TE.TaskEither<Error, unknown> => TE.tryCatch(
    () => fetch(url).then(res => {
        if (!res.ok) {
            throw new Error(`fetch failed with status: ${res.status}`); // totally irrelevant error message.
        }
        return res.json();
    }),
    E.toError
);

// The result of this function doesnt matter, its just used as type conversion tool
// between TaskEither<v> - Task<Option<v>>
const decodeError = (e: t.Errors): Error => {
    const missingKeys = e.map(e => e.context.map(({ key }) => key).join("."));
    return new Error(`${missingKeys}`);
}
// The result of this function doesnt matter, its just used as type conversion tool
// between TaskEither<v> - Task<Option<v>>
const decode = (res: unknown): TE.TaskEither<Error, c.ResultListC> =>
    F.pipe(TE.fromEither(c.ResultListC.decode(res)), TE.mapLeft(decodeError));

// The actual, fully type safe search handler.
// It:
//      - checks if there is actually a value in the search bar
//          - if not, it skips all subsequent steps by lifting to a TaskEither.left
//          - if so, it lifts the value to a TaskEiter.right
//      - appends the query parameter to a search string to be proxied
//      - runs the actual async search code and flattens the nested TaskEithers.  
//          - if there are any errors bypass the rest of the computation and set results to a {None}
//      - validates the incoming data to make sure that it passes the codec schema defined in `codecs.ts`
//          - if it fails it folds out to a Task<None> and assigns that to results
//      - assigns the typechecked, validated data to Context and updates the state machine.
// 
// It is important to note that every step here is fully type safe.  The *ML combinators allow for very terse,
// very compact, compile-time guaranteed code, but I acknowledge this makes a lot of people weep and cry
// the first time they see this :)


const invoke_search = (context: TContext, event: TEvent): T.Task<O.Option<c.ResultListC>> => F.pipe(
    context.search,
    TE.fromOption(() => F.pipe(O.none, E.toError)),
    TE.map(constants.API_URL),
    TE.chain(async_search),
    TE.chain(decode),
    TE.fold(e => F.pipe(O.none, T.of), v => F.pipe(v, O.some, T.of))
);

const apply_results_to_context = assign((context: TContext, event: DoneInvokeEvent<O.Option<c.ResultListC>>) => F.pipe(
    context,
    contextLens(['results']).set(event.data),
))

export const searchMachine = createMachine<TContext, TEvent>({
    id: 'searchMachine',
    context: {
        search: O.none,
        results: O.none,
        lastResult: O.none,
    },
    initial: 'main',
    states: {
        main: {
            on: {
                ENTER_SEARCH: {
                    target: 'enter_search',
                    actions: [update_search],
                },
                SUBMIT_SEARCH: {
                    target: 'invoke_search',
                },
                SEARCH_SELECT: {
                    target: 'search_select',
                    actions: [select_search],
                }
            }
        },
        enter_search: {
            on: {
                ENTER_SEARCH: {
                    target: 'enter_search',
                    actions: [update_search],
                },
                SUBMIT_SEARCH: {
                    target: 'invoke_search',
                }
            },
            after: {
                1000: {
                    target: 'invoke_search',
                }
            }
        },
        invoke_search: {
            invoke: {
                id: 'search',
                src: invoke_search,
                onDone: {
                    target: 'main',
                    actions: [
                        apply_results_to_context,
                    ],
                },
                onError: {
                    target: 'enter_search',

                }
            }
        },
        search_select: {
            on: {
                ENTER_SEARCH: {
                    target: 'enter_search',
                    actions: [
                        clear_search,
                    ],
                },
            }
        }
    }
});

export const lazySearchMachine = () => searchMachine;
