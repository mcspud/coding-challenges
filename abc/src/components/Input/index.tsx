import * as React from "react";
import * as F from 'fp-ts/lib/function'
import * as O from 'fp-ts/lib/Option'

import { useActor } from "@xstate/react";
import { StateContext } from "../../App";
import "./Input.css";

type Props = {
  className?: string
}
export const Input = (props: Props) => {
  const services = React.useContext(StateContext)
  const [current, send] = useActor(services.searchService)

  // we need to convert a {None | Some v} to a value that react
  // and the end user understands.  ENTER THE ALMIGHT EMPTY STRING
  const value = F.pipe(current.context.search, O.fold(F.constant(''), F.identity))

  return (
    <input
      className={"Input " + (props.className || "")}
      type="text"
      value={value}
      onChange={e => send({ type: 'ENTER_SEARCH', data: e.currentTarget.value })}
    />
  );
}

export default Input