import * as React from 'react'
import * as A from 'fp-ts/lib/Array'
import * as O from 'fp-ts/lib/Option'
import * as E from 'fp-ts/lib/Either'
import * as F from 'fp-ts/lib/function'
import * as S from 'fp-ts/lib/string'
import { StateContext } from '../../App';
import { Result, ResultListC } from "../../codecs";

import "./ResultsList.css";
import { useActor } from '@xstate/react';

const react_lift = (v: JSX.Element | Array<JSX.Element>) => <>{v}</>
const react_noop = (v: unknown) => () => <></>

type ListItemProps = {
  className?: string
  item: Result
  onSelect: () => any
}

const ListItem = (props: ListItemProps) =>
  <li className="ResultsList-item" onClick={props.onSelect}>
    <button className="ResultsList-button">
      {props.item.name}, {props.item.state.abbreviation}
    </button>
  </li>


type Props = {
  mixed?: any,
  className?: string
}
const ResultsList = (props: Props) => {
  const services = React.useContext(StateContext)
  const [current, send] = useActor(services.searchService)

  // Simple search that just lower cases both the term and the name
  const searchTerm = F.pipe(
    current.context.search,
    O.fold(F.constant(''), F.identity),
    S.toLowerCase,
    S.trim,
  )

  const searchItem = (term: string) => (item: Result) => F.pipe(
    item.name, S.toLowerCase, S.trim, S.startsWith(term)
  ) 

  const ListItems = F.pipe(
    current.context.results,
    O.fold(F.constant([]), F.identity),
    A.filter(searchItem(searchTerm)),
    A.mapWithIndex((idx, item) => <ListItem key={idx} item={item} onSelect={() => send({type: 'SEARCH_SELECT', data: item})} />),
    react_lift,
    F.constant,
  )

  const ListItemsComponent = F.pipe(
    current.value,
    E.fromPredicate(value => value === 'search_select', F.constFalse),
    E.fold(F.constant(ListItems), react_noop)
  )

  return (
    <ul className={"ResultsList " + (props.className || "")} {...props.mixed}>
      <ListItemsComponent />
    </ul>
  );
}

export default ResultsList
