import React from "react";

import ResultsList from "./components/ResultsList";
import Input from "./components/Input";
import Button from "./components/Button";
import { useInterpret, useActor } from '@xstate/react';
import { ActorRefFrom } from "xstate";
import { searchMachine, lazySearchMachine } from './machine'

import './App.css'



export type StateContext = {
  searchService: ActorRefFrom<typeof searchMachine>
}
export const StateContext = React.createContext({} as StateContext);


export default function App() {
  const searchService = useInterpret(lazySearchMachine());
  const [current, send] = useActor(searchService)

  return (
    <StateContext.Provider value={{ searchService }}>
      <section>
        {current.value.toString()}
        <div className="container">
          <Input />
          <Button />
        </div>
        <ResultsList />
      </section>
    </StateContext.Provider>
  );
}
