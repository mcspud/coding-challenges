Originally pulled from https://github.com/abcdigital/frontend-coding-exercise


Node16 or later.

`yarn dev` 


## Technical

Read `ARCHITECTURE.md`

There is a 1 second debounce from typing to an async call being made.  Once that is done and results are shown you can do real time client filtering, ie:

- type "syd" and wait for it load some results
- type "sydney dome" to see realtime client side filtering using the cached results