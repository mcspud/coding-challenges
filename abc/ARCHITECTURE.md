## Arch Decisions:
### Decision 1: Rewrite the Comments into Types 😅

I rewrote the entire thing in Typescript.  Passing the testing responsibility to the compiler instead of the human is always a good move.  Types are a far better indicator of what is happening to BOTH the programmer and the computer rather than comments.  The code is fully typed and commented with types 🤙

### Decision 2: Branded Types for Latitude and Longitude

Branded types were implemented in the `codec.ts` for Latitude and Longitude.  A branded type allows a refinement over a runtime value at compile time and fixes about 1 billion derpy human errors.  Here is an example, experienced by yours truly:

```
type Data = {
    contact_id:  string
    contract_id: string
}
data_from_some_endpoint:Data = {
    contact_id:  2c2c3e30-ec05-4306-b3bc-3c82390d2afd
    contract_id: a65804a3-d964-4f6f-b824-20d33bc89bcd
}

const get_contract_value(contract_uuid:string, contact_uuid:string) => ...

const value = () => get_contract_value(contact_uuid, contract_uuid)
// value: 0, expecting $10
```

Do you see the problem?
I've mixed up the order of `contact_id` and `contract_id`.  The compiler let the data through because a UUID is a string, but this is a stupid problem.  Using encoded types alerts the compiler in these situations, ie:

```
type Data = {
    contact_id: ContactId 
    contract_id: ContractId 
}
client_data_from_some_endpoint = {
    contact_id:  2c2c3e30-ec05-4306-b3bc-3c82390d2afd
    contract_id: a65804a3-d964-4f6f-b824-20d33bc89bcd
}

const get_contract_value(contract_uuid:ContractId, contact_uuid:ContactId) => ...

const value = () => get_contract_value(contact_uuid, contract_uuid)
// compile time error, u did dun goofed son
```

### Decision 3: Load relevant data into locally defined context

This is essentially a reimplementation of a Reader Monad (like in Haskell).  Prop passing leads to ugly an ugly and confusing code nightmare.  You are better to inject a local context/shared state between components and have them access what they need.

### Decision 4: Deterministic finite state automata driving logic

Lets us lift all logic entirely out of any components.  Also gives us compile time safety, and a human readable state tree.  No more nightmare of chained boolean ex checks like `k === true && d === false %% g.length > 0 && ...`.  FSM are the way to go for these things.

### Decision 5: Runtime type codecs

Assume forever and always that all incoming data is busted and/or broken and will break our invariants and assumptions.  Codecs were implemented to ensure our runtime values match our compile types at the boundary 🚀

### Decision 6: Sum of Product types for Australian States encoded in domain

The state data looks like this: 
```
type State = {
    name: string
    abbreviation: string
}
```

This doesn't mean we cant get stupid results like: 
```
data State = {
    name:         Western Australia 
    abbreviation: QLD
}
```
By defining the total set of {State} values as an invariant sum of product types we can make sure we don't confuse our users.

### Decision 7: Runtime search validation

Titles are lowercased and whitespace is trimmed when searching locally.  woooooooooooooo 🤙


# Future Improvements

- make the search cancellable.
- ~~make the button toggle depending on if it can or cant display stuff.~~  Implemented.
- enter in `aria` labels and tab navigation in the menu/search bar and results list


