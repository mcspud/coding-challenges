import * as t from "io-ts";
import { pipe } from 'fp-ts/lib/pipeable'
import { flow, constFalse, constTrue } from 'fp-ts/lib/function'
import { fold as foldB } from 'fp-ts/lib/boolean'
import { Either, left, right, chain, fold } from 'fp-ts/lib/Either'

// WhereOnEarthId
// ----------------------------
export const WhereOnEarthId = t.brand(
    t.number,
    (n): n is t.Branded<number, { readonly WhereOnEarthId: unique symbol }>  => pipe(n, Number.isInteger),
    'WhereOnEarthId'
)
export type WhereOnEarthId = (t.TypeOf<typeof WhereOnEarthId>)


// Id
// ----------------------------
export const Id = t.brand(
    t.number,
    (n): n is t.Branded<number, { readonly Id: unique symbol }>  => pipe(n, Number.isInteger),
    'Id'
)
export type Id = (t.TypeOf<typeof Id>)


// Temperature
// ----------------------------
export const Temperature = t.brand(
    t.number,
    (n): n is t.Branded<number, { readonly Temperature: unique symbol }>  => typeof n === 'number',
    // pipe(n, parseFloat, Number.isNaN, foldB(constTrue, constFalse)),
    'Temperature'
)
export type Temperature = (t.TypeOf<typeof Temperature>)


// LatLonGeoPair
// ----------------------------
export const canBisect = (s: string): Either<string, string> => (
    s.match(/,/g)?.length === 1 ? right(s) : left(s)
)

export const isPair = (s: string): Either<string, string> => (
    s.split(',').length === 2 ? right(s) : left(s)
)

export const isNum = (s: string) => pipe(
    s,
    (s) => s.split(',').filter(flow(Number, Number.isNaN, foldB(constTrue, constFalse))),
    (a) => a.length === 2 ? right(s) : left(s)
)

export const LatLonGeoPairV = flow(
    canBisect,
    chain(isPair),
    chain(isNum),
    fold(constFalse, constTrue)
)

export const LatLonGeoPair = t.brand(
    t.string,
    (n): n is t.Branded<string, { readonly LatLonGeoPair: unique symbol }> =>  LatLonGeoPairV(n),
    'LatLonGeoPair'
)
export type LatLonGeoPair = (t.TypeOf<typeof LatLonGeoPair>)


// Public Interface
// ----------------------------
export default {
    WhereOnEarthId,
    Id,
    Temperature,
    LatLonGeoPair,
}
