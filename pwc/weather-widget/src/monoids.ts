import * as c from './codecs'
import * as b from './brands'


export const Parent = (): c.Parent => ({
  title: 'Not Loaded',
  location_type: 'Continent',
  woeid: 0 as b.WhereOnEarthId,
  latt_long: '0,0' as b.LatLonGeoPair,
})


export const ApiResponse = (): c.ApiResponse => ({
  consolidated_weather: [],
  sources: [],
  parent: Parent(),
  time: Date.now().toString(),
  sun_rise: '',
  sun_set: '',
  timezone_name: '',
  title: 'Not loaded',
  location_type: 'Continent',
  woeid: 0 as b.WhereOnEarthId,
  latt_long: '0,0' as b.LatLonGeoPair,
  timezone: '',
})


// Public Interface
export default {
  ApiResponse,
}
