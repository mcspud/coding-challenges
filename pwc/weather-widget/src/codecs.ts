import * as t from "io-ts";
import b from './brands'

export const Condition = t.keyof({
  Snow: null,
  Sleet: null,
  Hail: null,
  Thunderstorm: null,
  'Heavy Rain': null,
  'Light Rain': null,
  Showers: null,
  'Heavy Cloud': null,
  'Light Cloud': null,
  Clear: null,
})
export type Condition = (t.TypeOf<typeof Condition>)

export const ConditionAbbreviation = t.keyof({
  sn: null,
  sl: null,
  h: null,
  t: null,
  hr: null,
  lr: null,
  s: null,
  hc: null,
  lc: null,
  c: null,
})
export type ConditionAbbreviation = (t.TypeOf<typeof ConditionAbbreviation>)


export const LocationType = t.keyof({
  'City': null, 
  'Region / State / Province': null, 
  'Country': null, 
  'Continent': null
})
export type LocationType = (t.TypeOf<typeof LocationType>)


export const Weather = t.exact(t.type({
  id: b.Id,
  weather_state_name: Condition,
  weather_state_abbr: ConditionAbbreviation,
  wind_direction_compass: t.string,
  created: t.string,         // could use ISO date
  applicable_date: t.string, // could use ISO date
  min_temp: b.Temperature,
  max_temp: b.Temperature,
  the_temp: b.Temperature,
  wind_speed: t.number,
  wind_direction: t.number,
  air_pressure: t.number,
  humidity: t.number,
  visibility: t.number,
  predictability: t.number,
}))
export type Weather = (t.TypeOf<typeof Weather>)


export const Source = t.exact(t.type({
  title: t.string,
  slug: t.string,
  url: t.string,
  crawl_rate: t.number,
}))
export type Source = (t.TypeOf<typeof Source>)


export const Sources = t.array(Source)
export type Sources = (t.TypeOf<typeof Sources>)

export const Parent = t.exact(t.type({
    title: t.string,
    location_type: LocationType,
    woeid: b.WhereOnEarthId,
    latt_long: b.LatLonGeoPair,
}))
export type Parent = t.TypeOf<typeof Parent>

export const ApiResponse = t.exact(t.type({
  consolidated_weather: t.array(Weather),
  time: t.string,               // DateFromISOString,
  sun_rise: t.string,           // DateFromISOString,
  sun_set: t.string,            // DateFromISOString,
  timezone_name: t.string,
  title: t.string,
  location_type: LocationType,
  woeid: b.WhereOnEarthId,
  latt_long: b.LatLonGeoPair,
  timezone: t.string,
  parent: Parent,
  sources: Sources,
}))
export type ApiResponse = (t.TypeOf<typeof ApiResponse>)

export const SearchResult = t.exact(t.type({
 title: t.string, 
 location_type: LocationType, 
 woeid: b.WhereOnEarthId, 
 latt_long:  b.LatLonGeoPair
}))
export type SearchResult = t.TypeOf<typeof SearchResult>

export const SearchAPIResponse = t.array(SearchResult)
export type SearchAPIResponse = t.TypeOf<typeof SearchAPIResponse>