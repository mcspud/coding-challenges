import React from 'react'

import {useMachine} from '@xstate/react'
import {fold} from 'fp-ts/lib/Option'
import {fromPredicate as fromPredicateE, fold as foldE} from 'fp-ts/lib/Either'
import {pipe} from 'fp-ts/lib/pipeable'
import {identity} from 'fp-ts/lib/function'

import * as a from '../automata'
import * as m from '../monoids'

import Search from './Search'
import Banner from './Banner'
import Day from './Day'

export type Props = {
  current: a.CurrentState
  send: a.Send
}

export const loadWeather = (send: a.Send) => (e: React.MouseEvent<HTMLDivElement>) => ( 
  send({type: a.Events.LOAD_LOCATION_WEATHER_START})
)

export const celciusToFahrenheit = (isFahrenheit: boolean, temp: number) => pipe(
  temp,
  fromPredicateE(v => isFahrenheit === true, identity),
  foldE(identity, v => v * 9/5 + 32),
  Math.round,
)

export const getSymbol = (isFahrenheit: boolean) => pipe(
  isFahrenheit,
  fromPredicateE(identity, identity),
  foldE(_ => 'C', _ => 'F'),
  s => <span>{'\u00b0'}{' '}{s}</span> 
)

export const dataM = (current: a.CurrentState) => (
  pipe(current.context.data.response, fold(m.ApiResponse, identity))
)

export default (): JSX.Element => {
  const [current, send] = useMachine(a.machine)

  const data = dataM(current)

  return (
    <div className='border border-grey-400 bg-white md:w-4/5 lg:w-2/3 p-3'>
      <Search current={current} send={send} />
      <Banner current={current} send={send} />
      <div className='flex'>
        {data.consolidated_weather.map(w => <Day weather={w} key={w.id}/>)}
      </div>
    </div>
  )

}

