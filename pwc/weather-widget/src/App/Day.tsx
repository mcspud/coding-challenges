import React from 'react'
import {celciusToFahrenheit, getSymbol} from './'

import * as c from '../codecs'
import Weather from '../Weather'

type Props = {
  weather: c.Weather
}
export default (props: Props) => {
  const [isFahrenheit, toggleIsFahrenheit] = React.useState(false)

  const min_temp = celciusToFahrenheit(isFahrenheit,props.weather.min_temp)
  const max_temp = celciusToFahrenheit(isFahrenheit,props.weather.max_temp)
  const humidity = props.weather.humidity.toFixed(0)
  const visibility = props.weather.visibility.toFixed(2)
  const day = new Date(props.weather.applicable_date).toLocaleString('en-au', { weekday: 'long' })
  const symbol = getSymbol(isFahrenheit)

  return (
    <div className='flex-1 m-1 justify-center'>
        <div className='bold text-center text-xs sm:text-sm md:text-lg lg:text-lg'>
          {day}
        </div>
        <div className='flex justify-center'>
          <Weather 
            className='xs:w-1/2 w-3/4'
            condition={props.weather.weather_state_abbr} />
        </div>
        <div className='flex cursor-pointer justify-center text-xs sm:text-sm' onClick={e => toggleIsFahrenheit(!isFahrenheit)}>
          <div className='text-gray-900'>{max_temp}{symbol}</div>
          <div className='text-gray-500 ml-1'>{min_temp}{symbol}</div>
        </div>
        <div className='flex justify-center text-xs'>
          Hum: {humidity}
        </div>
        <div className='flex justify-center text-xs'>
          Vis: {visibility}
        </div>
    </div>
  )
  }
