import * as React from 'react'
import classnames from 'classnames'
import {Transition} from '@headlessui/react'

import {fold, fromPredicate, isSome} from 'fp-ts/lib/Option'
import {pipe} from 'fp-ts/lib/pipeable'
import {identity, constant, constVoid} from 'fp-ts/lib/function'

import {Props} from './'
import * as c from '../codecs'
import * as a from '../automata'

const stringEmpty = constant('')

export const updateSearchString = (send: a.Send) => (e: React.FormEvent<HTMLInputElement>) => (
  send({type: a.Events.CHANGE_SEARCH_STRING, value: e.currentTarget.value})
)

export const sendSearchResult = (send: a.Send, current: a.CurrentState) => (e: React.KeyboardEvent<HTMLInputElement>) => (
  isSome(current.context.data.searchString) &&
  pipe(
    e.key,
    fromPredicate(k => k === 'Enter'),
    fold(constVoid, _ => send({type: a.Events.RUN_SEARCH}))
  )
)

export const getLoadingStatus = (current: a.CurrentState) => {
  switch(current.value) {
    case a.States.LOCATION_SEARCH_REQUEST:
      return <>Searching...</>
    case a.States.LOCATION_SEARCH_ERROR:
      return <>Error loading the request'</>
    default:
      return <>Press enter to search</>
  }
}

export const getSearchResults = (current: a.CurrentState) => pipe(
  current.context.data.searchResponse,
  fold(constant([]), identity)
)

export const setLocationId = (result: c.SearchResult, send: a.Send) => (e: React.MouseEvent<HTMLDivElement>) => (
  send({
      type: a.Events.CHANGE_LOCATION_ID,
      woeid: result.woeid,
  })
)

export const SearchResult = (props: {result: c.SearchResult, send: a.Send}) => (
  <div className='hover:bg-grey-100 px-2 py-1 cursor-pointer'
       onClick={setLocationId(props.result, props.send)}>
    {props.result.title}
  </div>
)
export default (props: Props): JSX.Element => {
  const [isOpen, setIsOpen] = React.useState(false)
  const toggleOpen = (e: React.SyntheticEvent) => (setIsOpen(!isOpen))
  const search = pipe(props.current.context.data.searchString, fold(stringEmpty, identity)) 
  const loadingStatus = getLoadingStatus(props.current)

  const buttonClasses = classnames(
    "inline-flex justify-center w-full px-4 py-2 text-sm font-medium text-gray-700", 
    "bg-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100",
    "focus:ring-indigo-500",
    isOpen && 'z-50'
  )

  const menuClasses = classnames(
    "absolute right-0 w-56 mt-2 bg-white shadow-lg origin-top-right rounded-md",
    "ring-1 ring-black ring-opacity-5",
    isOpen && 'z-40'
  )

  const searchResults = getSearchResults(props.current)

  return  (
    <div>
      <div className="relative inline-block text-left">
        <div>
          <button type="button"
            onClick={toggleOpen}
            className={buttonClasses}
            aria-haspopup="true"
            aria-expanded="true">
              <div className='flex'>
                <input 
                  type='text' 
                  placeholder='Search'
                  className='border border-grey px-3 py-2 rounded'
                  onKeyPress={sendSearchResult(props.send, props.current)}
                  onChange={updateSearchString(props.send)} 
                  value={search}/>
              </div>
              <svg className="w-5 h-5 ml-2 -mr-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
              </svg>
          </button>
        </div>

        <Transition
          appear={false}
          show={isOpen}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95">
          <div className='z-30 opacity-0 w-full h-full fixed left-0 top-0' 
               onClick={toggleOpen}/>
            <div className={menuClasses} 
                 onClick={e => setIsOpen(false)}>
              <div className="py-1"
                role="menu"
                aria-orientation="vertical"
                aria-labelledby="options-menu">
                  <div>{loadingStatus}</div>
                  <div>{searchResults.map(r => <SearchResult result={r} send={props.send} />)}</div>
              </div>
            </div>
        </Transition>
      </div>
    </div>
  )
}

