export const data1 = {
    "consolidated_weather": [
        {
            "id": 5846657041170432,
            "weather_state_name": "Light Cloud",
            "weather_state_abbr": "lc",
            "wind_direction_compass": "N",
            "created": "2020-12-02T21:20:16.786982Z",
            "applicable_date": "2020-12-02",
            "min_temp": 9.725,
            "max_temp": 16.134999999999998,
            "the_temp": 13.68,
            "wind_speed": 4.076130189803547,
            "wind_direction": 359.0,
            "air_pressure": 1020.5,
            "humidity": 54,
            "visibility": 14.759740614809512,
            "predictability": 70
        },
        {
            "id": 4624971277008896,
            "weather_state_name": "Clear",
            "weather_state_abbr": "c",
            "wind_direction_compass": "NE",
            "created": "2020-12-02T21:20:20.146514Z",
            "applicable_date": "2020-12-03",
            "min_temp": 8.91,
            "max_temp": 16.67,
            "the_temp": 14.254999999999999,
            "wind_speed": 3.0338116339192447,
            "wind_direction": 41.61035453612921,
            "air_pressure": 1025.5,
            "humidity": 59,
            "visibility": 15.033765310586176,
            "predictability": 68
        },
        {
            "id": 6724775721304064,
            "weather_state_name": "Light Cloud",
            "weather_state_abbr": "lc",
            "wind_direction_compass": "NNE",
            "created": "2020-12-02T21:20:22.889972Z",
            "applicable_date": "2020-12-04",
            "min_temp": 9.355,
            "max_temp": 17.155,
            "the_temp": 15.805,
            "wind_speed": 3.342568597905565,
            "wind_direction": 27.83356698802847,
            "air_pressure": 1024.5,
            "humidity": 47,
            "visibility": 15.72038047800843,
            "predictability": 70
        },
        {
            "id": 5921453762412544,
            "weather_state_name": "Light Cloud",
            "weather_state_abbr": "lc",
            "wind_direction_compass": "N",
            "created": "2020-12-02T21:20:25.661817Z",
            "applicable_date": "2020-12-05",
            "min_temp": 9.125,
            "max_temp": 15.27,
            "the_temp": 14.115,
            "wind_speed": 3.293299926124764,
            "wind_direction": 7.243031887443663,
            "air_pressure": 1024.0,
            "humidity": 54,
            "visibility": 14.800751113497176,
            "predictability": 70
        },
        {
            "id": 4719341640613888,
            "weather_state_name": "Clear",
            "weather_state_abbr": "c",
            "wind_direction_compass": "N",
            "created": "2020-12-02T21:20:28.781492Z",
            "applicable_date": "2020-12-06",
            "min_temp": 9.085,
            "max_temp": 14.27,
            "the_temp": 14.16,
            "wind_speed": 3.5694907195630847,
            "wind_direction": 349.0,
            "air_pressure": 1024.0,
            "humidity": 69,
            "visibility": 14.005085301837271,
            "predictability": 68
        },
        {
            "id": 5582132421328896,
            "weather_state_name": "Clear",
            "weather_state_abbr": "c",
            "wind_direction_compass": "NNE",
            "created": "2020-12-02T21:20:31.841633Z",
            "applicable_date": "2020-12-07",
            "min_temp": 9.175,
            "max_temp": 17.84,
            "the_temp": 17.87,
            "wind_speed": 7.348922094965402,
            "wind_direction": 31.000000000000007,
            "air_pressure": 1020.0,
            "humidity": 44,
            "visibility": 9.999726596675416,
            "predictability": 68
        }
    ],
    "time": "2020-12-02T15:14:17.577239-08:00",
    "sun_rise": "2020-12-02T07:07:49.498892-08:00",
    "sun_set": "2020-12-02T16:50:53.742372-08:00",
    "timezone_name": "LMT",
    "parent": {
        "title": "California",
        "location_type": "Region / State / Province",
        "woeid": 2347563,
        "latt_long": "37.271881,-119.270233"
    },
    "sources": [
        {
            "title": "BBC",
            "slug": "bbc",
            "url": "http://www.bbc.co.uk/weather/",
            "crawl_rate": 360
        },
        {
            "title": "Forecast.io",
            "slug": "forecast-io",
            "url": "http://forecast.io/",
            "crawl_rate": 480
        },
        {
            "title": "HAMweather",
            "slug": "hamweather",
            "url": "http://www.hamweather.com/",
            "crawl_rate": 360
        },
        {
            "title": "Met Office",
            "slug": "met-office",
            "url": "http://www.metoffice.gov.uk/",
            "crawl_rate": 180
        },
        {
            "title": "OpenWeatherMap",
            "slug": "openweathermap",
            "url": "http://openweathermap.org/",
            "crawl_rate": 360
        },
        {
            "title": "Weather Underground",
            "slug": "wunderground",
            "url": "https://www.wunderground.com/?apiref=fc30dc3cd224e19b",
            "crawl_rate": 720
        },
        {
            "title": "World Weather Online",
            "slug": "world-weather-online",
            "url": "http://www.worldweatheronline.com/",
            "crawl_rate": 360
        }
    ],
    "title": "San Francisco",
    "location_type": "City",
    "woeid": 2487956,
    "latt_long": "37.777119, -122.41964",
    "timezone": "US/Pacific"
}


export const data2 = {
    "consolidated_weather":[
        {
            "id":6505201121886208,
            "weather_state_name":"Light Cloud",
            "weather_state_abbr":"lc",
            "wind_direction_compass":"NNE",
            "created":"2020-12-03T21:20:17.515752Z",
            "applicable_date":"2020-12-03",
            "min_temp":9.965,
            "max_temp":16.65,
            "the_temp":14.345,
            "wind_speed":3.6561916889100985,
            "wind_direction":32.22652112674949,
            "air_pressure":1025.0,
            "humidity":56,
            "visibility":15.097145172194384,
            "predictability":70
        },
        {
            "id":5282717466886144,
            "weather_state_name":"Clear",
            "weather_state_abbr":"c",
            "wind_direction_compass":"NNE",
            "created":"2020-12-03T21:20:20.114733Z",
            "applicable_date":"2020-12-04",
            "min_temp":9.54,
            "max_temp":17.25,
            "the_temp":15.455,
            "wind_speed":3.1514153061003736,
            "wind_direction":29.000000000000004,
            "air_pressure":1024.5,
            "humidity":51,
            "visibility":15.414976537023781,
            "predictability":68
        },
        {
            "id":6349681966710784,
            "weather_state_name":"Heavy Cloud",
            "weather_state_abbr":"hc",
            "wind_direction_compass":"NNW",
            "created":"2020-12-03T21:20:22.996103Z",
            "applicable_date":"2020-12-05",
            "min_temp":9.15,
            "max_temp":14.98,
            "the_temp":14.649999999999999,
            "wind_speed":2.742023006901031,
            "wind_direction":336.97026339376947,
            "air_pressure":1024.0,
            "humidity":58,
            "visibility":14.817528135687585,
            "predictability":71
        },
        {
            "id":5273581702348800,
            "weather_state_name":"Heavy Cloud",
            "weather_state_abbr":"hc",
            "wind_direction_compass":"NNW",
            "created":"2020-12-03T21:20:26.064388Z",
            "applicable_date":"2020-12-06",
            "min_temp":9.535,
            "max_temp":14.395,
            "the_temp":13.965,
            "wind_speed":4.178026362631566,
            "wind_direction":339.4752488256829,
            "air_pressure":1024.5,
            "humidity":73,
            "visibility":12.469677085818818,
            "predictability":71
        },
        {
            "id":6558843686354944,
            "weather_state_name":"Clear",
            "weather_state_abbr":"c",
            "wind_direction_compass":"NNE",
            "created":"2020-12-03T21:20:28.968829Z",
            "applicable_date":"2020-12-07",
            "min_temp":9.445,
            "max_temp":17.545,
            "the_temp":17.54,
            "wind_speed":6.445543140544554,
            "wind_direction":30.304818495801335,
            "air_pressure":1020.5,
            "humidity":48,
            "visibility":15.483327368169888,
            "predictability":68
        },
        {
            "id":6573028184948736,
            "weather_state_name":"Light Cloud",
            "weather_state_abbr":"lc",
            "wind_direction_compass":"N",
            "created":"2020-12-03T21:20:31.830087Z",
            "applicable_date":"2020-12-08",
            "min_temp":9.77,
            "max_temp":16.205,
            "the_temp":15.54,
            "wind_speed":3.107921538216814,
            "wind_direction":10.500000000000009,
            "air_pressure":1019.0,
            "humidity":48,
            "visibility":9.999726596675416,
            "predictability":70
        }
    ],
    "time":"2020-12-03T13:40:34.859673-08:00",
    "sun_rise":"2020-12-03T07:08:44.560864-08:00",
    "sun_set":"2020-12-03T16:50:45.062396-08:00",
    "timezone_name":"LMT",
    "parent":{
        "title":"California",
        "location_type":"Region / State / Province",
        "woeid":2347563,
        "latt_long":"37.271881,-119.270233"
    },
    "sources":[
        {
            "title":"BBC",
            "slug":"bbc",
            "url":"http://www.bbc.co.uk/weather/",
            "crawl_rate":360
        },
        {
            "title":"Forecast.io",
            "slug":"forecast-io",
            "url":"http://forecast.io/",
            "crawl_rate":480
        },
        {
            "title":"HAMweather",
            "slug":"hamweather",
            "url":"http://www.hamweather.com/",
            "crawl_rate":360
        },
        {
            "title":"Met Office",
            "slug":"met-office",
            "url":"http://www.metoffice.gov.uk/",
            "crawl_rate":180
        },
        {
            "title":"OpenWeatherMap",
            "slug":"openweathermap",
            "url":"http://openweathermap.org/",
            "crawl_rate":360
        },
        {
            "title":"Weather Underground",
            "slug":"wunderground",
            "url":"https://www.wunderground.com/?apiref=fc30dc3cd224e19b",
            "crawl_rate":720
        },
        {
            "title":"World Weather Online",
            "slug":"world-weather-online",
            "url":"http://www.worldweatheronline.com/",
            "crawl_rate":360
        }
    ],
    "title":"San Francisco",
    "location_type":"City",
    "woeid":2487956,
    "latt_long":"37.777119, -122.41964",
    "timezone":"US/Pacific"
}
export const searchRespons = [{ "title": "San Francisco", "location_type": "City", "woeid": 2487956, "latt_long": "37.777119, -122.41964" }, { "title": "San Diego", "location_type": "City", "woeid": 2487889, "latt_long": "32.715691,-117.161720" }, { "title": "San Jose", "location_type": "City", "woeid": 2488042, "latt_long": "37.338581,-121.885567" }, { "title": "San Antonio", "location_type": "City", "woeid": 2487796, "latt_long": "29.424580,-98.494614" }, { "title": "Santa Cruz", "location_type": "City", "woeid": 2488853, "latt_long": "36.974018,-122.030952" }, { "title": "Santiago", "location_type": "City", "woeid": 349859, "latt_long": "-33.463039,-70.647942" }, { "title": "Santorini", "location_type": "City", "woeid": 56558361, "latt_long": "36.406651,25.456530" }, { "title": "Santander", "location_type": "City", "woeid": 773964, "latt_long": "43.461498,-3.810010" }, { "title": "Busan", "location_type": "City", "woeid": 1132447, "latt_long": "35.170429,128.999481" }, { "title": "Santa Cruz de Tenerife", "location_type": "City", "woeid": 773692, "latt_long": "28.46163,-16.267059" }, { "title": "Santa Fe", "location_type": "City", "woeid": 2488867, "latt_long": "35.666431,-105.972572" }]