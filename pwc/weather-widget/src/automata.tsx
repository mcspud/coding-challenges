import { Machine, Interpreter, MachineConfig, assign, DoneInvokeEvent, State } from "xstate";
import { Option, none, some, fold, fromEither, fromNullable, fromPredicate } from 'fp-ts/lib/Option'
import { pipe } from 'fp-ts/lib/pipeable'
import { Lens } from 'monocle-ts'

import async from './async'
import * as c from './codecs'
import * as b from './brands'
import { constant } from "fp-ts/lib/function";
import { Errors, identity } from "io-ts";
import { Either } from "fp-ts/lib/Either";

// States
export enum States {
  LOAD_LOCATION_WEATHER_BASE = 'LOAD_LOCATION_WEATHER_BASE',
  LOAD_LOCATION_WEATHER_REQUEST = 'LOAD_LOCATION_WEATHER_REQUEST',
  LOAD_LOCATION_WEATHER_SUCCESS = 'LOAD_LOCATION_WEATHER_SUCCESS',
  LOAD_LOCATION_WEATHER_ERROR = 'LOAD_LOCATION_WEATHER_ERROR',
  
  LOCATION_SEARCH_REQUEST = 'SEARCH_LOCATION_REQUEST',
  LOCATION_SEARCH_SUCCESS = 'SEARCH_LOCATION_SUCCESS',
  LOCATION_SEARCH_ERROR = 'SEARCH_LOCATION_ERROR',
}

export type AutomataStates = {
  states: {
    [States.LOAD_LOCATION_WEATHER_BASE]: {},
    [States.LOAD_LOCATION_WEATHER_REQUEST]: {},
    [States.LOAD_LOCATION_WEATHER_SUCCESS]: {},
    [States.LOAD_LOCATION_WEATHER_ERROR]: {},
    [States.LOCATION_SEARCH_REQUEST]: {},
    [States.LOCATION_SEARCH_SUCCESS]: {},
    [States.LOCATION_SEARCH_ERROR]: {},
  }
}


// Context
export type AutomataContext = {
  data: {
    locationId: b.WhereOnEarthId
    response: Option<c.ApiResponse>
    searchString: Option<string>
    searchResponse: Option<c.SearchAPIResponse>
  }
}

export const AutomataContext = (): AutomataContext => ({
  data: {
    locationId: 1103816 as b.WhereOnEarthId,
    response: none,
    searchString: none,
    searchResponse: none
  }
})


// Events
export enum Events {
  CHANGE_SEARCH_STRING = 'CHANGE_SEARCH_STRING',
  CHANGE_LOCATION_ID = 'CHANGE_LOCATION_ID',
  RUN_SEARCH = 'RUN_SEARCH',
  LOAD_LOCATION_WEATHER_START = 'LOAD_LOCATION_WEATHER',
  LOAD_LOCATION_WEATHER_RETRY = 'LOAD_LOCATION_WEATHER_RETRY',
}

type CHANGE_SEARCH_STRING = { type: Events.CHANGE_SEARCH_STRING, value: string }
type RUN_SEARCH = { type: Events.RUN_SEARCH }
type LOAD_LOCATION_WEATHER_START = { type: Events.LOAD_LOCATION_WEATHER_START }
type LOAD_LOCATION_WEATHER_RETRY = { type: Events.LOAD_LOCATION_WEATHER_RETRY }
type CHANGE_LOCATION_ID = { type: Events.CHANGE_LOCATION_ID, woeid: b.WhereOnEarthId }
export type AutomataEvent =
  | CHANGE_SEARCH_STRING
  | CHANGE_LOCATION_ID
  | RUN_SEARCH
  | LOAD_LOCATION_WEATHER_START
  | LOAD_LOCATION_WEATHER_RETRY


// Services
export const loadWeather = async (c: AutomataContext, e: LOAD_LOCATION_WEATHER_START): Promise<Option<c.ApiResponse>> => (
  pipe(
    await async.location(c.data.locationId),
    fromEither,
    fold(
      () => Promise.reject(none),
      r => Promise.resolve(some(r))
    )
  )
)

export const searchLocation = (c: AutomataContext, e: LOAD_LOCATION_WEATHER_START) => pipe( 
  c.data.searchString,
  fold(Promise.reject, async.search)
)
 

// Actions
export const assignDataToContext = assign((c: AutomataContext, e: DoneInvokeEvent<Option<c.ApiResponse>>): AutomataContext =>
  Lens.fromPath<AutomataContext>()(['data', 'response']).set(e.data)(c)
)

export const assignSearchStringToContext = assign((c: AutomataContext, e: CHANGE_SEARCH_STRING): AutomataContext => {
  const lens = Lens.fromPath<AutomataContext>()(['data', 'searchString'])
  return pipe(
    e.value, 
    fromNullable,
    fold(() => none, fromPredicate(v => v.length > 0)),
    fold(
      () => lens.set(none)(c),
      v => lens.set(some(v))(c),
      )
    )
})

export const assignSearchResultToContext = assign((c: AutomataContext, e: DoneInvokeEvent<Either<Errors, c.SearchAPIResponse>>): AutomataContext => {
  const lens = Lens.fromPath<AutomataContext>()(['data', 'searchResponse'])
  return pipe(
    e.data,
    fromEither,
    fold(constant(none), r => some(r)),
    lens.set, f => f(c)
  )
})

export const changeLocationId = assign((c: AutomataContext, e: CHANGE_LOCATION_ID) => 
  Lens.fromPath<AutomataContext>()(['data', 'locationId']).set(e.woeid)(c)
)

// Config
export const config: MachineConfig<AutomataContext, AutomataStates, AutomataEvent> = {
  id: 'Machine::LOAD_WEATHER',
  initial: States.LOAD_LOCATION_WEATHER_BASE,
  context: AutomataContext(),
  states: {
    [States.LOAD_LOCATION_WEATHER_BASE]: {
      after: {
        500: {target: States.LOAD_LOCATION_WEATHER_REQUEST}
      },
      on: {
        [Events.LOAD_LOCATION_WEATHER_START]: {
          target: States.LOAD_LOCATION_WEATHER_REQUEST
        },
      }
    },
    [States.LOAD_LOCATION_WEATHER_REQUEST]: {
      invoke: {
        src: 'loadWeather',
        onDone: {
          target: States.LOAD_LOCATION_WEATHER_SUCCESS,
          actions: [
            'assignDataToContext',
          ]
        },
        onError: {
          target: States.LOAD_LOCATION_WEATHER_ERROR,
          actions: [],
        },
      }
    },
    [States.LOAD_LOCATION_WEATHER_SUCCESS]: {
      on: {
        [Events.LOAD_LOCATION_WEATHER_RETRY]: {
          target: States.LOAD_LOCATION_WEATHER_REQUEST
        },
        [Events.LOAD_LOCATION_WEATHER_START]: {
          actions: ['assignSearchStringToContext'],
        },
        [Events.CHANGE_SEARCH_STRING]: {
          actions: ['assignSearchStringToContext',],
        },
        [Events.RUN_SEARCH]: {
          target: States.LOCATION_SEARCH_REQUEST,
        }
      }
    },
    [States.LOAD_LOCATION_WEATHER_ERROR]: {
      on: {
        [Events.LOAD_LOCATION_WEATHER_RETRY]: {
          target: States.LOAD_LOCATION_WEATHER_REQUEST
        },
        [Events.LOAD_LOCATION_WEATHER_START]: {
          actions: ['assignSearchStringToContext'],
        }
      }
    },
    [States.LOCATION_SEARCH_REQUEST]: {
      invoke: {
        src: 'searchLocation',
        onDone: {
          target: States.LOCATION_SEARCH_SUCCESS,
          actions: ['assignSearchResultToContext']
        },
        onError: {
          target: States.LOCATION_SEARCH_ERROR
        }
      }
    },
    [States.LOCATION_SEARCH_SUCCESS]: {},
    [States.LOCATION_SEARCH_ERROR]: {},
  },
  on: {
    [Events.CHANGE_SEARCH_STRING]: {
      actions: ['assignSearchStringToContext',],
    },
    [Events.RUN_SEARCH]: {
      target: States.LOCATION_SEARCH_REQUEST,
    },
    [Events.CHANGE_LOCATION_ID]: {
      target: States.LOAD_LOCATION_WEATHER_REQUEST,
      actions: ['changeLocationId']
    }
  }
}

export const options: any = {
  services: {
    loadWeather,
    searchLocation,
  },
  actions: {
    assignDataToContext,
    assignSearchStringToContext,
    assignSearchResultToContext,
    changeLocationId,
  }
}

export type AutomataService = (Interpreter<AutomataContext, AutomataStates, AutomataEvent>)
export type CurrentState = (State<AutomataContext, AutomataEvent>)
export type Machine = (MachineConfig<AutomataContext, AutomataStates, AutomataEvent>)
export type Send = AutomataService['send']
export type Service = (Interpreter<AutomataContext, AutomataStates, AutomataEvent>)

export const machine = Machine(config, options)
export default machine
