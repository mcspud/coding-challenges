import * as React from "react"

function SvgComponent(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      viewBox="0 0 18 18"
      {...props}
    >
      <defs>
        <path id="prefix__a" d="M0 0h18v18H0z" />
      </defs>
      <clipPath id="prefix__b">
        <use xlinkHref="#prefix__a" overflow="visible" />
      </clipPath>
      <path
        d="M14.5 3c-.213 0-.42.026-.622.062A3.995 3.995 0 0010 0a4 4 0 00-4 4 .5.5 0 01-1 0c0-.622.128-1.212.337-1.762A3.978 3.978 0 004 2a4 4 0 000 8h10.5a3.5 3.5 0 100-7"
        clipPath="url(#prefix__b)"
        fillRule="evenodd"
        clipRule="evenodd"
        fill="#bebdbd"
      />
      <path
        d="M8 13.8c0 .664-.447 1.2-1 1.2s-1-.536-1-1.2c0-.663 1-2.8 1-2.8s1 2.137 1 2.8M4 13.8c0 .664-.447 1.2-1 1.2s-1-.536-1-1.2c0-.663 1-2.8 1-2.8s1 2.137 1 2.8M16 13.8c0 .664-.447 1.2-1 1.2s-1-.536-1-1.2c0-.663 1-2.8 1-2.8s1 2.137 1 2.8M12 13.8c0 .664-.447 1.2-1 1.2s-1-.536-1-1.2c0-.663 1-2.8 1-2.8s1 2.137 1 2.8M10 16.8c0 .664-.447 1.2-1 1.2-.553 0-1-.536-1-1.2 0-.663 1-2.8 1-2.8s1 2.137 1 2.8M6 16.8c0 .664-.447 1.2-1 1.2s-1-.536-1-1.2c0-.663 1-2.8 1-2.8s1 2.137 1 2.8M14 16.8c0 .664-.447 1.2-1 1.2s-1-.536-1-1.2c0-.663 1-2.8 1-2.8s1 2.137 1 2.8"
        clipPath="url(#prefix__b)"
        fillRule="evenodd"
        clipRule="evenodd"
        fill="#00697d"
      />
    </svg>
  )
}

export default SvgComponent
