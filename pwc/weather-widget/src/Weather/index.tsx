import React from 'react'
import * as codecs from '../codecs'

import C from './c'
import H from './h'
import S from './s'
import SL from './sl'
import SN  from './sn'
import HR from './hr'
import LR from './lr'
import T from './t'
import HC from './hc'
import LC from './lc'


type Props = {
    condition: codecs.ConditionAbbreviation
    className?: string
}

export default (props: Props) => {
    switch (props.condition) {
        case 'c':
            return <C {...props} />
        case 'h':
            return <H {...props} />
        case 's':
            return <S {...props} />
        case 'sl':
            return <SL {...props} />
        case 'sn':
            return <SN {...props} />
        case 'hr':
            return <HR {...props} />
        case 'lr':
            return <LR {...props} />
        case 't':
            return <T {...props} />
        case 'hc':
            return <HC {...props} />
        case 'lc':
            return <LC {...props} />
        default:
            const _: never = props.condition
            return <div />
    }
}
