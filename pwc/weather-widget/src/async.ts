import axios, {AxiosResponse} from "axios";
import {Lens} from "monocle-ts";
import {ApiResponse, SearchAPIResponse} from './codecs'
import {WhereOnEarthId} from './brands'

export const location = (woeid: WhereOnEarthId) => (
  axios.get(`https://www.metaweather.com/api/location/${woeid}/`)
    .then(Lens.fromProp<AxiosResponse>()('data').get)
    .then(ApiResponse.decode)
)

export const search = (searchString: string) => (
  axios.get(`https://www.metaweather.com/api/location/search/?query=${searchString}`)
    .then(Lens.fromProp<AxiosResponse>()('data').get)
    .then(SearchAPIResponse.decode)
)

export default {
  location,
  search,
}