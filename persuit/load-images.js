// Asynchronously load multiple images and average out their dimensions
// https://codepen.io/mcspud/pen/OegeoM

const loadImage = url => new Promise(
    (resolve, reject) => {
        const img = new Image();
        img.onload = () => resolve({ url, img, success: true });
        img.onerror = () => resolve({ url, img, success: false });
        img.src = url;
    })


const solution = urls => Promise
        .all(urls.map(loadImage))
        .then(results => results.filter(result => result.success === true))
        .then(results => results.reduce((acc, result) => ({
            width: acc.width + result.img.width,
            height: acc.height + result.img.height,
            count: acc.count + 1
        }), { 
            width: 0, 
            height: 0, 
            count: 0,
        }))
        .then(result => ({
            width: result.width / result.count,
            height: result.height / result.count,
        }))