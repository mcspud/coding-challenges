// Given a list of items, a page length and a starting page, return the 
// list of characters for that page.
// Some business rules:
// - If the result is an empty list, return null instead.
// - If the page is negative, convert to a positive
// - A page of 0 is the same as a page of 1
// https://codepen.io/mcspud/pen/YoQdwy

const solution = (pageNumber, itemsPerPage, pageData) => {
    const result = [pageNumber]
        .map(num => num === 0 ? 1 : num)
        .map(Math.abs)
        .map(idx => idx - 1)
        .map(idx => idx * itemsPerPage)
        .reduce((data, start) => data.slice(start, start + itemsPerPage), pageData)

    return result.length === 0 ? null : result
}