// Convert an objects values into an array.
// https://codepen.io/mcspud/pen/wLeXOV

const solution = obj => 
    Object.values(obj)
