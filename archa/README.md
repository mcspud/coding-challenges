### Setup

- `docker compose build`
- `docker compose up`

In another terminal window run

- `sh manage.py migrate`
- `sh manage.py seed`

Navigate to:

- `localhost:8000/api` 


### Description

This is implemented with HATEOAS.  Each resource has a navigable hyperlink that will load it from the database.

4 users have been created:

- `user_1a` - admin user for scenario 1
- `user_1b` - general user for scenario 1
- `user_2a` - admin user for scenario 2
- `user_2b` - general user for scenario 2


#### Scenario 1 - user_1a

Log in as `user_1a / password` in the top left of the page from `localhost:8000/api`

![Login](./media/login.png)

Navigate to `/api/me`.  Notice that there is a company under the `admin_companies` field.  This is a list of companies that this user is an admin for.  

![Image](./media/user_1a-me.png)

Clicking the link will load up the company resource, again implemented in HATEOAS. you will see 2 credit cards attached to this company - one for the `user_1a` user and one for the `user_1b` user.

![Image](./media/user_1a-company.png)

Clicking the card resource hyperlink for `user_1b` will load a screen, with a PUT/update option for `limit` field at the bottom.

![Image](./media/user_1a_cc-user_1b.png)

Return back to the `company resource` screen and access the credit card for `user_1a`.  You will see something similar to the following:

![Image](./media/user_1a_cc-user_1a.png)

This user is an admin of the company, but CANNOT change their own CC balance.  This is an example of `SCENARIO 3`.  The code for this can be found in:

- `archa/credit_cards/permissions:IsCompanyAdmin` and `archa/credit_cards/viewsets:CreditCardViewSet.permission_classes`

#### Scenario 1 - user_1b

Log in as `user_1a / password` in the top left of the page from `localhost:8000/api`

![Login](./media/login.png)

Navigate to `/api/me`.  Notice that there is a company under the `admin_companies` field.  This is a list of companies that this user is an admin for.  You will see that this user has no admin options.

![Image](./media/user_1b-me.png)

Going to the `/api/card/` you will see a single card that you can access by following its hyperlink.  You will notice that there is no option to modify the limit unlike `user_1a`.

#### Scenario 2 - user_2a and user_2b

Same as scenario 1, but navigating to `api/companies` will show multiple companies.  

The same rules apply that implement an example of `SCENARIO 3` - per-resource based permissions (`credit card` in thie case), restricting user access to update their own limits.


## Bonus

- Transactions are implemented per credit card.
- These are generated randomly when running `seed`
- These transactions are scoped intuitively, ie:
  - Company admin users can see all those transactions related to companies that the user is an admin of
  - It also includes their personal transactions
  - Non-admin users can only see their personal transactions
- Database is under `archa/db.sqlite3` and can be deleted and recreated at any time.  You'll need to restart docker.