from rest_framework import serializers
from . import models
from django.core.validators import RegexValidator


class NestedTransactionSerializer(serializers.Serializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='transaction-detail',
        lookup_field='identifier',
    )
    identifier = serializers.UUIDField(read_only=True)
    created = serializers.DateTimeField(read_only=True)
    amount = serializers.DecimalField(
        read_only=True,
        max_digits=7,
        decimal_places=2,
    )


class NestedCompanySerializer(serializers.Serializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='company-detail',
        lookup_field='identifier',
    )
    identifier = serializers.UUIDField()
    name = serializers.CharField()


class NestedUserSerializer(serializers.Serializer):
    username = serializers.CharField()
    email = serializers.CharField()


class CreditCardSerializer(serializers.Serializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='creditcard-detail',
        lookup_field='identifier',
    )
    identifier = serializers.UUIDField(read_only=True)
    number = serializers.SerializerMethodField()
    expiry_date = serializers.CharField(
        read_only=True,
        validators=[RegexValidator(r'(^(^0?[1-9]|10|11|12$)/([2-3][0-9])$)'), ]
    )
    limit = serializers.DecimalField(
        decimal_places=2, 
        max_digits=7,
    )
    balance = serializers.DecimalField(
        read_only=True,
        decimal_places=2,
        max_digits=7,
    )
    user = NestedUserSerializer(read_only=True)
    company = NestedCompanySerializer(read_only=True)
    transactions = NestedTransactionSerializer(
        read_only=True,
        source='transaction_set',
        many=True,
    )

    # Totally PCI compliant
    def get_number(self, instance: models.CreditCard):
        return f"**** **** **** {instance.number[-4:]}"

    def update(self, instance, validated_data):
        for k, v in validated_data.items():
            setattr(instance, k, v)
        instance.save()
        return instance


class CreditCardNestedSerializer(serializers.Serializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='creditcard-detail',
        lookup_field='identifier',
    )
    identifier = serializers.UUIDField(read_only=True)
    limit = serializers.DecimalField(
        decimal_places=2, 
        max_digits=10,
    )
    balance = serializers.DecimalField(
        decimal_places=2, 
        max_digits=10,
    )


class TransactionSerializer(serializers.Serializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='transaction-detail',
        lookup_field='identifier',
    )
    identifier = serializers.UUIDField(read_only=True)
    created = serializers.DateTimeField(read_only=True)
    amount = serializers.CharField(read_only=True)
    credit_card = CreditCardNestedSerializer(read_only=True)
