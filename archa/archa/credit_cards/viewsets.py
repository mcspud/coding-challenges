from rest_framework import viewsets, mixins, authentication, permissions
from . import models, serializers, permissions as perms


class CardViewSet(mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  viewsets.GenericViewSet):

    lookup_field = 'identifier'
    serializer_class = serializers.CreditCardSerializer
    authentication_classes = [authentication.SessionAuthentication] 
    permission_classes = [permissions.IsAuthenticated] 

    def get_queryset(self, **kwargs):
        return models.CreditCard.objects.filter(
            user=self.request.user,
        )


class CreditCardViewSet(mixins.UpdateModelMixin,
                        mixins.ListModelMixin,
                        mixins.RetrieveModelMixin,
                        viewsets.GenericViewSet):

    lookup_field = 'identifier'
    serializer_class = serializers.CreditCardSerializer
    authentication_classes = [authentication.SessionAuthentication]
    permission_classes = [
        permissions.IsAuthenticated, 
        perms.IsCompanyAdmin,
    ] 

    def get_queryset(self, **kwargs):
        qs1 = models.CreditCard.objects.filter(user=self.request.user)
        qs2 = models.CreditCard.objects.filter(company__in=self.request.user.admin_set.all())
        return models.CreditCard.objects.filter(id__in=qs1.union(qs2).values('id'))



class TransactionViewSet(mixins.UpdateModelMixin,
                         mixins.ListModelMixin,
                         mixins.RetrieveModelMixin,
                         viewsets.GenericViewSet):

    lookup_field = 'identifier'
    serializer_class = serializers.TransactionSerializer
    authentication_classes = [authentication.SessionAuthentication]
    permission_classes = [permissions.IsAuthenticated] 

    def get_queryset(self, **kwargs):
        qs1 = models.CreditCard.objects.filter(user=self.request.user)
        qs2 = models.CreditCard.objects.filter(company__in=self.request.user.admin_set.all())
        company_ids = qs1.union(qs2).values('id')
        return models.Transaction.objects.filter(credit_card__id__in=company_ids)