import decimal
import uuid

from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.validators import RegexValidator, MinValueValidator, MinLengthValidator
from django.utils.timezone import now


class CreditCard(models.Model):
    identifier = models.UUIDField(default=uuid.uuid4)
    number = models.CharField(
        max_length=16,
        validators=[RegexValidator(r'[0-9]{16}')],
    ) 
    expiry_date = models.CharField(
        max_length=5,
        validators=[
            RegexValidator(r'(^(^0?[1-9]|10|11|12$)/([2-3][0-9])$)'),
            MinLengthValidator(5),
        ],
    )
    user = models.ForeignKey(
        to=get_user_model(),
        on_delete=models.CASCADE,
    )
    company = models.ForeignKey(
        to='company.Company',
        on_delete=models.CASCADE,
    )
    limit = models.DecimalField(
        max_digits=7,
        decimal_places=2,
        validators=[MinValueValidator(0)],
    )

    @property
    def _consumed(self) -> decimal.Decimal:
        record = self.transaction_set.aggregate(sum=models.Sum('amount'))
        if record['sum'] is None:
            return 0
        return record['sum']

    @property
    def balance(self) -> decimal.Decimal:
        return self.limit - self._consumed

    def save(self, **kwargs) -> None:
        return super().save(**kwargs)


class Transaction(models.Model):
    identifier = models.UUIDField(default=uuid.uuid4)
    created = models.DateTimeField(
        default=now,
    )
    credit_card = models.ForeignKey(
        to=CreditCard,
        on_delete=models.CASCADE,
    )
    amount = models.DecimalField(
        max_digits=7,
        decimal_places=2,
        validators=[MinValueValidator(0)],
    )