from django.contrib.auth.models import User
from rest_framework import serializers
from company.models import Company


class NestedCompanySerializer(serializers.Serializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='company-detail',
        lookup_field='identifier',
    )
    identifier = serializers.UUIDField()
    name = serializers.CharField()


class ProfileSerializer(serializers.Serializer):
    username = serializers.CharField()
    email = serializers.CharField()
    admin_companies = NestedCompanySerializer(many=True, source='admin_set')