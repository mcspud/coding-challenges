from django.apps import AppConfig


class ArchaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'archa'
