import uuid
from django.conf import settings
from django.db import models
from django.contrib.auth.models import Group


class Company(models.Model):
    identifier = models.UUIDField(default=uuid.uuid4)
    name = models.TextField()

    admin_set = models.ManyToManyField(
        to=settings.AUTH_USER_MODEL,
        related_name='admin_set',
    )
    user_set = models.ManyToManyField(
        to=settings.AUTH_USER_MODEL,
        related_name='user_set',
    )