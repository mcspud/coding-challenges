# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     NomNomMeter.Repo.insert!(%NomNomMeter.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

# def user_fixture(attrs \\ %{}) do
#   {:ok, user} =
#     Enum.into(attrs, %{
#       email: Faker.Person.email(),
#       nickname: Faker.Person.name(),
#       password: Faker.Person.email()
#     })
#     |> NomNomMeter.Accounts.register_user()
#   user
# end

users = [
  %{email: "test@test.com", nickname: "test@test.com", password: "test@test.com"}
  |> NomNomMeter.Accounts.register_user(),

  %{email: "test2@test.com", nickname: "test2@test.com", password: "test2@test.com"}
  |> NomNomMeter.Accounts.register_user(),

  %{email: "test3@test.com", nickname: "test3@test.com", password: "test3@test.com"}
  |> NomNomMeter.Accounts.register_user(),
]

for _ <- 1..3 do
  dish =
    NomNomMeter.Repo.insert!(%NomNomMeter.Dishes.Dish{
      name: Faker.Lorem.sentence(),
      description: Faker.Lorem.paragraph(3..10),
      price: Enum.random(100..1000) / 10
    })

  for idx <- 0..2 do
    NomNomMeter.Repo.insert!(%NomNomMeter.Ratings.Rating{
      dish_id: dish.id,
      user_id: with {:ok, user} <- Enum.at(users, idx) do user.id end,
      rating: Enum.random(1..5)
    })
  end
end
