defmodule NomNomMeter.Repo.Migrations.CreateRatingsRating do
  use Ecto.Migration

  def change do
    create table(:ratings_rating) do
      add :user_id, references(:accounts_users, on_delete: :delete_all)
      add :dish_id, references(:dishes_dish, on_delete: :delete_all)
      add :rating, :integer

      timestamps()
    end

    # can only rate a dish once
    create unique_index(:ratings_rating, [:user_id, :dish_id])
    create index(:ratings_rating, :user_id)
    create index(:ratings_rating, :dish_id)
    # allow only values 1->5
    create constraint("ratings_rating", :rating_value_must_be_in_range, check: "rating between 1 and 5")
  end
end
