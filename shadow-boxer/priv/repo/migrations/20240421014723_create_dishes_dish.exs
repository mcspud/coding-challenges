defmodule NomNomMeter.Repo.Migrations.CreateDishesDish do
  use Ecto.Migration

  def change do
    create table(:dishes_dish) do
      add :name, :string, null: false
      add :description, :text, null: false
      add :price, :decimal, null: false

      timestamps()
    end

    create unique_index(:dishes_dish, [:name])

    # need this for digest(text text)
    execute "create extension if not exists pgcrypto"

    # Creates an index to make sure that we can determine if 
    # a description is unique based on a generated hash.
    execute("""
      CREATE UNIQUE INDEX "dishes_dish_description_digest_index"
      ON "dishes_dish"
      USING btree (digest("description", 'sha512'::text))
    """)
  end
end
