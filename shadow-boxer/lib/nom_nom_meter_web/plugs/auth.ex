defmodule NomNomMeter.Plugs.Auth.AuthErrorHandler do
  import Plug.Conn

  @behaviour Guardian.Plug.ErrorHandler

  @impl Guardian.Plug.ErrorHandler
  def auth_error(conn, {type, _reason}, _opts) do
    body = to_string(type)

    conn
    |> put_resp_content_type("text/plain")
    |> send_resp(401, body)
  end
end

defmodule NomNomMeterWeb.Plugs.Auth do
  use Guardian.Plug.Pipeline,
    otp_app: :nom_nom_meter,
    module: NomNomMeter.Guardian,
    error_handler: NomNomMeter.Plugs.Auth.AuthErrorHandler

  plug Guardian.Plug.VerifyHeader, claims: %{"typ" => "access"}, scheme: "Bearer"
  plug Guardian.Plug.EnsureAuthenticated
  plug Guardian.Plug.LoadResource, allow_blank: false, ensure: true
end
