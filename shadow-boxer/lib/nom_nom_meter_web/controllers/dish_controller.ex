defmodule NomNomMeterWeb.DishController do
  use NomNomMeterWeb, :controller

  alias NomNomMeter.Dishes
  alias NomNomMeter.Dishes.Dish
  alias NomNomMeter.Ratings

  action_fallback NomNomMeterWeb.FallbackController

  def index(conn, _params) do
    dishes_dish = Dishes.list_dishes_dish()
    render(conn, :index, dishes_dish: dishes_dish)
  end

  def create(conn, %{"dish" => dish_params}) do
    with {:ok, %Dish{} = dish} <- Dishes.create_dish(dish_params) do
      # need to add the "rating" key to our dish
      dish = 
        dish |> Map.put(:rating, 0)

      conn
      |> put_status(:created)
      |> put_resp_header("location", ~p"/api/dishes/#{dish}")
      |> render(:show, dish: dish)
    end
  end

  def show(conn, %{"id" => id}) do
    dish = Dishes.get_dish!(id)
    render(conn, :show, dish: dish)
  end

  def update(conn, %{"id" => id, "dish" => dish_params}) do
    dish = Dishes.get_dish!(id)

    with {:ok, %Dish{} = dish} <- Dishes.update_dish(dish, dish_params) do
      render(conn, :show, dish: dish)
    end
  end

  def delete(conn, %{"id" => id}) do
    dish = Dishes.get_dish!(id)

    with {:ok, %Dish{}} <- Dishes.delete_dish(dish) do
      send_resp(conn, :no_content, "")
    end
  end

  def rate(conn, %{"id" => id, "rating" => rating}) do
    user = Guardian.Plug.current_resource(conn)
    dish = Dishes.get_dish!(id)

    case Ratings.create_rating(%{"dish_id" => id, "user_id" => user.id, "rating" => rating}) do
      {:ok, %Ratings.Rating{}} ->
        render(conn, :show, dish: dish)

      {:error, %Ecto.Changeset{} = _changeset} ->
        send_resp(conn, 400, "You have already rated this item")
    end
  end
end
