defmodule NomNomMeterWeb.TokenController do
  use NomNomMeterWeb, :controller

  alias NomNomMeter.Guardian
  alias NomNomMeter.Accounts
  alias Guardian.Plug

  def get_user(conn, _params) do
    user = Guardian.Plug.current_resource(conn)

    conn |> put_status(200) |> json(%{email: user.email, id: user.id})
  end


  def get_token(conn, %{"email" => email, "password" => password}) do
    user = NomNomMeter.Accounts.get_user_by_email_and_password(email, password)

    case user do
      nil ->
        conn |> put_status(401) |> json(%{error: "Invalid credentials"})

      _ ->
        {:ok, jwt, _full_claims} = NomNomMeter.Guardian.encode_and_sign(user)
        conn |> put_status(200) |> json(%{token: jwt})
    end
  end


  def get_token(conn, _params) do
    conn |> put_status(400) |> json(%{error: "Bad Request"})
  end
end
