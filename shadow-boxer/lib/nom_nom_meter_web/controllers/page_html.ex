defmodule NomNomMeterWeb.PageHTML do
  use NomNomMeterWeb, :html

  embed_templates "page_html/*"
end
