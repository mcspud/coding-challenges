defmodule NomNomMeterWeb.DishJSON do
  alias NomNomMeter.Dishes.Dish
  use NomNomMeterWeb, :verified_routes

  @doc """
  Renders a list of dishes_dish.
  """
  def index(%{dishes_dish: dishes_dish}) do
    %{data: for(dish <- dishes_dish, do: data(dish))}
  end

  @doc """
  Renders a single dish.
  """
  def show(%{dish: dish}) do
    %{data: show_data(dish)}
  end

  defp data(%Dish{} = dish) do
    %{
      id: dish.id,
      name: dish.name,
      description: dish.description,
      price: dish.price,
      location: ~p"/api/dishes/#{dish}",
      rate: ~p"/api/dishes/#{dish}/rate"
    }
  end

  defp show_data(%Dish{} = dish) do
    dish |> data() |> Map.merge(%{rating: dish.rating || 0 })
  end
end
