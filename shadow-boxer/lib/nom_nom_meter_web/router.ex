defmodule NomNomMeterWeb.Router do
  use NomNomMeterWeb, :router

  import NomNomMeterWeb.UserAuth

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, html: {NomNomMeterWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user
  end

  pipeline :api do
    plug :accepts, ["json"]
  end


  pipeline :api_auth do
    plug :api
    plug NomNomMeterWeb.Plugs.Auth
  end

  scope "/", NomNomMeterWeb do
    pipe_through :browser

    get "/", PageController, :home
  end

  # Other scopes may use custom stacks.
  scope "/api", NomNomMeterWeb do
    pipe_through :api

    post "/get_token", TokenController, :get_token
  end

  scope "/api", NomNomMeterWeb do 
    pipe_through :api_auth
    resources "/dishes", DishController #, except: [:new, :edit]

    post "/dishes/:id/rate", DishController, :rate
  end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:nom_nom_meter, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: NomNomMeterWeb.Telemetry
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end

  ## Authentication routes

  scope "/", NomNomMeterWeb do
    pipe_through [:browser, :redirect_if_user_is_authenticated]

    live_session :redirect_if_user_is_authenticated,
      on_mount: [{NomNomMeterWeb.UserAuth, :redirect_if_user_is_authenticated}] do
      live "/accounts_users/register", UserRegistrationLive, :new
      live "/accounts_users/log_in", UserLoginLive, :new
      live "/accounts_users/reset_password", UserForgotPasswordLive, :new
      live "/accounts_users/reset_password/:token", UserResetPasswordLive, :edit
    end

    post "/accounts_users/log_in", UserSessionController, :create
  end

  scope "/", NomNomMeterWeb do
    pipe_through [:browser, :require_authenticated_user]

    live_session :require_authenticated_user,
      on_mount: [{NomNomMeterWeb.UserAuth, :ensure_authenticated}] do
      live "/accounts_users/settings", UserSettingsLive, :edit
      live "/accounts_users/settings/confirm_email/:token", UserSettingsLive, :confirm_email
    end
  end

  scope "/", NomNomMeterWeb do
    pipe_through [:browser]

    delete "/accounts_users/log_out", UserSessionController, :delete

    live_session :current_user,
      on_mount: [{NomNomMeterWeb.UserAuth, :mount_current_user}] do
      live "/accounts_users/confirm/:token", UserConfirmationLive, :edit
      live "/accounts_users/confirm", UserConfirmationInstructionsLive, :new
    end
  end
end
