defmodule NomNomMeterWeb.Layouts do
  use NomNomMeterWeb, :html

  embed_templates "layouts/*"
end
