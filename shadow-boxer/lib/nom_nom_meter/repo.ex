defmodule NomNomMeter.Repo do
  use Ecto.Repo,
    otp_app: :nom_nom_meter,
    adapter: Ecto.Adapters.Postgres
end
