defmodule NomNomMeter.Ratings.Rating do
  use Ecto.Schema
  import Ecto.Changeset

  schema "ratings_rating" do
    field :rating, :integer

    belongs_to :dishes_dish, NomNomMeter.Dishes.Dish, foreign_key: :dish_id
    belongs_to :accounts_users, NomNomMeter.Accounts.User, foreign_key: :user_id 

    timestamps()
  end

  @doc false
  def changeset(rating, attrs) do
    rating
    |> cast(attrs, [:user_id, :dish_id, :rating])
    |> validate_required([:user_id, :dish_id, :rating])
    |> unique_constraint([:user_id, :dish_id], message: "You have already rated this dish")
  end
end
