defmodule NomNomMeter.Guardian do
  alias NomNomMeter.Accounts
  use Guardian, otp_app: :nom_nom_meter

  def subject_for_token(user, _claims) do
    # use email only and derive off that.
    {:ok, user.email |> to_string()}
  end

  def subject_for_token(_, _) do
    {:error, :reason_for_error}
  end

  def resource_from_claims(%{"sub" => email}) do
    user = NomNomMeter.Accounts.get_user_by_email(email)
    {:ok, user}
  rescue
    Ecto.NoResultsError -> {:error, :resource_not_found}
  end

  def resource_from_claims(_claims) do
    {:error, :reason_for_error}
  end
end
