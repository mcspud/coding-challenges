defmodule NomNomMeter.Dishes.Dish do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query, only: [from: 2]

  schema "dishes_dish" do
    field :name, :string
    field :description, :string
    field :price, :decimal

    has_many :ratings_rating, NomNomMeter.Ratings.Rating, foreign_key: :id

    timestamps()
  end

  @doc false
  def changeset(dish, attrs) do
    dish
    |> cast(attrs, [:name, :description, :price])
    |> validate_required([:name, :description, :price])
    |> unique_constraint(:name)
    # use our custom hash to check uniqueness of description
    |> unique_constraint(:description, name: "dishes_dish_description_digest_index")
  end

  def average_rating(dish_id) do
    from(r in NomNomMeter.Ratings.Rating,
      where: r.dish_id == ^dish_id,
      select: avg(r.rating)
    )
  end
end
