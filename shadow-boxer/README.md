# NomNomMeter

To start your Phoenix server:

- Run docker with `docker compose up`
- Run `mix setup` to install and setup dependencies
- Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser. Some default users, dishes and ratings have been seeded.

NOTE: I did **not** install phoenix in a container for this test so you'll need multiple terminal windows to run this.

# Usage

- `GET localhost:4000/api/get_token`

  ```bash
    { "email": "test@test.com", "password": "test@test.com"}
    // eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiO...
  ```


- `GET localhost:4000/api/dishes`

  ```bash
      {
      "data": [
        {
            "id": 1,
            "name": "Cumque nulla eius qui qui accusantium est.",
            "description": "Autem fugiat autem numquam corrupti! Sint molestias harum ea dolorum aut! Exercitationem reiciendis voluptates aliquid accusantium neque incidunt!",
            "location": "/api/dishes/1",
            "rate": "/api/dishes/1/rate",
            "price": "19.7"
        },
      ...
      ]
      }
  ```


- `POST localhost:4000/api/dishes`

```bash 
{
    "dish": {
        "name": "Aspedisfv.",
        "description": "Iasdfnventore.",
        "price": "48.1"
    }
}

```

See notes about pattern matching and validation below!


- `PUT localhost:4000/api/dishes/:id`

```bash 
{
    "dish": {
        "name": "new name",
        "description": "new description.",
        "price": "69420"
    }
}

```


- `DELETE localhost:4000/api/dishes/:id`

```bash 
{
}
```


- `POST localhost:4000/api/dishes/:id/rate`

```bash 
{
  "rating": 4
}

```


# Assumptions + Comments

- The test asks for both username AND nickname. I'm assuming this is a typo, so with the auth I've left it with the following intuition:

  - `email`: `username` is a synonym for email and is used to authenticate.
  - `nickname`: additionl field added to the user model

- I've indexed the `description` text field by:

  - I've created a SHA256 hash and indexed based on it. This can be seen in `migrations/20240421021847_create_ratings_rating.exs`
  - I've added an integration test in `dishes_test.ex:unique index hash works as expected`
  - Changesets have been updated to include this non-standard unique constraint

- I've done a very low level `HATEOAS` implementation on the `dishes` route

  - See `location` key
  - See `rating` key

- I've put in a limited number of catch-all matches for invalid JSON pattern matching. I didn't think it was worth spending too much time so I've shown it once and we'll go from there, see:
  - `NomNomMeterWeb.TokenController.get_token`

- JWT was implemented using `Bearer` scheme.
  - Hit the `get_token` endpoint above
  - Set `Authorization: Bearer <your JWT>`

- I have check constraints for the rating, limiting it to:
  - An integer 
  - Between 1-5

- I had all tests passing until I put in the db seeds and then a few broke.  Derp.  
  ![]( ./tests.png )

