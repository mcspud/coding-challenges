defmodule NomNomMeterWeb.ErrorJSONTest do
  use NomNomMeterWeb.ConnCase, async: true

  test "renders 404" do
    assert NomNomMeterWeb.ErrorJSON.render("404.json", %{}) == %{errors: %{detail: "Not Found"}}
  end

  test "renders 500" do
    assert NomNomMeterWeb.ErrorJSON.render("500.json", %{}) ==
             %{errors: %{detail: "Internal Server Error"}}
  end
end
