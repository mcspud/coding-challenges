defmodule NomNomMeter.DishesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `NomNomMeter.Dishes` context.
  """

  @doc """
  Generate a dish.
  """

  def dish_fixture(attrs \\ %{}) do
    {:ok, dish} =
      attrs
      |> Enum.into(%{
        description: "some description",
        name: "some name",
        price: "120.5"
      })
      |> NomNomMeter.Dishes.create_dish()

    dish
  end


  def dish_fixture_dynamic(attrs \\ %{}) do
    {:ok, dish} =
      attrs
      |> Enum.into(%{
        description: Faker.Lorem.paragraph(3..8),
        name: Faker.Lorem.sentence(),
        price: "120.5"
      })
      |> NomNomMeter.Dishes.create_dish()

    dish
  end
end
