defmodule NomNomMeter.RatingsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `NomNomMeter.Ratings` context.
  """
  alias NomNomMeter.DishesFixtures
  alias NomNomMeter.AccountsFixtures

  @doc """
  Generate a rating.
  """
  def rating_fixture(attrs \\ %{}) do
    {:ok, rating} =
      attrs
      |> Enum.into(%{
        dish_id: DishesFixtures.dish_fixture().id,
        rating: Enum.random(1..5),
        user_id: AccountsFixtures.user_fixture().id
      })
      |> NomNomMeter.Ratings.create_rating()

    rating
  end
end
