defmodule NomNomMeter.RatingsTest do
  alias NomNomMeter.DishesFixtures
  alias NomNomMeter.AccountsFixtures
  use NomNomMeter.DataCase

  alias NomNomMeter.Ratings

  describe "ratings_rating" do
    alias NomNomMeter.Ratings.Rating

    import NomNomMeter.RatingsFixtures

    @invalid_attrs %{user_id: nil, dish_id: nil, rating: nil}

    @tag :skip
    test "list_ratings_rating/0 returns all ratings_rating" do
      rating = rating_fixture()
      assert Ratings.list_ratings_rating() == [rating]
    end

    test "get_rating!/1 returns the rating with given id" do
      rating = rating_fixture()
      assert Ratings.get_rating!(rating.id) == rating
    end

    test "create_rating/1 with valid data creates a rating" do
      user = AccountsFixtures.user_fixture()
      dish = DishesFixtures.dish_fixture() 
      valid_attrs = %{
        user_id: user.id,
        dish_id: dish.id,
        rating: 1,
      }

      assert {:ok, %Rating{} = rating} = Ratings.create_rating(valid_attrs)
      assert rating.user_id == user.id
      assert rating.dish_id == dish.id
      assert rating.rating == 1
    end

    test "create_rating/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Ratings.create_rating(@invalid_attrs)
    end

    test "update_rating/2 with valid data updates the rating" do
      rating = rating_fixture()

      user_new = AccountsFixtures.user_fixture()
      dish_new = DishesFixtures.dish_fixture_dynamic() 

      update_attrs = %{
        user_id: user_new.id, 
        dish_id: dish_new.id, 
        rating: 5
      }

      assert {:ok, %Rating{} = rating} = Ratings.update_rating(rating, update_attrs)
      assert rating.user_id == user_new.id
      assert rating.dish_id == dish_new.id
      assert rating.rating == 5
    end

    test "update_rating/2 with invalid data returns error changeset" do
      rating = rating_fixture()
      assert {:error, %Ecto.Changeset{}} = Ratings.update_rating(rating, @invalid_attrs)
      assert rating == Ratings.get_rating!(rating.id)
    end

    test "delete_rating/1 deletes the rating" do
      rating = rating_fixture()
      assert {:ok, %Rating{}} = Ratings.delete_rating(rating)
      assert_raise Ecto.NoResultsError, fn -> Ratings.get_rating!(rating.id) end
    end

    test "change_rating/1 returns a rating changeset" do
      rating = rating_fixture()
      assert %Ecto.Changeset{} = Ratings.change_rating(rating)
    end
  end
end
