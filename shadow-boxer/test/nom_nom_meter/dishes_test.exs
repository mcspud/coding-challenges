defmodule NomNomMeter.DishesTest do
  use NomNomMeter.DataCase

  alias NomNomMeter.Dishes

  describe "dishes_dish" do
    alias NomNomMeter.Dishes.Dish

    import NomNomMeter.DishesFixtures

    @invalid_attrs %{name: nil, description: nil, price: nil}

    @tag :skip
    test "list_dishes_dish/0 returns all dishes_dish" do
      dish = dish_fixture()
      assert Dishes.list_dishes_dish() == [dish]
    end

    @tag :skip
    test "get_dish!/1 returns the dish with given id" do
      dish = dish_fixture()
      assert Dishes.get_dish!(dish.id) == dish
    end

    test "create_dish/1 with valid data creates a dish" do
      valid_attrs = %{
        name: "some name",
        description: "some description",
        price: "120.5"
      }

      assert {:ok, %Dish{} = dish} = Dishes.create_dish(valid_attrs)
      assert dish.name == "some name"
      assert dish.description == "some description"
      assert dish.price == Decimal.new("120.5")
    end

    test "create_dish/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Dishes.create_dish(@invalid_attrs)
    end

    test "update_dish/2 with valid data updates the dish" do
      dish = dish_fixture()

      update_attrs = %{
        name: "some updated name",
        description: "some updated description",
        price: "456.7"
      }

      assert {:ok, %Dish{} = dish} = Dishes.update_dish(dish, update_attrs)
      assert dish.name == "some updated name"
      assert dish.description == "some updated description"
      assert dish.price == Decimal.new("456.7")
    end

    @tag :skip
    test "update_dish/2 with invalid data returns error changeset" do
      dish = dish_fixture()
      assert {:error, %Ecto.Changeset{}} = Dishes.update_dish(dish, @invalid_attrs)
      assert dish == Dishes.get_dish!(dish.id)
    end

    test "delete_dish/1 deletes the dish" do
      dish = dish_fixture()
      assert {:ok, %Dish{}} = Dishes.delete_dish(dish)
      assert_raise Ecto.NoResultsError, fn -> Dishes.get_dish!(dish.id) end
    end

    test "change_dish/1 returns a dish changeset" do
      dish = dish_fixture()
      assert %Ecto.Changeset{} = Dishes.change_dish(dish)
    end

    test "unique index hash works as expected" do
      dish = dish_fixture_dynamic()

      try do
        dish_fixture_dynamic(%{:description => dish.description})
          refute true, "Test failed intentionally, this should have failed due to duplicate description"
      rescue
        _ in MatchError -> assert true
      end
    end
  end
end
