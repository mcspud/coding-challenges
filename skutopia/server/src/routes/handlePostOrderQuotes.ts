import { withAsyncErrorHandling } from './withAsyncErrorHandling';
import { carrierCodeSchema } from '../domain/entities';
import { z } from 'zod-http-schemas';
import {
  carrierQuoteApplied,
  CarrierQuoteResults,
} from '../domain/operations/carrierQuote';

const carrierQuoteRequestSchema = z.object({
  carriers: z.array(carrierCodeSchema),
});

const urlParamsSchema = z.object({
  id: z.string().nonempty(),
});

export const handlePostOrderQuotes = withAsyncErrorHandling(
  async (req, res) => {
    const bodyParseResult = carrierQuoteRequestSchema.safeParse(req.body);
    if (!bodyParseResult.success) {
      res.status(400).json({
        error: 'INVALID_REQUEST_BODY',
        validationError: bodyParseResult.error,
      });
      return;
    }


    const urlParamsParseResult = urlParamsSchema.safeParse(req.params);
    if (!urlParamsParseResult.success) {
      res.status(400).json({
        error: 'INVALID_URL_PARAMETER',
        validationError: urlParamsParseResult.error,
      });
      return;
    }

    const order = await carrierQuoteApplied(urlParamsParseResult.data.id, bodyParseResult.data.carriers)

    const outcomeStatusCodeMap: Record<CarrierQuoteResults['outcome'], number> = {
      SUCCESS: 200,
      ORDER_ALREADY_BOOKED: 400,
      ORDER_NOT_FOUND: 404,
    };

    res.status(outcomeStatusCodeMap[order.outcome]).json(order);
  }
);
