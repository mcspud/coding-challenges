import { expect } from 'chai';
import { Order } from './order';
import { OrderItem } from './orderItem';
import { orderHasQuote } from './order.invariants';


describe('order.invariants.orderHasQuote', () => {

  const mockItems: OrderItem[] = [
    {
      sku: 'SHOE-RED-1',
      quantity: 1,
      gramsPerItem: 100,
      price: 20,
    },
    {
      sku: 'SHOE-RED-2',
      quantity: 2,
      gramsPerItem: 1000,
      price: 15,
    },
  ]

  const mockOrder: Order = {
    id: '123',
    customer: 'Sally Bob',
    items: mockItems,
    quotes: [],
    status: 'RECEIVED',
  };

  it('fails with no quotes', () => {
    const result = orderHasQuote(mockOrder, 'UPS')
    expect(result).to.equal(false)
  })

  it('fails with wrong carrier code', () => {
    const order: Order = {
      ...mockOrder,
      quotes: [{ priceCents: 1000, carrier: 'FEDEX' }]
    }
    const result = orderHasQuote(order, 'UPS')
    expect(result).to.equal(false)
  })

  it('succeeds with correct carrier code', () => {
    const order: Order = {
      ...mockOrder,
      quotes: [
        { priceCents: 1000, carrier: 'FEDEX' },
        { priceCents: 2000, carrier: 'USPS' },
        { priceCents: 3000, carrier: 'UPS' },
      ]
    }
    const result = orderHasQuote(order, 'UPS')
    expect(result).to.equal(true)
  })
})
