import { CarrierCode } from "./carrierQuote";
import { Order } from "./order";

export const orderHasQuote = (order: Order, carrier: CarrierCode): boolean => {
  return order.quotes.find((q) => q.carrier === carrier) ? true : false
} 
