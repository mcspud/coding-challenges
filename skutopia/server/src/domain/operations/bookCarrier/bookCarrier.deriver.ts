import { CarrierCode, CarrierQuote, Order } from '../../entities';

type Success = {
  outcome: 'SUCCESS';
  order: Order;
};
type NoMatchingQuote = {
  outcome: 'NO_MATCHING_QUOTE';
  quotes: Order['quotes'];
};
type InvalidOrderStatus = {
  outcome: 'INVALID_ORDER_STATUS';
  expected: 'QUOTED';
  actual: Order['status'];
};
type OrderNotFound = {
  outcome: 'ORDER_NOT_FOUND';
};
type OrderAlreadyBooked = {
  outcome: 'ORDER_ALREADY_BOOKED';
  order: Order;
};

export type BookCarrierResult =
  | Success
  | NoMatchingQuote
  | InvalidOrderStatus
  | OrderNotFound
  | OrderAlreadyBooked;

export const deriveBookCarrierOutcome = (
  order: Order | undefined,
  carrier: CarrierCode
): BookCarrierResult => {
  if (!order) {
    return {
      outcome: 'ORDER_NOT_FOUND',
    };
  }
  if (order.status === 'BOOKED') {
    return {
      outcome: 'ORDER_ALREADY_BOOKED',
      order,
    };
  }

  // INFO: This block originally broke the tests so I have fixed it with the intuition described 
  //       above each comment.  The key part here is we essentially had to check multiple things 
  //       at once and draw conclusions from them.  The tests have been updated as well.
  const quote = order.quotes.find((q) => q.carrier === carrier);

  // If an order HAS been marked as quoted, but NOT for this carrier we can't do much, so lets reject this
  // activity.
  if (order.status === 'QUOTED' && quote === undefined) {
    return {
      outcome: 'NO_MATCHING_QUOTE',
      quotes: order.quotes,
    };
  }

  // If an order has NOT been marked as quoted,but we CAN find a quote then we return an invalid
  // order status.  It makes no sense to have a quote on an UNQUOTED order
  if (order.status !== 'QUOTED' && quote === undefined) {
    return {
      outcome: 'INVALID_ORDER_STATUS',
      expected: 'QUOTED',
      actual: order.status,
    };
  }


  // Finally at this stage we know an order SHOULD be marked as being quoted, so lets complain to the end user
  if (order.status !== 'QUOTED') {
    return {
      outcome: 'INVALID_ORDER_STATUS',
      expected: 'QUOTED',
      actual: order.status,
    };
  }


  return {
    outcome: 'SUCCESS',
    order: {
      ...order,
      status: 'BOOKED',
      carrierPricePaid: (quote as CarrierQuote).priceCents,
      carrierBooked: (quote as CarrierQuote).carrier,
    },
  };
};
