import { CarrierCode, Order } from '../../entities';


type Success = {
  outcome: 'SUCCESS';
  order: Order;
};

type NotFound = {
  outcome: 'ORDER_NOT_FOUND'
}

type OrderAlreadyBooked = {
  outcome: 'ORDER_ALREADY_BOOKED';
};

export type CarrierQuoteResults = 
  | Success
  | NotFound
  | OrderAlreadyBooked


const Success = (order: Order): Success => ({
  outcome: "SUCCESS",
  order: order,
})

const NotFound = (order: undefined | null): NotFound => ({
  outcome: 'ORDER_NOT_FOUND'
})

const OrderAlreadyBooked = (order: Order):OrderAlreadyBooked => ({
  outcome: 'ORDER_ALREADY_BOOKED',
})


export const calculateCarrierFees = (
  carrier: CarrierCode,
  items: Order["items"]
): number => {
  switch (carrier) {
    case "UPS":
      return items.reduce((acc, item) => acc + item.gramsPerItem * 0.05, 800);
    case "USPS":
      return items.reduce((acc, item) => acc + item.gramsPerItem * 0.02, 1050);
    case "FEDEX":
      return items.reduce((acc, item) => acc + item.gramsPerItem * 0.03, 1000);
    default:
      const _bottom: never = carrier;
      return _bottom
  }
};


// Simple helper function that takes an order and a carrier and returns back the expected
// data structure.  In general mapping over anonymous functions can cause all sorts of 
// strange assignment issues - check the tests to see a property-based implementation 
export const createCarrierQuote = (carrier: CarrierCode, order: Order): Order["quotes"][0] => ({
  carrier,
  priceCents: calculateCarrierFees(carrier, order.items)
})

export const deriveCarrierQuoteOutcome = (
  order: Order | undefined | null,
  carriers: CarrierCode[],
): CarrierQuoteResults => {

  if (order === null || order === undefined) {
    return NotFound(order)
  }

  if (order.status === 'BOOKED' ) {
    return OrderAlreadyBooked(order)
  }


  // Compute quotes for each carrier 
  const quotes: Order["quotes"] = carriers.map(carrier => createCarrierQuote(carrier, order))


  const updatedOrder: Order = {
    ...order,
    quotes,
    status: 'QUOTED',
  }

  return Success(updatedOrder)

}
