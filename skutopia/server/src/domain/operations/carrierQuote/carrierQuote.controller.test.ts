import { expect } from 'chai';
import { Order, OrderItem } from '../../entities';
import { carrierQuote } from './carrierQuote.controller';
import { ordersRepo } from '../../../repos/ordersRepo';
import * as sinon from 'sinon'
import { createCarrierQuote } from './carrierQuote.deriver';

const mockItems: OrderItem[] = [
  {
    sku: 'SHOE-RED-1',
    quantity: 1,
    gramsPerItem: 100,
    price: 20,
  },
  {
    sku: 'SHOE-RED-2',
    quantity: 2,
    gramsPerItem: 1000,
    price: 15,
  },
]

const mockOrder: Order = {
  id: '123',
  customer: 'Sally Bob',
  items: mockItems,
  quotes: [],
  status: 'RECEIVED',
};


describe('carrierQuote.controller', () => {

  afterEach(() => {
    sinon.restore()
  })

  it("controller calls getOrder method when given data", () => {
    const getOrder = sinon.spy(ordersRepo, "getOrder")
    const controller = carrierQuote(ordersRepo)

    controller("1", ["FEDEX"])

    expect(getOrder.calledOnce).to.be.true
  })


  it("controller does not call save on invalid state", async () => {
    const getOrder = sinon.spy(ordersRepo, "getOrder")
    const saveOrder = sinon.spy(ordersRepo, "saveOrder")
    const controller = carrierQuote(ordersRepo)

    const result = await controller("1", ["FEDEX"])

    expect(getOrder.calledOnce).to.be.true
    expect(saveOrder.called).to.be.false
    expect(result.outcome).to.equal('ORDER_NOT_FOUND')
  })


  it("controller does call save on valid state", async () => {
    const order: Order = {...mockOrder, quotes: [createCarrierQuote("UPS", mockOrder)]}
    await ordersRepo.saveOrder(order)
    const orders = await ordersRepo.getOrders()

    // confirm order in state
    expect(orders.length).to.be.equal(1)

    // wrap ordersRepo method calls
    const getOrder = sinon.spy(ordersRepo, "getOrder")
    const saveOrder = sinon.spy(ordersRepo, "saveOrder")
    const updateOrder = sinon.spy(ordersRepo, "updateOrder")

    // create a controller
    const controller = carrierQuote(ordersRepo)
  
    // get the result
    const result = await controller(order.id, ["UPS"])

    // test internal calls
    expect(getOrder.calledOnce).to.be.true
    expect(updateOrder.called).to.be.true
    expect(result.outcome).to.equal('SUCCESS')
  })

})
