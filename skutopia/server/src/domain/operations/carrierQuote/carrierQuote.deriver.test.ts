import { expect } from 'chai';
import { CarrierCode, Order, OrderItem } from '../../entities';
import {
  deriveCarrierQuoteOutcome,
  calculateCarrierFees,
  createCarrierQuote,
  CarrierQuoteResults
} from './carrierQuote.deriver';


const mockItems: OrderItem[] = [
  {
    sku: 'SHOE-RED-1',
    quantity: 1,
    gramsPerItem: 100,
    price: 20,
  },
  {
    sku: 'SHOE-RED-2',
    quantity: 2,
    gramsPerItem: 1000,
    price: 15,
  },
]


const mockOrder: Order = {
  id: '123',
  customer: 'Sally Bob',
  items: mockItems,
  quotes: [],
  status: 'RECEIVED',
};


describe('carrierQuote.calculateCarrierFees', () => {

  // UPS
  it('correctly computes UPS values (single)', () => {
    const result = calculateCarrierFees('UPS', Array.of(mockItems[0]))
    expect(result).to.equal(805)
  })

  it('correctly computes UPS values (multiple)', () => {
    const result = calculateCarrierFees('UPS', mockItems)
    expect(result).to.equal(855)
  })

  // USPS
  it('correctly computes USPS values (single)', () => {
    const result = calculateCarrierFees('USPS', Array.of(mockItems[0]))
    expect(result).to.equal(1052)
  })

  it('correctly computes USPS values (multiple)', () => {
    const result = calculateCarrierFees('USPS', mockItems)
    expect(result).to.equal(1072)
  })

  // FEDEX
  it('correctly computes FEDEX values (single)', () => {
    const result = calculateCarrierFees('FEDEX', Array.of(mockItems[0]))
    expect(result).to.equal(1003)
  })

  it('correctly computes FEDEX values (multiple)', () => {
    const result = calculateCarrierFees('FEDEX', mockItems)
    expect(result).to.equal(1033)
  })
})


describe('carrierQuote.createCarrierQuote', () => {
  it('ensures that it returns with the correct CARRIER code', () => {
    // This test comes from the fact that you can get stupid results like 
    // this:
    //  State = {
    //    abrv: string
    //    name: string
    //  }
    //
    //  wa: State = { 
    //    abrv: 'WA', 
    //    name: 'Queensland',
    //  }
    //
    //  To implement type constraints for these limits correctly we'd need 
    //  to make sure that we derive values in our type signatures themselves.
    //
    //  Since we only have 3 carriers lets just test them manually for now


    const order: Order = { ...mockOrder }
    const upsFees = createCarrierQuote('UPS', order)
    const uspsFees = createCarrierQuote('USPS', order)
    const fedexFees = createCarrierQuote('FEDEX', order)

    expect(upsFees.carrier).to.equal('UPS' as CarrierCode)
    expect(uspsFees.carrier).to.equal('USPS' as CarrierCode)
    expect(fedexFees.carrier).to.equal('FEDEX' as CarrierCode)
  })
})


describe('carrierQuote.deriver', () => {
  it('returns NOT FOUND if order is undefined', () => {
    const result = deriveCarrierQuoteOutcome(
      undefined,
      ['UPS', "FEDEX", "USPS"]
    );
    expect(result).to.deep.equal({ 'outcome': 'ORDER_NOT_FOUND' } as CarrierQuoteResults)
  });


  it('returns NOT FOUND if order is null', () => {
    const result = deriveCarrierQuoteOutcome(
      null,
      ['UPS', "FEDEX", "USPS"]
    );
    expect(result).to.deep.equal({ 'outcome': 'ORDER_NOT_FOUND' } as CarrierQuoteResults)
  });


  it('returns ORDER ALREADY BOOKED if order is booked', () => {
    const order: Order = { ...mockOrder, status: 'BOOKED' }
    const result = deriveCarrierQuoteOutcome(
      order,
      ['UPS', "FEDEX", "USPS"]
    );
    expect(result).to.deep.equal({ 'outcome': 'ORDER_ALREADY_BOOKED' } as CarrierQuoteResults)
  });


  it('returns SUCCESS if all checks pass', () => {
    const order: Order = { ...mockOrder }
    const result = deriveCarrierQuoteOutcome(
      order,
      ['UPS', "FEDEX", "USPS"]
    );
    expect(result.outcome).to.deep.equal('SUCCESS' as CarrierQuoteResults['outcome'])
  });


  it('returns QUOTES if status is SUCCESS ', () => {
    const order: Order = { ...mockOrder }
    const upsFees = createCarrierQuote('UPS', order)
    const uspsFees = createCarrierQuote('USPS', order)
    const fedexFees = createCarrierQuote('FEDEX', order)
    const result = deriveCarrierQuoteOutcome(
      order,
      // To future maintainers: The order that these are put in matters.
      // If you start getting failing results check your order!  These 
      // are computed in an Array.map method, and that method guarantees 
      // that ordering is consistent.
      ['UPS', "FEDEX", "USPS"]
    );
    expect(result.outcome).to.equal('SUCCESS' as CarrierQuoteResults['outcome'])
    expect(result).to.deep.equal({
      outcome: 'SUCCESS',
      order: {
        ...order,
        quotes: [upsFees, fedexFees, uspsFees],
        status: 'QUOTED',
      }
    } as CarrierQuoteResults)
  })
});
