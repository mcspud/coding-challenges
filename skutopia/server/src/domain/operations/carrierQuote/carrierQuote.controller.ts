import { CarrierCode, Order } from '../../entities';
import { ordersRepo } from '../../../repos/ordersRepo';
import {
  deriveCarrierQuoteOutcome,
  CarrierQuoteResults,
} from './carrierQuote.deriver';

// Allows for nicer imports
export { CarrierQuoteResults };


// WARN: You almost definitely don't want to use this directly. 
// WARN: Most of the time you want to use the `Applied` version below
//
// Implements a curried repository definition so we can switch it out during 
// testing if required, or potentially allows us to switch in a dynamic 
// repository during runtime for more advanced cases.
//
export const carrierQuote = (repo: typeof ordersRepo) => async (
  orderId: Order['id'],
  carriers: CarrierCode[],
): Promise<CarrierQuoteResults> => {

  const order = await repo.getOrder(orderId);

  const result = deriveCarrierQuoteOutcome(order, carriers);

  if (result.outcome === 'SUCCESS') {
    await repo.updateOrder({ ...result.order });
  }
  return result;
};


// INFO: You almost certainly want to use this during development
// Gives us a working domain controller using our default repository/database
// You almost certainly want to use this version rather than the curried version
// above
export const carrierQuoteApplied = carrierQuote(ordersRepo)

