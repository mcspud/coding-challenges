# Improvements

**Author:** Jamie Strauss
**Role:** Tech Lead
**Time taken:** 3 hours-ish

<Write about your suggested improvements here. Remember, software engineering is about communicating with people more than it is about writing instructions for machines>

### 1 - Exhaustive Matching

There is no guarantee that your unions are exhaustively checked in the `bookCarrier.deriver` methods.  If you check my `calculateCarrierFees` implementation under `carrierQuote.deriver` you will see me implement an exhaustive match.  We call this a "bottom type", which comes from being unable to compute this thing .  In more modern versions of `typescript` you can use  `value satisfies never`, but I have had to instead resort to assigning it to a constant. 

I strongly recommend this being used where possible

If you comment out one of the case statements you will see the compiler complain.  See https://en.wikipedia.org/wiki/Bottom_type for more info

### 2 - Test Review required

I think theres a bug in the `miniSkutopia.test.ts` file.  I pulled the `calculateCarrierFees` function from the API test, but it isn't testing quantity within each order itself.  I didn't update this test because I wasn't sure of the expectations here - if the test is correct then we'll need a comment explaining to future reviewers that this is intentional.

### 3 - Type Constructors
The  `BookCarrierResults` functions should be mapped to type constructors.  For example,

```ts
type Success = {
  outcome: 'SUCCESS';
  order: Order;
};

```

should be mapped to a constructor like so:

```ts 


type Success = {
  outcome: 'SUCCESS';
  order: Order;
};

const Success = (order: Order): Success => ({
  outcome: "SUCCESS",
  order: order,
})

```

This allows us to do lots of useful things:

- it saves us duplicating string-constants everywhere
- we can extract our type constructors into our domain much more cleanly
- it makes testing significantly easier

I have done this in the `carrierQuote.deriver` file.

### 4 - Compiler as a friend

Types should be imported and used in the tests.  See `carrierQuote.deriver.test` file for implementation.  Testing can be ardorous at times, but using types helps the compiler narrow changes not only in application code but also in the support code.  A compiler is our first line of testing, and I strongly advocate for it being used everywhere possible.

### 5 - Dev Experience 

Original implementation when running `npm test` from the top level is challenging as it requires a restart of the server every test cycle.  I set up a start:dev command under the server to let it automagically restart (and clear the persistance) every time a test is run.

To run:

```bash
cd ./server 
npm run start:dev

... in another terminal
cd npm test
```

Now whenever you make changes to server code it will update for you.


### 6 Curried Repositories

I've curried the repository under `carrierQuote.controller`, and created a default implementation based on the `createOrder` and `bookOrder` controller files.  I've also created a default implementation that can be used.  This lets us swap out different repositories at runtime, and gives us greater flexibility in dev time if required.  As long as the standard interface is implemented for the `new` repository it lets us decouple our dev and test experience from external services.  

See `carrierQuote.controller.test` for an implementation using this.

### 7 Invariant Composition and unclear Domain Logic

Lots of the code has not easy to grok logic.  For example, should you be able to recalculate a quote for a carrier if it already has one?  I have created an example invariant for `Order` that checks if there is a carrier quote on an order already.  This could be combined with other invariants to create a proper anti-corruption layer that can be tailored towards each resource as required.  As a thought experiment in psuedo-code: 

```ts

const orderHasExistingQuoteFromCarrier: ():boolean
const orderHasBeenBookedForShipping: ():boolean

const canRecalculateQuote(order) => orderHasBeenBookedForShipping === false
const canUpdateShippingCarrier(order) =>  orderHasBeenBookedForShipping === true && orderHasExistingQuoteFromCarrier === true
```

In these cirumstances you can define domain rules and more easily convey intent.





